<?php

return [

    // These CSS rules will be applied after the regular template CSS


        'css' => [
            '.button-content .button { background: red }',
            '.imgpop h2 {width: 100%, text-align: center; margin: 0 auto!important;color: white!important;}'
        ],


    'colors' => [

        'highlight' => '#1b1b1b',
        'button'    => '#004cad',

    ],

    'view' => [
        'senderName'  => 'Admin',
        'reminder'    => null,
        'unsubscribe' => null,
        'address'     => null,

        'title' => "Confirmation",

        'logo'        => [
            'path'   => 'logo2.png',
            'width'  => '',
            'height' => '',
        ],

        'twitter'  => null,
        'facebook' => null,
        'flickr'   => null,
    ],

];
