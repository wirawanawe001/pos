<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Basic -->
    <meta charset="utf-8">
    <title>Chick n Chill App</title>
    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="YOURStore - Responsive HTML5 Template">
    <meta name="author" content="etheme.com">
    <link rel="shortcut icon" href="{{URL('/')}}/front/images/Asset 1.png">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- External Plugins CSS -->
    <link rel="stylesheet" href="{{URL('/')}}/front/external/slick/slick.scss">
    <link rel="stylesheet" href="{{URL('/')}}/front/external/slick/slick-theme.css">
    <link rel="stylesheet" href="{{URL('/')}}/front/external/magnific-popup/magnific-popup.css">
    <link rel="stylesheet" href="{{URL('/')}}/front/external/nouislider/nouislider.css">
    <link rel="stylesheet" href="{{URL('/')}}/front/external/bootstrap-select/bootstrap-select.css">
    <link rel="stylesheet" href="{{URL('/')}}/assets/global/plugins/bootstrap/css/bootstrap.css">
    <!-- SLIDER REVOLUTION 4.x CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="{{URL('/')}}/front/external/rs-plugin/css/settings.css" media="screen" />
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{URL('/')}}/front/css/style.css">
    <link rel="stylesheet" href="{{URL('/')}}/front/css/custom.css">

    <!-- Icon Fonts  -->
    <link rel="stylesheet" href="{{URL('/')}}/front/font/style.css">
    <!-- Head Libs -->
    <!-- Modernizr -->
    <script src="{{URL('/')}}/front/external/modernizr/modernizr.js"></script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="index">
<div id="app" style="height: 100vh; font-family: 'Chronograph Slab';font-weight: bold;">
    @include('layouts.header.header_table')

    @yield('content')

</div>
<script type="text/javascript">
    var base_url = '{{URL('/')}}';
    var network;
</script>
<script src="{{URL('/')}}/front/external/jquery/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>

<!-- Bootstrap 3-->
<script src="{{URL('/')}}/front/external/bootstrap/bootstrap.min.js"></script>

<script src="{{URL('/')}}/js/app.js"></script>
<!-- Specific Page External Plugins -->
<script src="{{URL('/')}}/front/external/slick/slick.min.js"></script>
<script src="{{URL('/')}}/front/external/bootstrap-select/bootstrap-select.min.js"></script>
<script src="{{URL('/')}}/front/external/countdown/jquery.plugin.min.js"></script>
<script src="{{URL('/')}}/front/external/countdown/jquery.countdown.min.js"></script>
<script src="{{URL('/')}}/front/external/instafeed/instafeed.min.js"></script>
<script src="{{URL('/')}}/front/external/magnific-popup/jquery.magnific-popup.min.js"></script>
<script src="{{URL('/')}}/front/external/nouislider/nouislider.min.js"></script>
<script src="{{URL('/')}}/front/external/isotope/isotope.pkgd.min.js"></script>
<script src="{{URL('/')}}/front/external/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="{{URL('/')}}/front/external/colorbox/jquery.colorbox-min.js"></script>
<!-- SLIDER REVOLUTION 4.x SCRIPTS  -->
<script type="text/javascript" src="{{URL('/')}}/front/external/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="{{URL('/')}}/front/external/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<!-- Custom -->
<script src="{{URL('/')}}/front/js/custom.js"></script>
<script src="{{URL('/')}}/front/js/js-index-06.js"></script>
@yield('page_js')

</body>
</html>