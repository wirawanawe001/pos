		<!-- HEADER section -->
		<div class="header-wrapper" style="height: 100px;">
			<header id="header" class="header-layout-06">
				<div class="container" style="width: 100%;">
					<div class="row">
						<div class="col-md-12" style="height: 40px;background-color: #6d1c1e;">
							<img src="{{ asset('uploads') }}/{{$slider[2]['background']}}" alt="placeholder+image" height="90%" style="padding-top: 5px;padding-left: 20px;">
						</div>
						<div class="col-md-12" style="padding-top: 30px;padding-left: 50px;padding-right: 50px;">
							<a href="{{route('table')}}"><button type="button" class="btn btn-success" style="color: #6d1c1e;">Back</button></a>
							<a href="{{route('order.delete', $order['id'])}}"><button type="button" class="btn btn-danger" style="color: #6d1c1e;">Cancel <i class="fa fa-trash"></i> </button></a>
							<div class="login" style="float: right;">
								<ul style="list-style-type: none;">
									<li>
										<i class="fa fa-user" style="color: #6d1c1e;"></i>
										@guest
											<a href="{{route('ulogin')}}">Login</a>
										@else
											<a href="{{route('account')}}" style="color: #6d1c1e;">{{ Auth::user()->name }}</a><a href="{{route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="color: #6d1c1e;">
												[Logout]
											</a>
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										@endguest
									</li>
								</ul>
							</div>
						</div>
                    </div>
				</div>
			</header>
		</div>
		<!-- End HEADER section -->