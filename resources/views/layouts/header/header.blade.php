		<!-- HEADER section -->
		<div class="header-wrapper" style="height: 100px;">
			<header id="header" class="header-layout-06" style="top: 40px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12 logo" style="padding: 0;position: relative;">
							<a href=""><img class="center-block" src="{{ asset('uploads') }}/{{ $slider[1]['background'] }}" style="height: 160%;"></a>
							<div class="login" style="float: right;position: absolute;top: 0;right: 0;">
								<ul style="list-style-type: none;padding-left: 0;">
									<li>
										@guest
											<a href="{{route('login')}}" style="color: #ffffff;padding-right: 20px;">Dashboard</a>
											<i class="fa fa-user" style="color: #ffffff;"></i>
											<a href="{{route('ulogin')}}" style="color: #ffffff;">Login</a>
										@else
											<a href="#" style="color: #ffffff;">{{ Auth::user()->name }}</a><a href="{{route('logout') }}"onclick="event.preventDefault();document.getElementById('logout-form').submit();" style="color: #ffffff;">
												[Logout]
											</a>
											<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
												{{ csrf_field() }}
											</form>
										@endguest
									</li>
								</ul>
							</div>
						</div>
                    </div>
				</div>
			</header>
		</div>
		<!-- End HEADER section -->