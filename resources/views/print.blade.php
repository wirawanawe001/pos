 <!-- Print Struck -->
    <div class="print_struk" id="DivIdToPrintStruck">
        <div class="check" style="padding-bottom: 20px;text-align: center">
            <span>###</span><span> Check Order</span><span> ###</span>
        </div>
        <div class="col-md-12">
            <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
        </div>
        <div class="info" style="width: 100%;position: relative">
            <div style="50%;">Table  <span style="padding-left: 5px;"> :</span> {{$table['number']}}</div>
            <div style="50%;position: absolute;top: 0;right: 0;"> Customer  <span style="padding-left: 5px;"> :</span> {{$order['name']}}</div>
        </div>
        <div class="col-md-12">
            <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
        </div>
        @forelse($orderDetail->where('stage',$order->stage) as $orD)
            <div class="bill_main" style="font-size: 12pt;width: 100%;position: relative;padding-bottom: 10px;">
                <div style="width: 40%;display: inline-block;">{{$orD->product['name']}} </div> <div style="width: 20%;display: inline-block;position: absolute;top: 0;right: 0;"> {{$orD->qty}}</div>
            </div>
        @empty
            <div class="empty"><h3>Menu Empty stage {{$order->stage}}</h3></div>
        @endforelse
        <div class="col-md-12">
            <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
        </div>
        @php
            $date = new DateTime($order['created_at']);
            $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
        @endphp
        <div style="float: right;font-size: 9pt; "><span>Tanggal </span> <span style="padding-left: 5px;"> :</span> {{$date->format('Y-m-d H:i:s')}}</div>
    </div>
    <!-- Print Struck -->

    <!-- Print Bill -->
    <div class="print_bill" style="overflow-y: auto;" id="DivIdToPrintBill">
        <div class="col-md-12 bill_header">
            <div class="header_img" style="height: 60px;text-align: center">
                <img src="{{ asset('uploads') }}/{{ $slider->first()->background }}" height="100%" alt="">
            </div>
            <div class="header_info" style="font-size: 10pt;text-align: center;">
                <span>{{$addresses->first()['value']}}</span> <br> <span>{{$phone['value']}}</span>
            </div>
            <div class="col-md-12 orderName" style="font-size: 10pt;padding-left: 0px;text-align: left;padding-top: 10px;">
                <span>Receipt # </span> <span style="padding-left: 5px;"> :</span> {{$order['order_code']}}<br>
                @php
                    $date = new DateTime($order['created_at']);
                    $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
                @endphp
                <span>Tanggal </span> <span style="padding-left: 15px;"> :</span> {{$date->format('Y-m-d H:i:s')}}<br>
                <span>Operator </span><span style="padding-left: 7px;"> :</span> {{ Auth::user()->name }}<br>
                <span>Customer  </span><span style="padding-left: 4px;"> :</span> {{$order['name']}} <br>
                @if($table['id'] != NULL )
                    <span>Table </span> <span style="padding-left: 30px;"> :</span> {{$table['number']}}<br>
                @elseif($order['address'] != NULL)
                    <span>Address </span> <span style="padding-left: 14px;"> :</span> {{$order['address']}}<br>
                @else
                @endif
                <h4 style="padding-top: 10px;">{{$order['type']}}</h4>

            </div>
            <div class="clearfix"></div>
        </div>
        <div class="con_bill">
            <div class="col-md-12">
                <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
            </div>
            @forelse($orderDetail as $orD)
                <div class="col-md-12 bill_main" style="font-size: 12pt;width: 100%;">
                    <div style="width: 40%;display: inline-block;">{{$orD->product['name']}} </div> <div style="width: 20%;display: inline-block;"> {{$orD->qty}}</div>
                    <div style="width: 35%;display: inline-block;text-align: right;"> @php echo "Rp ".number_format($orD->qty*$orD->product['menu_price'],0,',','.') @endphp</div>
                </div>
            @empty
                <div class="empty"><h3>Menu Empty</h3></div>
            @endforelse
            <div class="col-md-12">
                <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
            </div>
        </div>
        @if($orderDetail->count() > 0)
                <div class="con_total" style="position: relative;">
                    <div class="total">
                        <div style="text-align: left;"><b>Subtotal</b></div>
                        <div style="float: right;position: absolute;top: 0;right: 0;"><b>Rp <span>@php echo "".number_format($subtotal,0,',','.') @endphp</span></b></div>
                        <input type="hidden" value="{{$subtotal}}" name="total">
                    </div>
                    @if($order->discount != NULL)
                        <div class="bayar">
                            <div style="text-align: left;"><b>Discount</b></div>
                            <div style="float: right;position: absolute;top: 20px;right: 0;"><b>{{$order->discount}}%</b></div>
                        </div>
                        <div class="total">
                            <div style="text-align: left;"><b>Total Inc. Disc.</b></div>
                            <div style="float: right;position: absolute;top: 38px;right: 0;"><b>Rp <span class="total_harga">@php echo " ".number_format((100/100)*$order->total,0,',','.') @endphp</span></b></div>
                        </div>
                        {{--<div class="tax">--}}
                            {{--<div style="text-align: left;"><b>Tax</b></div>--}}
                            {{--<div style="float: right;position: absolute;top: 55px;right: 0;"><b><span>10%</span></b></div>--}}
                        {{--</div>--}}
                        <div class="total">
                            <div class="main_name" style="text-align: left;"><b>Total</b></div>
                            <div class="main_price" style="float: right;position: absolute;top: 55px;right: 0;"><b>Rp <span class="total_harga">@php echo " ".number_format($order->total,0,',','.') @endphp</span></b></div>
                        </div>
                        <div class="bayar">
                            <div class="main_name" style="text-align: left;"><b>Pembayaran</b></div>
                            <div style="float: right;position: absolute;top: 70px;right: 0;"><b>Rp <span>@php echo "".number_format($order['payment'],0,',','.') @endphp</span></b></div>
                        </div>
                        <div class="col-md-12 bayar">
                            <div class="main_name" style="text-align: left;"><b>Kembalian</b></div>
                            <div style="float: right;position: absolute;top: 85px;right: 0;"><b>Rp <span>@php echo "".number_format($order['change'],0,',','.') @endphp</span></b></div>
                        </div>
                    @else
                    {{--<div class="tax">--}}
                        {{--<div style="text-align: left;"><b>Tax</b></div>--}}
                        {{--<div style="float: right;position: absolute;top: 20px;right: 0;"><b><span>10%</span></b></div>--}}
                    {{--</div>--}}
                        <div class="total">
                            <div class="main_name" style="text-align: left;"><b>Total</b></div>
                            <div class="main_price" style="float: right;position: absolute;top: 20px;right: 0;"><b>Rp <span class="total_harga">@php echo " ".number_format($order->total,0,',','.') @endphp</span></b></div>
                        </div>
                    <div class="bayar">
                        <div class="main_name" style="text-align: left;"><b>Pembayaran</b></div>
                        <div style="float: right;position: absolute;top: 40px;right: 0;"><b>Rp <span>@php echo "".number_format($order['payment'],0,',','.') @endphp</span></b></div>
                    </div>
                    <div class="col-md-12 bayar">
                        <div class="main_name" style="text-align: left;"><b>Kembalian</b></div>
                        <div style="float: right;position: absolute;top: 54px;right: 0;"><b>Rp <span>@php echo "".number_format($order['change'],0,',','.') @endphp</span></b></div>
                    </div>
                    @endif
                    <div class="col-md-12" style="margin-top: 10px;">
                        <div class="line_bill" style="border-bottom: dashed;"></div>
                    </div>
                    <div class="col-md-12" style="margin-top: 4px;">
                        <div class="line_bill" style="border-bottom: dashed;"></div>
                    </div>
                </div>
            @endif
        <div class="bill_footer" style="text-align: center;">
            <div class="footer_thx">
                <h4>{{$slider->first()->title}}</h4>
                <p>{{$slider->first()->subtitle}}</p>
            </div>
            <div class="sosmed" style="text-align: center;font-size: 18pt;">
                <div><img src="{{URL('/')}}/front/images/igcopy.png" alt="" width="20%"></div><div style="color: #000000;text-align:center;width: 100%;">{{$instagram['value']}}</div>
            </div>

            <div class="delivery"><h4>Delivery Number :</h4>
                <p>{{$phone['value']}}</p>
            </div>
        </div>
    </div>
    <!-- Print Bill -->
