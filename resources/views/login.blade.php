@extends('layouts.layout')
@section('page_style')

@endsection

@section('content')
		<!-- cart page content section start -->
		<section class="login-page section-padding">
			<div class="container">
				<div class="row" style="position: absolute;left: 50%;transform: translateX(-50%);top: 250px;margin: 0;">
					<div class="col-sm-6 col-sm-offset-3">
						<div class="row">
							<div class="single-check">
								<form action="{{route('login')}}" method="post">
									{{ csrf_field() }}
									<div class="single-input p-bottom50 clearfix" style="background-color: #fcfcf2;border-radius: 20px;text-align: center;">
										<div class="col-xs-12" style="padding-top: 30px;">
											<div class="check-title">
												<h3 style="font-family:'Chewy';color: #6d1c1e;font-size: 18pt;">LOGIN</h3>
											</div>
										</div>
										<div class="col-xs-12">
											<label>Username:</label>
											<div class="input-text">
												<input type="text" name="email" style="border-radius: 10px;"/>
											</div>
										</div>
										<div class="col-xs-12">
											<label>Password:</label>
											<div class="input-text">
												<input type="password" name="password" style="border-radius: 10px;"/>
											</div>
										</div>
										<div class="col-xs-12">
											<!--<div class="forget">
												<a href="#">Forget your password?</a>
											</div>-->
										</div>
										<div class="col-xs-12" style="padding-top: 40px;">
											<div class="submit-text">
												<button class="btn-success" type="submit" style="border-radius: 10px;background-color: #6d1c1e;width: 100px;height: 40px;font-family:'Chewy';font-size: 18pt; ">ENTER</button>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="forget" style="margin-top: 30px;">
												<a href="{{route('uregister')}}">Not member, register here</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!--<div class="col-sm-6">
						<div class="single-input p-bottom50 clearfix">
							<form action="my-account.html" method="post">
								<div class="row">
									<div class="col-xs-12">
										<div class="check-title">
											<h3>New Customer</h3>
										</div>
									</div>
									<div class="col-sm-6">
										<label>First Name:</label>
										<div class="input-text">
											<input type="text" name="name" />
										</div>
									</div>
									<div class="col-sm-6">
										<label>Last Name:</label>
										<div class="input-text">
											<input type="text" name="name" />
										</div>
									</div>
									<div class="col-xs-12">
										<label>Address:</label>
										<div class="input-text">
											<input type="text" name="address" />
										</div>
									</div>
									<div class="col-xs-12">
										<label>City/Town:</label>
										<div class="input-text">
											<input type="text" name="city" />
										</div>
									</div>
									<div class="col-sm-6">
										<label>Email:</label>
										<div class="input-text">
											<input type="text" name="email" />
										</div>
									</div>
									<div class="col-sm-6">
										<label>Phone:</label>
										<div class="input-text">
											<input type="text" name="phone" />
										</div>
									</div>
									<div class="col-xs-12">
										<div class="billing-checkbox">
											<input type="checkbox" name="billing-address" value="1" class="checkbox">
											<label>Sign up for our newsletter! </label>
										</div>
										<div class="submit-text">
											<input type="submit" name="submit" value="Register">
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>-->
				</div>
			</div>
		</section>
		<!-- cart page content section end -->
@endsection
