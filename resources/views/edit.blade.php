@extends('layouts.layout2')
@section('page_style')

@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8 menu"><h3>Meja {{$table['number']}}</h3>
                    <div class="col-md-12 batas">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <div class="tab-menu">
                                    <ul style="list-style-type: none;padding-left: 0;">
                                        @foreach($categories as $indexKey=>$category)
                                            @if($category->productCategory->count() != 0)
                                                @if(($indexKey+1)==1)
                                                    <li class="active" style="width: 15%;padding-left: 30px;">
                                                        <a data-toggle="tab" href="#{{$category->name}}"><img src="{{ asset('uploads') }}/{{ $category->image }}" alt="placeholder+image" width="100%"></a>
                                                    </li>
                                                @else
                                                    <li  style="width: 15%;padding-left: 30px;">
                                                        <a data-toggle="tab" href="#{{$category->name}}"><img src="{{ asset('uploads') }}/{{ $category->image }}" alt="placeholder+image" width="100%"> @if(count($category)!=($indexKey+1))@endif</a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="overflow-y: auto;height: 500px;">
                            <div class="text-center tab-content">
                                @foreach($categories as $indexKey=>$category)
                                    <div class="tab-pane fade in @if(++$indexKey==1) active @endif" id="{{$category->name}}">
                                        <div class="four-item single-products">
                                            @foreach($category->productCategory as $productCategory)
                                                <div class="col-md-4 menu_title">{{$productCategory->product['name']}}
                                                    <div class="clearfix"></div>
                                                    <form action="{{route('orderDetail.edit',[$order['id']])}}" method="POST">
                                                        {{csrf_field()}}
                                                        <div class="menu_img" style="background: url('{{ asset('uploads') }}/{{$productCategory->product['imageProduct'][0]['url']}}');background-size: cover;">
                                                            <button type="submit" class="btn" style="width: 100%;height: 100%;border-radius: 20px;opacity: 0;">Select</button>
                                                        </div>
                                                        <input type="hidden" value="{{$table['id']}}" name="table">
                                                        <input type="hidden" value="{{$productCategory->product['id']}}" name="product">
                                                        <input type="hidden" value="{{$order['id']}}" name="order">
                                                        <input type="hidden" value="{{$productCategory->product['menu_price']}}" name="price">
                                                        @if($order->table_id != NULL)
                                                            <input type="hidden" name="stage" value="{{$order->stage}}">
                                                        @endif
                                                        <div class="menu_qty">
                                                            <div class="s-select s-plus-minus">
                                                                <div class="plus-minus">
                                                                    <a class="dec qtybutton">-</a>
                                                                    <input type="text" value="1" name="qtybutton" class="plus-minus-box" readonly>
                                                                    <a class="inc qtybutton">+</a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <form action="{{route('order.add.update',$order['id'])}}" method="POST">
                    {{csrf_field()}}
                    <div class="col-md-4 con_bill" >
                        <div class="bill" style="overflow-y: auto;font-family: 'Verdana';font-weight: lighter;color: #000000;">
                            <div class="col-md-12 bill_header">
                                <div class="header_img">
                                    <img src="{{ asset('uploads') }}/{{ $slider->first()->background }}" height="100%" alt="">
                                </div>
                                <div class="header_info" style="font-size: 10pt;">
                                    <span>{{$addresses->first()['value']}}</span> / <span>{{$phone['value']}}</span>
                                </div>

                                <div class="col-md-12 orderName" style="font-size: 10pt;padding-left: 0px;text-align: left;">
                                    <span>Receipt # </span> <span style="padding-left: 5px;"> :</span> {{$order['order_code']}}<br>
                                    @php
                                        $date = new DateTime($order['created_at']);
                                        $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
                                    @endphp
                                    <span>Tanggal </span> <span style="padding-left: 15px;"> :</span> {{$date->format('Y-m-d H:i:s')}}<br>                                    <span>Operator </span><span style="padding-left: 7px;"> :</span> {{ Auth::user()->name }}<br>
                                    <span>Customer  </span><span style="padding-left: 4px;"> :</span> {{$order['name']}} <br>
                                    @if($table['id'] != NULL )
                                        <span>Table </span> <span style="padding-left: 30px;"> :</span> {{$table['number']}}<br>
                                    @elseif($order['address'] != NULL)
                                        <span>Address </span> <span style="padding-left: 30px;"> :</span> {{$order['address']}}<br>
                                    @endif
                                    <h4 style="padding-top: 10px;"><b>{{$order['type']}}</b></h4>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-12">
                                <div class="line"></div>
                            </div>
                            @forelse($orderDetail as $orD)
                                <div class="col-md-12 bill_main">
                                    <div class="col-md-6 main_name">{{$orD->product->name}}</div>
                                    <div class="col-md-1 main_qty">{{$orD->qty}}</div>
                                    <div class="col-md-4 main_price">@php echo "Rp ".number_format($orD->qty*$orD->product->menu_price,0,',','.') @endphp</div>
                                    <span class="cancel-item"><a href="{{route('orderDetail.delete',$orD->id)}}"><i class="fa fa-close"></i></a></span>
                                </div>
                            @empty
                                <div class="empty"><h3>Menu Empty</h3></div>
                            @endforelse
                            <div class="col-md-12">
                                <div class="line"></div>
                            </div>
                            @if($orderDetail->count() > 0)
                                <div class="col-md-12 total">
                                    <div class="col-md-6 main_name" style="text-align: left;"><b>Total</b></div>
                                    <div class="col-md-6 main_price"><b>@php echo "Rp ".number_format($subtotal,0,',','.') @endphp</b></div>
                                </div>
                            @endif
                            <div class="col-md-12 bill_footer">
                                <div class="footer_thx">
                                    <h4>{{$slider->first()->title}}</h4>
                                    <p>{{$slider->first()->subtitle}}</p>
                                </div>
                                <div class="sosmed" style="text-align: left;">
                                    <ul class="clearfix">
                                        <li>
                                            <a href="https://www.facebook.com/{{$facebook['value']}}"><i class="fa fa-facebook"></i></a>
                                        </li>
                                        <li>
                                            <a href="https://www.instagram.com/{{$instagram['value']}}"><i class="fa fa-instagram"></i></a>
                                        </li>

                                        <li>
                                            <a href="{{$email['value']}}"><i class="fa fa-envelope"></i></a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="delivery"><h4>Delivery Number :</h4>
                                    <p>{{$phone['value']}}</p>
                                </div>
                            </div>
                        </div>
                        @include('print')
                        <div class="print" style="text-align: center;">
                            <a  onclick="printDivStruck();" class="btn btn-success">Print Order</a>
                            <a href="{{route('bill',[$order['id']])}}" id="sample_editable_1_new" ><button class="btn btn-success">Save Order</button></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    </div>
@endsection
@section('page_js')
    <script>
        $(function() {
            var qty = $('input[name=qtybutton]').val();
            console.log("qty:" + qty);

            $(".dec").on("click", function () {
                var qty = parseInt($(this).next().val());
                if(qty>0){
                    qty = qty - 1;
                }
                $(this).next().val(qty);
            });
            $(".inc").on("click", function () {
                var qty = parseInt($(this).prev().val());
                qty = qty + 1;
                $(this).prev().val(qty);
            });
        });

        function printDivStruck()
        {

            var divToPrint=document.getElementById('DivIdToPrintStruck');

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">');
            newWin.document.write(divToPrint.innerHTML);
            newWin.document.write('</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);

        }
    </script>
@endsection