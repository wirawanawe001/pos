@extends('layouts.layout')
@section('page_style')

@endsection

@section('content')
	<!-- cart page content section start -->
	<section class="login-page section-padding">
		<div class="container">
			<div class="row">
			<!--<div class="col-sm-6 col-sm-offset-3">
						<div class="row">
							<div class="single-check">
								<form action="my-account.html" method="post">
									<div class="single-input p-bottom50 clearfix">
										<div class="col-xs-12">
											<div class="check-title">
												<h3>login</h3>
												<p>If you have an account with us, Please log in!</p>
											</div>
										</div>
										<div class="col-xs-12">
											<label>Email:</label>
											<div class="input-text">
												<input type="text" name="email" />
											</div>
										</div>
										<div class="col-xs-12">
											<label>Password:</label>
											<div class="input-text">
												<input type="password" name="password" />
											</div>
										</div>
										<div class="col-xs-12">
											<div class="forget">
												<a href="#">Forget your password?</a>
											</div>
										</div>
										<div class="col-xs-12">
											<div class="submit-text">
												<input type="submit" name="submit" value="Login">
											</div>
										</div>
										<div class="col-xs-12">
											<div class="forget" style="margin-top: 30px;">
												<a href="{{route('register')}}">Not login register here</a>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>-->
				<div class="col-sm-6 col-sm-offset-3">
					<div class="single-input p-bottom50 clearfix">
						<form action="{{route('uregister.post')}}" method="post">
							{{csrf_field()}}
							<div class="row" style="padding-top: 100px;">
								<div class="col-xs-12">
									<div class="check-title">
										<h3 style="color: #ffffff;">New Customer</h3>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">First Name:</label>
									<div class="input-text">
										<input type="text" name="first" required/>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">Last Name:</label>
									<div class="input-text">
										<input type="text" name="last" required/>
									</div>
								</div>
								<div class="col-xs-12">
									<label style="color: #ffffff;">Address:</label>
									<div class="input-text">
										<input type="text" name="address" required/>
									</div>
								</div>
								<div class="col-xs-12">
									<label style="color: #ffffff;">City/Town:</label>
									<div class="input-text">
										<input type="text" name="city" required/>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">Email:</label>
									<div class="input-text">
										<input type="text" name="email" required/>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">Phone:</label>
									<div class="input-text">
										<input type="text" name="phone" required/>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">Password:</label>
									<div class="input-text">
										<input type="password" name="password" required/>
									</div>
								</div>
								<div class="col-sm-6">
									<label style="color: #ffffff;">Confirm Password:</label>
									<div class="input-text">
										<input type="password" class="form-control" name="password_confirmation" required>
									</div>
								</div>
								<div class="col-xs-12">
									<!--<div class="billing-checkbox">
                                        <input type="checkbox" name="billing-address" value="1" class="checkbox">
                                        <label>Sign up for our newsletter! </label>
                                    </div>-->
									<div class="submit-text">
										<input type="submit" name="submit" value="Register">
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- cart page content section end -->
@endsection
