@extends('layouts.layout3')
@section('page_style')

@endsection

@section('content')
    <div class="container" style="padding-top: 30px;">
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-8 menu">
                    @if($order->table_id != NULL)<h3 style="color: #6d1c1e;">Meja {{$table['number']}}</h3>@endif
                    <div class="col-md-12 batas">
                        <div class="row">
                            <div class="col-xs-12 text-center">
                                <div class="tab-menu">
                                    <ul style="list-style-type: none;padding-left: 0;">
                                        @foreach($categories as $indexKey=>$category)
                                            @if($category->productCategory->count() != 0)
                                                @if(($indexKey+1)==1)
                                                    <li class="active" style="width: 15%;padding-left: 30px;">
                                                        <a data-toggle="tab" href="#{{$category->name}}"><img src="{{ asset('uploads') }}/{{ $category->image }}" alt="placeholder+image" width="100%"></a>
                                                    </li>
                                                @else
                                                    <li  style="width: 15%;padding-left: 30px;">
                                                        <a data-toggle="tab" href="#{{$category->name}}"><img src="{{ asset('uploads') }}/{{ $category->image }}" alt="placeholder+image" width="100%"> @if(count($category)!=($indexKey+1))@endif</a>
                                                    </li>
                                                @endif
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="overflow-y: auto;height: 500px;">
                            <div class="text-center tab-content">
                            @foreach($categories as $indexKey=>$category)
                                <div class="tab-pane fade in @if(++$indexKey==1) active @endif" id="{{$category->name}}">
                                    <div class="four-item single-products">
                                        @foreach($category->productCategory as $productCategory)
                                            <div class="col-md-4 menu_title" style="color: #6d1c1e;">{{$productCategory->product['name']}}
                                                <div class="clearfix"></div>
                                                <div class="menu_img" style="background: url('{{ asset('uploads') }}/{{$productCategory->product['imageProduct'][0]['url']}}');background-size: cover;"></div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                @if($order['payment'] == NULL)
                <form action="{{route('order.add.update',$order['id'])}}" method="POST">
                    {{csrf_field()}}
                <div class="col-md-4 con_bill">
                    <div class="bill" style="overflow-y: auto;font-family: 'Verdana';font-weight: lighter;color: #000000;">
                        <div class="col-md-12 bill_header">
                            <div class="header_img" style="height: 60px;">
                                <img src="{{ asset('uploads') }}/{{ $slider->first()->background }}" height="100%" alt="">
                            </div>
                            <div class="header_info" style="font-size: 10pt;">
                                <span>{{$addresses->first()['value']}}</span> / <span>{{$phone['value']}}</span>
                            </div>
                            <div class="col-md-12 orderName" style="font-size: 10pt;padding-left: 0px;text-align: left;padding-top: 20px;">
                                <span>Receipt # </span> <span style="padding-left: 5px;"> :</span> {{$order['order_code']}}<br>
                                @php
                                    $date = new DateTime($order['created_at']);
                                    $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
                                @endphp
                                <span>Tanggal </span> <span style="padding-left: 15px;"> :</span> {{$date->format('Y-m-d H:i:s')}}<br>
                                <span>Operator </span><span style="padding-left: 7px;"> :</span> {{ Auth::user()->name }}<br>
                                <span>Customer  </span><span style="padding-left: 4px;"> :</span> {{$order['name']}} <br>
                                @if($order->table_id != NULL )
                                <span>Table </span> <span style="padding-left: 30px;"> :</span> {{$order->table->number}}<br>
                                @endif
                                @if($order['address'] != NULL)
                                <span>Address </span> <span style="padding-left: 30px;"> :</span> {{$order['address']}}<br>
                                @endif
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="col-md-12">
                            <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
                        </div>
                        @forelse($orderDetail as $orD)
                            <div class="col-md-12 bill_main">
                                <div class="col-md-6 main_name">{{$orD->product['name']}}</div>
                                <div class="col-md-1 main_qty">{{$orD->qty}}</div>
                                <div class="col-md-4 main_price">@php echo "Rp ".number_format($orD->qty*$orD->product['menu_price'],0,',','.') @endphp</div>
                            </div>
                        @empty
                            <div class="empty"><h3>Menu Empty</h3></div>
                        @endforelse
                        <div class="col-md-12">
                            <div class="line" style="border-bottom: dashed;margin: 5px 0px;"></div>
                        </div>
                        @if($orderDetail->count() > 0)
                            <div class="col-md-12 total">
                                <div class="col-md-6 main_name" style="text-align: left;"><b>Subtotal</b></div>
                                <div class="col-md-6 main_price"><b>Rp<span class="subtotal">@php echo " ".number_format($subtotal,0,',','.') @endphp</span></b></div>
                            </div>
                            <div class="col-md-12 bayar" id="dvDiscount" style="display: block;">
                                <div class="col-md-6 main_name"  style="text-align: left;"><b>Discount</b></div>
                                <div class="col-md-6 main_pay">
                                    <div class="input-group">
                                        <b><input type="text" value="" style="border-color: white;height: 25px;width: 50px;left: 40px;color: #000000;" class="form-control discount" maxlength="9" name="discount" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required></b><span class="input-group-addon" style="border-color: white;color: #000000;padding-left: 0px;">%</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 total">
                                <div class="col-md-6 main_name" style="text-align: left;"><b>Total Inc. Disc.</b></div>
                                <div class="col-md-6 main_price"><b>Rp<span class=grand_total>@php echo " ".number_format((($subtotal)*(100-$order->discount))/100,0,',','.') @endphp</span></b></div>
                                <input type="hidden" value="{{($subtotal*100)/100}}" name="grand_total">
                            </div>
                            {{--<div class="col-md-12 total">--}}
                                {{--<div class="col-md-6 main_name" style="text-align: left;"><b>Tax</b></div>--}}
                                {{--<div class="col-md-6 main_price"><b>10%</b></div>--}}
                            {{--</div>--}}
                            <div class="col-md-12 total">
                                <div class="col-md-6 main_name" style="text-align: left;"><b>Total</b></div>
                                <div class="col-md-6 main_price"><b>Rp<span class="total_harga">@php echo " ".number_format(($subtotal*100)/100,0,',','.') @endphp</span></b></div>
                                <input type="hidden" name="total">
                            </div>
                            <div class="col-md-12 bayar">
                                <div class="col-md-6 main_name" style="text-align: left;"><b>Pembayaran</b></div>
                                <div class="col-md-6 main_pay">
                                    <div class="input-group">
                                        <span class="input-group-addon" style="border-color: white;color: #000000;">Rp</span><input type="text" style="border-color: white;height: 25px;color: #000000;" class="form-control pay" maxlength="9" name="payment" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 bayar">
                                <div class="col-md-6 main_name" style="text-align: left;"><b>Kembalian</b></div>
                                <b><div class="col-md-6 main_price">Rp<span class="kembalian">0</span></div></b>
                                <input type="hidden"  value="" name="change">
                            </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                <div class="line_bill" style="border-bottom: dashed;"></div>
                            </div>
                            <div class="col-md-12" style="margin-top: 4px;">
                                <div class="line_bill" style="border-bottom: dashed;"></div>
                            </div>
                        @endif
                        <div class="col-md-12 bill_footer">
                            <div class="footer_thx">
                                <h4 style="margin: 0;">{{$slider->first()->title}}</h4>
                                <p>{{$slider->first()->subtitle}}</p>
                            </div>
                            <div class="sosmed" style="text-align: center;font-size: 18pt;">
                                <a href="https://www.instagram.com/{{$instagram['value']}}"><i class="fa fa-instagram" style="color: #000000;"><span style="padding-left: 10px;color: #000000;">{{$instagram['value']}}</span></i></a>
                            </div>
                            <div class="delivery"><h4>Delivery Number :</h4>
                                <p>{{$phone['value']}}</p>
                            </div>
                        </div>
                    </div>
                    {{--<label for="discount">--}}
                        {{--<input type="checkbox" id="discount" onclick="ShowHideDiv(this)" />--}}
                        {{--Discount--}}
                    {{--</label>--}}
                    <div class="print">
                        <button type="submit" class="btn btn-success">Save Order</button>
                    </div>
                </div>
                </form>
                @else
                <form action="{{route('order.add.print', $order['id'])}}" method="POST">
                    {{csrf_field()}}
                    <div class="col-md-4 con_bill" >
                        <div class="bill" style="overflow-y: auto;font-family: 'Verdana';font-weight: lighter;color: #000000;">
                            <div class="col-md-12 bill_header">
                                <div class="header_img" style="height: 60px;">
                                    <img src="{{ asset('uploads') }}/{{ $slider->first()->background }}" height="100%" alt="">
                                </div>
                                <div class="header_info" style="font-size: 10pt;">
                                    <span>{{$addresses->first()['value']}}</span> / <span>{{$phone['value']}}</span>
                                </div>
                                <div class="col-md-12 orderName" style="font-size: 10pt;padding-left: 0px;text-align: left;padding-top: 20px;">
                                    <span>Receipt # </span> <span style="padding-left: 5px;"> :</span> {{$order['order_code']}}<br>
                                    @php
                                        $date = new DateTime($order['created_at']);
                                        $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
                                    @endphp
                                    <span>Tanggal </span> <span style="padding-left: 15px;"> :</span> {{$date->format('Y-m-d H:i:s')}}<br>
                                    <span>Operator </span><span style="padding-left: 7px;"> :</span> {{ Auth::user()->name }}<br>
                                    <span>Customer  </span><span style="padding-left: 4px;"> :</span> {{$order['name']}} <br>
                                    @if($order->table_id != NULL )
                                        <span>Table </span> <span style="padding-left: 30px;"> :</span> {{$table['number']}}<br>
                                    @endif
                                    @if($order['address'] != NULL)
                                        <span>Address </span> <span style="padding-left: 14px;"> :</span> {{$order['address']}}<br>
                                    @endif
                                    <h4 style="padding-top: 10px;">{{$order['type']}}</h4>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="con_bill">
                                <div class="col-md-12">
                                    <div class="line"></div>
                                </div>
                                @forelse($orderDetail as $orD)
                                    <div class="col-md-12 bill_main">
                                        <div class="col-md-6 main_name">{{$orD->product['name']}}</div>
                                        <div class="col-md-1 main_qty">{{$orD->qty}}</div>
                                        <div class="col-md-4 main_price">@php echo "Rp ".number_format($orD->qty*$orD->product['menu_price'],0,',','.') @endphp</div>
                                    </div>
                                @empty
                                    <div class="empty"><h3>Menu Empty</h3></div>
                                @endforelse
                                <div class="col-md-12">
                                    <div class="line"></div>
                                </div>
                            </div>
                            @if($orderDetail->count() > 0)
                                <div class="col-md-12 total">
                                    <div class="col-md-6 main_name" style="text-align: left;"><b>Subtotal</b></div>
                                    <div class="col-md-6 main_price"><b>@php echo "Rp ".number_format($subtotal,0,',','.') @endphp</b></div>
                                </div>
                                @if($order->discount != NULL)
                                    <div class="col-md-12 bayar">
                                        <div class="col-md-6 main_name" style="text-align: left;"><b>Discount</b></div>
                                        <div class="col-md-6 main_price"><b>{{$order->discount}}%</b></div>
                                    </div>
                                    <div class="col-md-12 total">
                                        <div class="col-md-6 main_name" style="text-align: left;"><b>Total Inc. Disc.</b></div>
                                        <div class="col-md-6 main_price"><b>Rp<span class="total_harga">@php echo " ".number_format((100/100)*$order->total,0,',','.') @endphp</span></b></div>
                                    </div>
                                    {{--<div class="col-md-12 total">--}}
                                        {{--<div class="col-md-6 main_name" style="text-align: left;"><b>Tax</b></div>--}}
                                        {{--<div class="col-md-6 main_price"><b>10%</b></div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-12 total">
                                        <div class="col-md-6 main_name" style="text-align: left;"><b>Total</b></div>
                                        <div class="col-md-6 main_price"><b>Rp<span class="total_harga">@php echo " ".number_format($order->total,0,',','.') @endphp</span></b></div>
                                    </div>
                                @else
                                    {{--<div class="col-md-12 total">--}}
                                        {{--<div class="col-md-6 main_name" style="text-align: left;"><b>Tax</b></div>--}}
                                        {{--<div class="col-md-6 main_price"><b>10%</b></div>--}}
                                    {{--</div>--}}
                                    <div class="col-md-12 total">
                                        <div class="col-md-6 main_name" style="text-align: left;"><b>Total</b></div>
                                        <div class="col-md-6 main_price"><b>Rp<span class="total_harga">@php echo " ".number_format($order->total,0,',','.') @endphp</span></b></div>
                                    </div>
                                @endif
                                <div class="col-md-12 bayar">
                                    <div class="col-md-6 main_name" style="text-align: left;"><b>Pembayaran</b></div>
                                    <div class="col-md-6 main_price"><b>@php echo "Rp ".number_format($order['payment'],0,',','.') @endphp</b></div>
                                </div>
                                <div class="col-md-12 bayar">
                                    <div class="col-md-6 main_name" style="text-align: left;"><b>Kembalian</b></div>
                                    <div class="col-md-6 main_price"><b>@php echo "Rp ".number_format($order['change'],0,',','.') @endphp</b></div>
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <div class="line_bill" style="border-bottom: dashed;"></div>
                                </div>
                                <div class="col-md-12" style="margin-top: 4px;">
                                    <div class="line_bill" style="border-bottom: dashed;"></div>
                                </div>
                            @endif
                            <div class="col-md-12 bill_footer">
                                <div class="footer_thx">
                                    <h4>{{$slider->first()->title}}</h4>
                                    <p>{{$slider->first()->subtitle}}</p>
                                </div>
                                <div class="sosmed" style="text-align: center;font-size: 18pt;">
                                    <a href="https://www.instagram.com/{{$instagram['value']}}"><i class="fa fa-instagram" style="color: #000000;"><span style="padding-left: 10px;color: #000000;">{{$instagram['value']}}</span></i></a>
                                </div>
                                <div class="delivery"><h4>Delivery Number :</h4>
                                    <p>{{$phone['value']}}</p>
                                </div>
                            </div>
                        </div>
                        @include('print')
                        <div class="print" style="text-align: center;">
                            <a  onclick="printDivBill();" class="btn btn-success">Print Bill</a>
                            <button type="submit" class="btn btn-success"  >Save Bill</button>
                        </div>
                    </div>
                </form>
                @endif
            </div>
        </div>
    </div>
@endsection
@section('page_js')
    <script>
        $(function() {
            var qty = $('input[name=qtybutton]').val();
            console.log("qty:" + qty);

            $(".dec").on("click", function () {
                var qty = parseInt($(this).next().val());
                if(qty>0){
                    qty = qty - 1;
                }
                $(this).next().val(qty);
            });
            $(".inc").on("click", function () {
                var qty = parseInt($(this).prev().val());
                qty = qty + 1;
                $(this).prev().val(qty);
            });

            //DISCOUNT//
            $("input[name=discount]").on('keyup paste',function(){
                var total = ((100 - parseInt($("input[name=discount]").val())) * parseInt(($(".subtotal").text()).split('.').join("")))/100;
                $(".grand_total").text(total);
                $(".total_harga").text((total*100)/100);
                $("input[name=total]").val((total*100)/100);

                if(parseInt($("input[name=payment]").val())>parseInt(($(".grand_total").text()).split('.').join(""))){
                    var total = parseInt($("input[name=payment]").val()) - parseInt(($(".grand_total").text()).split('.').join(""));
                    $(".kembalian").text(total);
                    $("input[name=change]").val(total);
                }else{
                    $(".kembalian").text(0);
                }
            });

            ///KEMBALIAN///
            $("input[name=payment]").on('keyup paste',function(){
                if(parseInt($("input[name=payment]").val())>parseInt(($(".total_harga").text()).split('.').join(""))){
                    var total = parseInt($("input[name=payment]").val()) - parseInt(($(".total_harga").text()).split('.').join(""));
                    $(".kembalian").text(total);
                    $("input[name=change]").val(total);
                }else{
                    $(".kembalian").text(0);
                }
            });
        });


        // function ShowHideDiv(discount) {
        //     var dvDiscount = document.getElementById("dvDiscount");
        //     dvDiscount.style.display = discount.checked ? "block" : "none";
        // }

        function printDivBill()
        {

            var divToPrint=document.getElementById('DivIdToPrintBill');

            var newWin=window.open('','Print-Window');

            newWin.document.open();

            newWin.document.write('<html><body onload="window.print()">');
            newWin.document.write(divToPrint.innerHTML);
            newWin.document.write('</body></html>');

            newWin.document.close();

            setTimeout(function(){newWin.close();},10);

        }

        // var doc = new jsPDF();
        // var specialElementHandlers = {
        //     '#editor': function (element, renderer) {
        //         return true;
        //     }
        // };
        //
        // $(document).ready(function() {
        //     $('#dwn').click(function () {
        //         doc.fromHTML($('#DivIdToPrint').html(), 15, 15, {
        //             'width': 170,
        //             'elementHandlers': specialElementHandlers
        //         });
        //         doc.save('bill-(order_code).pdf');
        //     });
        // });
    </script>
@endsection