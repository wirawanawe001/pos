@extends('layouts.layout')
@section('page_style')

@endsection

@section('content')
<div class="container" style="margin-top: 50px;">
    <div class="row" >
        <form class="form cf" action="{{route('order.add.post')}}" method="POST">
            {{csrf_field()}}
            <section class="plan cf" >
            <h4 style="text-align: center;margin-top: 70px;color: #ffffff;">Masukkan Nama :</h4>
            <div class="center-block" style="margin-top: 10px;width: 30%;">
                <input type="text" class="form-control" name="name" style="display: block!important;" required>
            </div>
            <div class="col-md-12 choice" style="padding-top: 30px;margin-left: 32%;padding-bottom: 15px;">
                <div class="mt-radio-inline">
                    <input type="radio" name="type" id="dine" value="Dine In"><label class="free-label four col" for="dine" style="border: none;height: 40px;width: 10%;color: #6d1c1e;">Dine In</label>
                    <input type="radio" name="type" id="deliv" value="Delivery"><label class="free-label four col" for="deliv" style="border: none;height: 40px;width: 10%;color: #6d1c1e;">Delivery</label>
                    <input type="radio" name="type" id="take" value="Take Away"><label class="free-label four col" for="take" style="border: none;height: 40px;width: 10%;color: #6d1c1e;">Take Away</label>
                </div>
            </div>
            <div class="col-md-12" style="padding-top: 30px;text-align: center;">
                <button type="submit" class="btn btn-success">Masuk</button>
            </div>
            </section>
        </form>
    </div>
</div>
@endsection
@section('page_js')
    <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
    <script>
        $(function() {
            var countdine = 0, countdelivery = 0;
            $("div.dine").css("display", "none");
            $("div.delivery").css("display", "none");
            $("input[name=type]").on("click", function() {
                if ($("input[name=type]:checked").val() == "Dine In") {
                    $("div.delivery").remove();
                    $("div.take").remove();
                    countdelivery = 0;
                    if(countdine == 0) {
                    $("div.choice").after(
                        '<div class="col-md-12 dine">'+
                        '@foreach($tables as $tab)'+
                        '@if($tab->status != "Filled")'+
                        '<div class="col-md-2 input">'+
                            '<input type="radio" name="table" id="{{$tab->id}}" value="{{$tab->id}}" required><label class="free-label four col" for="{{$tab->id}}" style="border: 2px solid black;color: #6d1c1e;">Meja {{$tab->number}}</label>'+
                        '</div>'+
                        '@else'+
                        '<div class="col-md-2 input">'+
                            '<a href="{{route('bill', $tab->order->first()->id)}}"><label class="free-label four col" for="{{$tab->id}}" style="border: 2px solid black;background-color:#a8a8a8;color: #6d1c1e;">Meja {{$tab->number}} <div>{{$tab->order->first()->name}}</div></label></a>'+
                        '</div>'+
                        '@endif'+
                        '@endforeach'+
                        '<div class="clearfix"></div>'+
                        '</div>');

                        countdine = 1;
                    }
                }else if ($("input[name=type]:checked").val() == "Delivery") {
                    $("div.dine").remove();
                    $("div.take").remove();
                    countdine = 0;
                    if(countdelivery == 0) {
                    $("div.choice").after(
                        '<div class="center-block delivery" style="width: 30%;margin-top: 10px;text-align: center;">'+
                        '<h4 style="text-align: center;margin-top: 30px;color: #ffffff;;">Masukkan Alamat :</h4>'+
                        '<input type="hidden" value="" name="table">'+
                        '<input type="text" class="form-control" name="address" style="display: block!important;" required>'+
                        '</div>');
                        countdelivery = 1;
                    }
                } else {
                    $("div.delivery").remove();
                    $("div.dine").remove();
                    $("div.choice").after('<input type="hidden" value="" name="table">');
                }
            });
        });
    </script>
@endsection
@section('page_js')
    <script src="{{ asset('js/blueimp/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/blueimp/jquery.iframe-transport.js') }}" type="text/javascript"></script>
    <script src="{{ asset('js/blueimp/jquery.fileupload.js') }}" type="text/javascript"></script>
@endsection