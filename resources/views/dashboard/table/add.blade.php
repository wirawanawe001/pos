@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.tables')}}">Table</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Add</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Table </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal" method="post" action="{{route('back.table.add.post')}}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Number</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="number" placeholder="Enter Table Number">
                                    <span class="help-block"> A block of help text. </span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position
                                </label>
                                <div class="col-md-4">
                                    <select class="table-group-action-input form-control input-medium" name="position" required>
                                        <option value="">Select...</option>
                                        <option value="Inside">Inside</option>
                                        <option value="Outside">Outside</option>
                                        <option value="Top">Top</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Status
                                </label>
                                <div class="col-md-4">
                                    <select class="table-group-action-input form-control input-medium" name="status" required>
                                        <option value="">Select...</option>
                                        <option value="Empty">Empty</option>
                                        <option value="Filled">Filled</option>
                                        <option value="Booked">Booked</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-4">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{route('back.tables')}}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->                        
                </div>
            </div>
          </div>
        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
@endsection
