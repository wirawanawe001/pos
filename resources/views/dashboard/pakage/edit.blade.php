@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Package</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Detail</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Package </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.pakage.edit.post', $pakage->id)}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name" placeholder="Enter pakage name" value="{{$pakage['name']}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="description" required>{{$pakage['description']}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ing">
                                        <label class="col-md-2 control-label">Ingredients:
                                            <span class="required"> * </span>
                                        </label>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Price:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4 input-group" style="padding-left: 15px;">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="text" class="form-control" value="{{$pakage['pakage_price']}}" name="pakage_price" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Image (Old):
                                        </label>
                                        <div class="col-md-4">
                                            <img src="{{ asset('uploads') }}/{{$pakage->image}}" alt="Old Image" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-3 control-label">Image (New):
                                        </label>
                                        <div class="col-md-4">
                                            <input type="file" class="form-control" name="image" placeholder="Enter Background" value="{{$pakage->image}}">
                                            <span class="help-block"> <i>Leave empty if you don't want to change background</i> </span>
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                      <label class="col-md-2 control-label"></label>
                                      <div class="col-md-4">
                                        <button type="button" name="addImg" class="btn btn-default blue"><i class="fa fa-plus"></i>Add Another</button>
                                      </div>
                                    </div>-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.pakage.view',$pakage['id'])}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script>
                $(function(){

                    $( "button[name=addImg]" ).on( "click" , function(){
                        $("div.image").after('<div class="form-group">'+
                            '<label class="col-md-2 control-label">Image(s):'+
                            '<span class="required"> * </span>'+
                            '</label>'+
                            '<div class="col-md-4">'+
                            '<input type="file" class="form-control" name="image[]" placeholder="Enter Image">'+
                            '<span class="help-block"> A block of help text. </span>'+
                            '</div>'+
                            '</div>');
                    });

                    $(document).on( "click" , ".deleteImg", function(){
                        $(this).parents("div").eq(1).remove();
                    });
                });


            </script>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.fileupload.js') }}" type="text/javascript"></script>

<script src="/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/jquery.iframe-transport.js"></script>
<script src="/js/jquery.fileupload.js"></script>
@endsection
