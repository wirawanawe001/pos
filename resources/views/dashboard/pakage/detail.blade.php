@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Package</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Detail</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Package </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
              <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">
                  <div class="tab-content">
                    <div class="tab-pane active" id="about">
                      <div class="portlet light bordered">
                        <div class="portlet-body form">
                          <!-- BEGIN FORM-->
                          <form class="form-horizontal" role="form">
                            <div class="form-body">
                              <h2 class="margin-bottom-20"> View About Information </h2>
                              <h3 class="form-section">Package Info</h3>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Name:</label>
                                    <div class="col-md-9">
                                      <p class="form-control-static"> {{$pakage['name']}} </p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Description:</label>
                                    <div class="col-md-9">
                                      <p class="form-control-static"> {{$pakage['description']}} </p>
                                    </div>
                                  </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Menu Price:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> @php echo "Rp ".number_format($pakage['cost_price'],0,',','.') @endphp </p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3"> Pakage Price:</label>
                                        <div class="col-md-9">
                                            <p class="form-control-static"> @php echo "Rp ".number_format($pakage['pakage_price'],0,',','.') @endphp </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                              </div>
                              <div class="form-actions">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-offset-9 col-md-3">
                                        <a href="{{route('back.pakage.edit',$pakage['id'])}}" type="submit" class="btn yellow">
                                          <i class="fa fa-pencil"></i> Edit
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!--/row-->
                                <h3 class="form-section">Menu Pakage</h3>
                                <div class="row">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <div class="table-toolbar">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="btn-group pull-right">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(count($pakage['pakageProduct']))
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                                    <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="5%"> # </th>
                                                        <th width="10%"> Name </th>
                                                        <th width="10%"> Quantity </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($pakage->pakageProduct as $indexKey => $propak)
                                                        <tr role="row" class="filter">
                                                            <td>{{$indexKey + 1}}</td>
                                                            <td>{{$propak->product['name']}}</td>
                                                            <td>{{$propak->pakage->qty}}</td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <div class="row">
                                                    <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </form>
                          <!-- END FORM-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
      </div>
        @endsection

        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
        @endsection
