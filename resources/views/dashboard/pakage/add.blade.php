@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Package</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.pakages')}}">List</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Package </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.pakage.add.post')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="description" required></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group ing">
                                        <label class="col-md-2 control-label">Menu:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="form-group">
                                            <div class="col-md-4">
                                                <button type="button" name="addIng" class="btn btn-default btn-sm blue"><i class="fa fa-plus"></i>Add</button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Price:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4 input-group" style="padding-left: 15px;">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="text" class="form-control" value="0" maxlength="6" name="pakage_price" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                        </div>
                                    </div>
                                    <div class="form-group image">
                                        <label class="col-md-2 control-label">Image(s):
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <input type="file" class="form-control" name="image" placeholder="Enter Image" required>
                                            <span class="help-block"> A block of help text. </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-4">
                                            <button type="button" name="addImg" class="btn btn-default btn-sm blue"><i class="fa fa-plus"></i>Add Another</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.pakages')}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script>
                $(function(){

                    $( "button[name=addIng]" ).on( "click" , function(){
                        $("div.ing").after('<div class="form-group">'+
                            '<label class="col-md-2 control-label">'+
                            '</label>'+
                            '<div class="col-md-2">'+
                                '<select class="form-control" name="product[]">'+
                                    '<option value="">Select...</option>'+
                                '@foreach($products as $prod)'+
                                    '<option value="{{$prod['id']}}">{{$prod['name']}}</option>'+
                                '@endforeach'+
                                '</select>'+
                            '</div>'+
                             '<div class="col-md-4">' +
                               '<label class="col-md-2 control-label">Qty:</label>'+
                                '<div class="col-md-8">'+
                                    '<div class="input-group">'+
                                        '<input type="text" class="form-control" value="" maxlength="6" name="qty[]" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>'+
                                    '</div>'+
                                '</div>'+
                            '</div>'+
                            // '<div class="col-md-4">' +
                            //     '<label class="col-md-2 control-label">Price: </label>'+
                            //         '<div class="col-md-8">'+
                            //          '<div class="input-group">'+
                            //             '<span class="input-group-addon">Rp</span>'+
                            //             '<input type="text" class="form-control" maxlength="6" name="menu_price" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57">'+
                            //         '</div>'+
                            //     '</div>'+
                            // '</div>'+
                        '</div>');
                    });

                    $( "button[name=addImg]" ).on( "click" , function(){
                        $("div.image").after('<div class="form-group">'+
                            '<label class="col-md-2 control-label">Image(s):'+
                            '<span class="required"> * </span>'+
                            '</label>'+
                            '<div class="col-md-4">'+
                            '<input type="file" class="form-control" name="image[]" placeholder="Enter Image">'+
                            '<span class="help-block"> A block of help text. </span>'+
                            '</div>'+
                            '</div>');
                    });

                    $(document).on( "click" , ".deleteImg", function(){
                        $(this).parents("div").eq(1).remove();
                    });
                });

            </script>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.fileupload.js') }}" type="text/javascript"></script>

<script src="/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/jquery.iframe-transport.js"></script>
<script src="/js/jquery.fileupload.js"></script>
@endsection
