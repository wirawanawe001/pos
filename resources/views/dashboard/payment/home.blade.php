@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.payments')}}">Payment Method</a>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Payment Method </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet light bordered">
                                  <div class="portlet-body">
                                      <div class="table-toolbar">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <div class="btn-group pull-right">
                                                      <a href="{{route('back.payment.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                                          <i class="fa fa-plus"></i>
                                                      </a>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      @if(count($payment))
                                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                          <thead>
                                              <tr>
                                                  <th> # </th>
                                                  <th> Type </th>
                                                  <th> Name </th>
                                                  <th> Account Number </th>
                                                  <th> Action? </th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @forelse($payment as $indexKey=>$pay)
                                              <tr class="odd gradeX">
                                                  <td>
                                                      {{++$indexKey}}
                                                  </td>
                                                  <td> {{$pay->type}} </td>
                                                  <td> {{$pay->account_name}} </td>
                                                  <td> {{$pay->account_number}} </td>
                                                  <td>
                                                    <a href="{{route('back.payment.view',$pay->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> View
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                                                      <a href="{{route('back.payment.delete', $pay->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                                          <i class="fa fa-trash"></i>
                                                      </a>
                                                  </td>
                                              </tr>
                                              @empty
                                                  <tr role="row" class="filter">
                                                      <td colspan="8">No Data Available</td>
                                                  </tr>
                                              @endforelse
                                          </tbody>
                                      </table>
                                      @else
                                      <div class="row">
                                        <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                                      </div>
                                      @endif
                                  </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                          </div>

        </div>
    <!-- END CONTENT BODY -->
  </div>
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
