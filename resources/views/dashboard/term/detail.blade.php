@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.term')}}">Payment Method</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Detail</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal" role="form">
                                <div class="form-body">
                                    <h2 class="margin-bottom-20"> View Term And Condition Info  </h2>
                                    <h3 class="form-section">Payment Info</h3>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Acount Type:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$payment->type}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Acount Name:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$payment->account_name}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label class="control-label col-md-3"><b>Acount Number:</b></label>
                                                <div class="col-md-9">
                                                    <p class="form-control-static"> {{$payment->account_number}} </p>
                                                </div>
                                            </div>
                                        </div>
                                        <!--/span-->
                                    </div>
                                    <!--/row-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <a href="{{route('back.payment.edit', $payment->id)}}" type="submit" class="btn green">
                                                        <i class="fa fa-pencil"></i> Edit</a>
                                                    <a href="{{route('back.payment')}}" type="button" class="btn default">Back</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6"> </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
    </div>
@endsection
@section('page_plugin_js')
    <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
    <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
