@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Product</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.about')}}">Category</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Category </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal form-row-seperated" method="post" action="{{route('back.contact.edit.post',$contact->id)}}" enctype="multipart/form-data">
                        {{csrf_field()}}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-2 control-label">Type:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-5">
                                    <select value="{{$contact['name']}}" class="table-group-action-input form-control input-medium" name="type" required>
                                        <option value="">Select...</option>
                                        <option value="Phone" {{$contact->type==="Phone"?"selected":""}}>Phone</option>
                                        <option value="Address" {{$contact->type==="Address"?"selected":""}}>Address</option>
                                        <option value="Email" {{$contact->type==="Email"?"selected":""}}>Email</option>
                                        <option value="Instagram" {{$contact->type==="Instagram"?"selected":""}}>Instagram</option>
                                        <option value="Twitter" {{$contact->type==="Twitter"?"selected":""}}>Twitter</option>
                                        <option value="Facebook" {{$contact->type==="Facebook"?"selected":""}}>Facebook</option>
                                        <option value="Line" {{$contact->type==="Line"?"selected":""}}>Line</option>
                                        <option value="Whatsapp" {{$contact->type==="Whatsapp"?"selected":""}}>Whatsapp</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">Value:
                                    <span class="required"> * </span>
                                </label>
                                <div class="col-md-4">
                                    <input type="text" value="{{$contact['value']}}" class="form-control" name="value" placeholder="" required> </div>
                            </div>


                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-4">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{route('back.about')}}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
          </div>
        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
@endsection
