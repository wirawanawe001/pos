@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.product')}}">Product</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Image</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Image Product </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.imageProduct.edit.post',[$product['id'],$imageProduct['id']])}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Image (Old):
                                    </label>
                                    <div class="col-md-6">
                                        <img src="{{ asset('uploads') }}/{{$imageProduct['url']}}" alt="Old Image" class="img-responsive">
                                    </div>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Image (New)</label>
                                        <div class="col-md-4">
                                            <input type="file" class="form-control" name="image" placeholder="Enter Image">
                                            <span class="help-block"> <i>Leave empty if you don't want to change image</i> </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.product.view',$product['id'])}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script>
                $(function(){
                    $("div.shoes").css("display","none");
                    $("div.clothes").css("display","none");
                    $( "input[name=choices]" ).on( "click", function(){
                        if($( "input[name=choices]:checked" ).val()=="clothes"){
                            $("div.shoes").remove();
                            $("div.choice").after('<div class="form-group clothes">'+
                                        '<label class="col-md-2 control-label">'+
                                        '</label>'+
                                        '<!-- CLOTHES SIZE -->'+
                                        '<div class="col-md-10">'+
                                            '<div class="mt-checkbox-inline">'+
                                                '<label for="checkboxS" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkboxS" value="s" name="size[]">S'+
                                                    '<span></span>'+
                                                '</label>'+
                                                '<label for="checkboxM" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkboxM" value="m" name="size[]">M'+
                                                    '<span></span>'+
                                                '</label>'+
                                                '<label for="checkboxL" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkboxL" value="l" name="size[]">L'+
                                                    '<span></span>'+
                                                '</label>'+
                                                '<label for="checkboxXL" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkboxXL" value="xl" name="size[]">XL'+
                                                    '<span></span>'+
                                                '</label>'+
                                                '<label for="checkboxAll" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkboxAll" value="all" name="size[]">All'+
                                                    '<span></span>'+
                                                '</label>'+
                                            '</div>'+
                                        '</div>'+
                                        '<!-- END CLOTHES SIZE -->'+
                                    '</div>');
                        }else if($( "input[name=choices]:checked" ).val()=="shoes"){
                            $("div.clothes").remove();
                            $("div.choice").after('<div class="form-group shoes">'+
                                        '<label class="col-md-2 control-label">'+
                                        '</label>'+
                                        '<!-- CLOTHES SIZE -->'+
                                        '<div class="col-md-10">'+
                                            '<div class="mt-checkbox-inline">'+
                                                '@for($i=38; $i < 43; $i++)'+
                                                '<label for="checkbox{{$i}}" class="mt-checkbox">'+
                                                    '<input type="checkbox" id="checkbox{{$i}}" value="{{$i}}" name="size[]">{{$i}}'+
                                                    '<span></span>'+
                                                '</label>'+
                                                '@endfor'+
                                            '</div>'+
                                        '</div>'+
                                        '<!-- END CLOTHES SIZE -->'+
                                    '</div>');
                        }else{
                            $("div.shoes").remove();
                            $("div.clothes").remove();
                        }
                    });
                });

                $(document).on( "click" , ".deleteImg", function(){
                    $(this).parents("div").eq(1).remove();
                });
            </script>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.fileupload.js') }}" type="text/javascript"></script>

<script src="/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/jquery.iframe-transport.js"></script>
<script src="/js/jquery.fileupload.js"></script>
@endsection
