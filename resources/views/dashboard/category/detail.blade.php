@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Product</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.categories')}}">Category</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Detail</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal" role="form">
                        <div class="form-body">
                            <h2 class="margin-bottom-20"> View Category - {{$category->name}} </h2>
                            <h3 class="form-section">Category Info</h3>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><b>Category Name:</b></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"> {{$category->name}} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-3"><b>Child Of:</b></label>
                                        <div class="col-md-9">
                                            <p class="form-control-static">
                                              @if($category->parent != NULL)
                                              {{$category->parent->name}}
                                              @else
                                              This is parent
                                              @endif
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="control-label col-md-4"><b>Description:</b></label>
                                        <div class="col-md-8">
                                            <p class="form-control-static"> {{$category->description}} </p>
                                        </div>
                                    </div>
                                </div>
                                <!--/span-->
                            </div>
                            <!--/row-->
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <a href="{{route('back.category.edit',$category->id)}}" type="submit" class="btn green">
                                            <i class="fa fa-pencil"></i> Edit</a>
                                            <a href="{{route('back.categories')}}" type="button" class="btn default">Back</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Preview Table -->
                        <div class="form-body">
                            <div class="row">
                              <div class="col-md-12">
                                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                <div class="portlet light bordered">
                                  <div class="portlet-body">
                                    @if(count($category->children))
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                      <thead>
                                        <tr>
                                          <th> # </th>
                                          <th> Name </th>
                                          <th> Description </th>
                                          <th style="width: 20%;"> Action? </th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @forelse($category->children as $indexKey=>$child)
                                        <tr class="odd gradeX">
                                          <td>   {{++$indexKey}} </td>
                                          <td> {{$child->name}} </td>
                                          <td> {{$child->description}} </td>
                                          <td>
                                            <a href="{{route('back.category.view',$child->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> View
                                              <i class="fa fa-eye"></i>
                                            </a>
                                            <a href="{{route('back.category.delete', $child->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                              <i class="fa fa-trash"></i>
                                            </a>
                                          </td>
                                        </tr>
                                        @empty
                                        <tr role="row" class="filter">
                                          <td colspan="8">No Data Available</td>
                                        </tr>
                                        @endforelse
                                      </tbody>
                                    </table>
                                    @else
                                    @endif
                                  </div>
                                </div>
                                <!-- END EXAMPLE TABLE PORTLET-->
                              </div>
                            </div>
                            <!--/row-->
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
          </div>
        </div>
    <!-- END CONTENT BODY -->
    </div>
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
