@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Product</span>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.categories')}}">Category</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Category </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form method="post" action="{{route('back.category.edit_sub.post',$category->id)}}" enctype="multipart/form-data" class="form-horizontal">
                        {{ csrf_field() }}
                        <div class="form-body">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Name</label>
                                <div class="col-md-4">
                                    <input type="text" class="form-control" name="name" placeholder="Enter text" value="{{$category->name}}">
                                    <span class="help-block"> A block of help text. </span>
                                </div>
                            </div>
                            <div class="form-group">
                              <label class="col-md-3 control-label">Have Parent</label>
                              <div class="col-md-4">
                                <select class="form-control" name="category" required>
                                  <option value="">Choose Parent Category</option>
                                  @foreach($categories as $category)
                                  @if($category->parent == NULL )
                                  <option value="{{$category->id}}">{{$category->name}}</option>
                                  @endif
                                  @endforeach
                                </select>
                                <span class="help-block"> Leave empty if you don't want to change parent category </span>
                              </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Description</label>
                                <div class="col-md-4">
                                    <textarea type="text" class="form-control" row="5" name="description" placeholder="Enter text" required>{{$category->description}}</textarea>
                                    <span class="help-block"> A block of help text. </span>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-4">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{route('back.categories')}}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
          </div>
        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
@endsection
