@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
  <!-- BEGIN CONTENT BODY -->
  <div class="page-content">
    <!-- BEGIN PAGE HEADER-->
    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
      <!-- BEGIN PAGE BREADCRUMB -->
      <ul class="page-breadcrumb">
        <li>
          <a href="{{route('back')}}">Home</a>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <span>Product</span>
          <i class="fa fa-circle"></i>
        </li>
        <li>
          <a href="{{route('back.categories')}}">Category</a>
        </li>
      </ul>
      <!-- END PAGE BREADCRUMB -->
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h1 class="page-title"> Category </h1>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
      <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
          <div class="portlet-body">
            <div class="table-toolbar">
              <div class="row">
                <div class="col-md-12">
                  <div class="btn-group pull-right">
                    <a href="{{route('back.category.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                      <i class="fa fa-plus"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
            @if(count($categories))
            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
              <thead>
                <tr>
                  <th> # </th>
                  <th> Logo </th>
                  <th> Name </th>
                  <th> Description </th>
                  <th style="width: 20%;"> Action? </th>
                </tr>
              </thead>
              <tbody>
                @forelse($categories as $indexKey=>$category)
                <tr class="odd gradeX">
                  <td>
                    {{++$indexKey}}
                  </td>
                  <td>
                    <a target="_blank" href="{{ asset('uploads') }}/{{ $category->image }}" data-toggle="tooltip" data-placement="right" title="Click for larger image">
                      <img src="{{ asset('uploads') }}/{{ $category->image }}" alt="placeholder+image" width="20%">
                    </a>
                  </td>
                  <td> {{$category->name}} </td>
                  <td>
                    {{$category->description}}
                  </td>
                  <td>
                    <a href="{{route('back.category.view',$category->id)}}" id="sample_editable_1_new" class="btn sbold yellow btn-sm"> View
                                                        <i class="fa fa-eye"></i>
                                                    </a>
                    <a href="{{route('back.category.delete', $category->id)}}" id="sample_editable_1_new" class="btn sbold red btn-sm"> Delete
                                                          <i class="fa fa-trash"></i>
                                                      </a>
                  </td>
                </tr>
                @empty
                <tr role="row" class="filter">
                  <td colspan="8">No Data Available</td>
                </tr>
                @endforelse
              </tbody>
            </table>
            @endif
          </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
      </div>

    </div>
    <!-- END CONTENT BODY -->
  </div>
  @endsection @section('page_plugin_js')
  <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
  @endsection @section('page_js')
  <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
  @endsection
