@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/clockface/css/clockface.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.product')}}">Product</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.promotional')}}">Promotional</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Edit</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Promotional Edit </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <form class="form-horizontal " method="post" action="{{route('back.promotional.edit.post',$promotional->id)}}" enctype="multipart/form-data">
                       {{csrf_field()}}
                        <div class="form-body">
                                <div class="form-group">
                                  <label class="control-label col-md-2">Promo Timer (End)</label>
                                  <div class="col-md-3">
                                    <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                      <input type="text" class="form-control" name="promo_timer" value="@php echo date('d-m-Y',strtotime($promotional->promo_timer)); @endphp"readonly>
                                      <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                        </button>
                                      </span>
                                    </div>
                                    <!-- /input-group -->
                                    <span class="help-block"> Select date </span>
                                  </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Product:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <select class="form-control" name="product_id">
                                          @foreach($products as $product)
                                          <option value="{{$product->id}}">{{$product->name}}</option>
                                          @endforeach
                                        </select>
                                        <span class="help-block"> A block of help text. </span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Description:
                                        <span class="required"> * </span>
                                    </label>
                                    <div class="col-md-4">
                                        <textarea name="description" rows="3" class="form-control">{{$promotional->description}}</textarea>
                                        <span class="help-block"> A block of help text. </span>
                                    </div>
                                </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-4">
                                    <button type="submit" class="btn green">Submit</button>
                                    <a href="{{route('back.promotional')}}" class="btn default">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                    <!-- END FORM-->
                </div>
            </div>
          </div>
        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
@endsection
