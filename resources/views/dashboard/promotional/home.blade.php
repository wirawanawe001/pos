@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.product')}}">Product</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Promotional</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Promotional </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
              <!-- BEGIN EXAMPLE TABLE PORTLET-->
              <div class="portlet light bordered">
                <div class="portlet-body">
                  @if(empty($promotional))
                  <div class="table-toolbar">
                    <div class="row">
                      <div class="col-md-12">
                        <div class="btn-group pull-right">
                          <a href="{{route('back.promotional.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                            <i class="fa fa-plus"></i>
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                  <div class="tab-pane active" id="about">
                    <div class="portlet light bordered">
                      <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form class="form-horizontal" role="form">
                          <div class="form-body">
                            <h3 class="form-section">Promotional Info</h3>
                            @if(!empty($promotional))
                            <div class="row">
                              <div class="col-md-6">
                                <div class="form-group">
                                  <label class="control-label col-md-3">Name:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> {{$promotional->product->name}} </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">Description:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> {{$promotional->product->description}} </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">Promo Until:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> @php echo date("d M Y",strtotime($promotional->promo_timer)); @endphp </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">Status:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> @if($promotional->product->status == TRUE) Available @else Unavailable @endif </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">Price:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> @php echo "Rp ".number_format($promotional->product->price,0,',','.') @endphp </p>
                                  </div>
                                </div>
                                <div class="form-group">
                                  <label class="control-label col-md-3">Discounted Price:</label>
                                  <div class="col-md-9">
                                    <p class="form-control-static"> @php echo "Rp ".number_format($promotional->product->discount_price,0,',','.') @endphp </p>
                                  </div>
                                </div>
                              </div>
                              <!--/span-->
                              <div class="col-md-6">
                                <div class="form-group">
                                  <img src="{{ asset('uploads') }}/{{ $promotional->product->imageProduct[0]['url'] }}" alt="image product" class="img-responsive">
                                </div>
                              </div>
                            </div>
                            <div class="form-actions">
                              <div class="row">
                                <div class="col-md-12">
                                  <div class="row">
                                    <div class="col-md-offset-9 col-md-3">
                                      <a href="{{route('back.promotional.edit',$promotional->id)}}" type="submit" class="btn yellow">
                                        <i class="fa fa-pencil"></i> Edit
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            @endif
                            <!--/row-->
                          </div>
                        </form>
                        <!-- END FORM-->
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <!-- END EXAMPLE TABLE PORTLET-->
            </div>
          </div>
    <!-- END CONTENT BODY -->
    </div>
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
