@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.home')}}">Front</a>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Home Front </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet light bordered">
                                  <div class="portlet-body">
                                      <div class="table-toolbar">
                                          <div class="row">
                                              <div class="col-md-12">
                                                  <div class="btn-group pull-right">
                                                      <a href="{{route('back.home.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                                          <i class="fa fa-plus"></i>
                                                      </a>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
                                      @if(count($slider))
                                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                          <thead>
                                              <tr>
                                                  <th style="width:5%;"> # </th>
                                                  <th style="width:20%;"> Logo </th>
                                                  <th style="width:25%;"> Title </th>
                                                  <th style="width:25%;"> Subtitle </th>
                                                  <th> Action? </th>
                                              </tr>
                                          </thead>
                                          <tbody>
                                              @forelse($slider as $indexKey=>$slide)
                                              <tr class="odd gradeX">
                                                  <td>
                                                      {{++$indexKey}}
                                                  </td>
                                                  <td>
                                                    <a target="_blank" href="{{ asset('uploads') }}/{{ $slide->background }}" data-toggle="tooltip" data-placement="right" title="Click for larger image">
                                                      <img src="{{ asset('uploads') }}/{{ $slide->background }}" alt="placeholder+image" width="100%">
                                                    </a>
                                                  </td>
                                                  <td>
                                                      {{$slide->title}}
                                                  </td>
                                                  <td>
                                                      {{$slide->subtitle}}
                                                  </td>
                                                  <td>
                                                    <a href="{{route('back.home.edit',$slide->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                                        <i class="fa fa-pencil"></i>
                                                    </a>
                                                      <a href="{{route('back.home.delete', $slide->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                                          <i class="fa fa-trash"></i>
                                                      </a>
                                                  </td>
                                              </tr>
                                              @endforeach
                                          </tbody>
                                      </table>
                                      @else
                                      <div class="row">
                                          <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                                      </div>
                                      @endif
                                  </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                          </div>

        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
<script>
  $(function(){
    $('[data-toggle="tooltip"]').tooltip();
  });
</script>
@endsection
