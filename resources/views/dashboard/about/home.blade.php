@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>About</span>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
            <div class="tabbable-line boxless tabbable-reversed">
              <ul class="nav nav-tabs">
                <li class="active">
                  <a href="#about" data-toggle="tab"> About </a>
                </li>
                <li>
                  <a href="#contact" data-toggle="tab"> Contact </a>
                </li>
                <li>
                  <a href="#sosmed" data-toggle="tab"> Social Media </a>
                </li>
                <li>
                  <a href="#address" data-toggle="tab"> Address </a>
                </li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="about">
                  <div class="portlet light bordered">
                    <div class="portlet-body form">
                      <!-- BEGIN FORM-->
                      <form class="form-horizontal" role="form">
                        <div class="form-body">
                          <h2 class="margin-bottom-20"> View About Information </h2>
                          <h3 class="form-section">About Info</h3>
                          <div class="row">
                            <div class="col-md-4">
                              <img src="{{ asset('uploads')}}/{{$about['image']}}" alt="placeholder+image" width="100%" class="img-responsive">
                            </div>
                            <!--/span-->
                            <div class="col-md-8">
                              <div class="form-group">
                                <label class="control-label col-md-3">Information:</label>
                                <div class="col-md-9">
                                  <p class="form-control-static"> {!! $about['information'] !!} </p>
                                </div>
                              </div>
                            </div>
                            <!--/span-->
                          </div>
                          <div class="form-actions">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="row">
                                  <div class="col-md-offset-9 col-md-3">
                                    <a href="{{route('back.about.edit')}}" type="submit" class="btn yellow">
                                      <i class="fa fa-pencil"></i> Edit
                                    </a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!--/row-->
                        </div>
                      </form>
                      <!-- END FORM-->
                    </div>
                  </div>
                </div>
                <div class="tab-pane" id="contact">
                  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                  <div class="portlet light bordered">
                    <div class="portlet-body">
                      <div class="table-toolbar">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="btn-group pull-right">
                              <a href="{{route('back.contact.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                <i class="fa fa-plus"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                          <tr>
                            <th> # </th>
                            <th> Icon </th>
                            <th> Type </th>
                            <th> Value </th>
                            <th width="150px"> Action? </th>
                          </tr>
                        </thead>
                        <tbody>
                          @forelse($contacts as $indexKey => $co)
                          <tr class="odd gradeX">
                            <td>{{$indexKey + 1}}</td>
                            <td><i class="fa fa-{{$co['icon']}}"></i></td>
                            <td>{{$co['name']}}</td>
                            <td>{{$co['value']}}</td>
                            <td>
                              <a href="{{route('back.contact.edit', $co->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                <i class="fa fa-pencil"></i>
                              </a>
                              <a href="{{route('back.contact.delete', $co->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                <i class="fa fa-trash"></i>
                              </a>
                            </td>
                          </tr>
                          @empty
                          <tr role="row" class="filter">
                            <td colspan="4">No Data Available</td>
                          </tr>
                          @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <div class="tab-pane" id="sosmed">
                  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                  <div class="portlet light bordered">
                    <div class="portlet-body">
                      <div class="table-toolbar">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="btn-group pull-right">
                              <a href="{{route('back.sosmed.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                <i class="fa fa-plus"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                        <tr>
                          <th> # </th>
                          <th> Icon </th>
                          <th> Type </th>
                          <th> Value </th>
                          <th width="150px"> Action? </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($sosmeds as $indexKey => $sm)
                          <tr class="odd gradeX">
                            <td>{{$indexKey + 1}}</td>
                            <td><i class="fa fa-{{$sm['name']}}"></i></td>
                            <td>{{$sm['name']}}</td>
                            <td>{{$sm['value']}}</td>
                            <td>
                              <a href="{{route('back.sosmed.edit', $sm->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                <i class="fa fa-pencil"></i>
                              </a>
                              <a href="{{route('back.sosmed.delete', $sm->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                <i class="fa fa-trash"></i>
                              </a>
                            </td>
                          </tr>
                        @empty
                          <tr role="row" class="filter">
                            <td colspan="4">No Data Available</td>
                          </tr>
                        @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- END EXAMPLE TABLE PORTLET-->
                </div>
                <div class="tab-pane" id="address">
                  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                  <div class="portlet light bordered">
                    <div class="portlet-body">
                      <div class="table-toolbar">
                        <div class="row">
                          <div class="col-md-12">
                            <div class="btn-group pull-right">
                              <a href="{{route('back.address.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                <i class="fa fa-plus"></i>
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                        <thead>
                        <tr>
                          <th> # </th>
                          <th> Title </th>
                          <th> Value </th>
                          <th> Location </th>
                          <th width="150px"> Action? </th>
                        </tr>
                        </thead>
                        <tbody>
                        @forelse($addresses as $indexKey => $ad)
                          <tr class="odd gradeX">
                            <td>{{$indexKey + 1}}</td>
                            <td>{{$ad['name']}}</td>
                            <td>{{$ad['value']}}</td>
                            <td>{{$ad['location']}}</td>
                            <td>
                              <a href="{{route('back.address.edit', $ad->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                <i class="fa fa-pencil"></i>
                              </a>
                              <a href="{{route('back.address.delete', $ad->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                <i class="fa fa-trash"></i>
                              </a>
                            </td>
                          </tr>
                        @empty
                          <tr role="row" class="filter">
                            <td colspan="4">No Data Available</td>
                          </tr>
                        @endforelse
                        </tbody>
                      </table>
                    </div>
                  </div>
                  <!-- END EXAMPLE TABLE PORTLET-->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
