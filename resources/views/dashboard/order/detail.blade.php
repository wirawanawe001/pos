@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.orders')}}">Orders</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Orders

                @switch($order['status'])
                    @case(1)
                    <span class="label label-success">Success</span>
                    @break
                    @case(2)
                    <span class="label label-primary">Sent</span>
                    @break
                    @case(3)
                    <span class="label label-warning">On Process</span>
                    @break
                    @case(4)
                    <span class="label label-info">Paid</span>
                    @break
                    @case(5)
                    <span class="label label-default">New</span>
                    @break
                    @default
                    <span class="label label-danger">Canceled</span>
                @endswitch
            </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="5%"> # </th>
                                    <th width="20%"> Product Name </th>
                                    <th width="10%"> Category </th>
                                    <th width="10%"> Price </th>
                                    <th width="10%"> Quantity </th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($orderDetail as $indexKey => $OD)
                                    <tr role="row" class="filter">
                                        <td>{{$indexKey + 1}}</td>
                                        <td>{{$OD['product']['name']}}</td>
                                        <td>
                                            {{$OD['product']['productCategory']['category']['name']}}
                                        </td>
                                        <td>@php echo "Rp ".number_format($OD->price,0,',','.') @endphp</td>
                                        <td>{{$OD->qty}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
