@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Product</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.product')}}">List</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Products </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="5%"> Date </th>
                                    <th width="10%"> Status </th>
                                    <th width="10%"> Order Code </th>
                                    <th width="10%"> Name </th>
                                    <th width="10%"> Address </th>
                                    <th width="10%"> Phone </th>
                                    <th width="10%"> Email </th>
                                    <th width="10%"> Total </th>
                                    <th width="10%"> Payment Method </th>
                                    <th width="15%"></th>
                                </tr>

                                </thead>
                                <tbody>
                                @forelse($orders as $indexKey => $order)
                                    <tr role="row" class="filter">
                                        <td>{{$order->created_at}}</td>
                                        <td>{{$order->status}}</td>
                                        <td>{{$order->order_code}}</td>
                                        <td>{{$order->name}}</td>
                                        <td>{{$order->address}}</td>
                                        <td>{{$order->phone}}</td>
                                        <td>{{$order->email}}</td>
                                        <td>@php echo "Rp ".number_format($order->total,0,',','.') @endphp</td>
                                        <td>{{$order->paymentMethod()->name}}</td>
                                        <td>
                                            <div class="margin-bottom-5">
                                                <a href="{{route('back.order.view', $order->code)}}"><button class="btn btn-sm btn-success filter-submit margin-bottom">Detail</button></a>
                                            </div>
                                            <div class="margin-bottom-5">
                                                <a href="{{route('back.order.delete', $order->code)}}"><button class="btn btn-sm btn-success filter-submit margin-bottom">Delete</button></a>
                                            </div>
                                        </td>
                                    </tr>
                                @empty
                                    <tr role="row" class="filter">
                                        <td colspan="10">No Data Available</td>
                                    </tr>
                                @endforelse
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
