@extends('dashboard.layouts.layout')
@section('page_plugin_css')
<link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <!-- BEGIN PAGE BREADCRUMB -->
            <ul class="page-breadcrumb">
                <li>
                    <a href="{{route('back')}}">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <a href="{{route('back.orders')}}">Orders</a>
                </li>
            </ul>
            <!-- END PAGE BREADCRUMB -->
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Orders </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <div class="row">
          <div class="col-md-12">
                              <!-- BEGIN EXAMPLE TABLE PORTLET-->
                              <div class="portlet light bordered">
                                  <div class="portlet-body">
                                      @if(count($orders))
                                      <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                          <thead>
                                              <tr>
                                                <th width="8%"> Date </th>
                                                <th width="8%"> Status </th>
                                                <th width="10%"> Order Code </th>
                                                <th width="10%"> Name Costumer </th>
                                                <th width="10%"> Type </th>
                                                <th width="10%"> Total </th>
                                                <th width="10%"> Action </th>
                                            </tr>
                                          </thead>
                                          <tbody>
                                              @forelse($orders as $indexKey => $order)
                                                <tr role="row" class="filter">
                                                    @php
                                                        $date = new DateTime($order['created_at']);
                                                        $date->setTimezone(new DateTimeZone('Asia/Jakarta'));
                                                    @endphp
                                                    <td>{{$date->format('Y-m-d H:i:s')}}</td>
                                                    <td>
                                                      @if($order->status == 5)
                                                      New Order
                                                      @elseif($order->status == 4)
                                                      Paid
                                                      @elseif($order->status == 3)
                                                      Process
                                                      @elseif($order->status == 2)
                                                      Sent
                                                      @elseif($order->status == 1)
                                                      Completed
                                                      @else
                                                      Canceled
                                                      @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('back.orderDetail', $order->id)}}">{{$order->order_code}}</a>
                                                    </td>
                                                    <td>{{$order->name}}</td>
                                                    <td>{{$order->type}}</td>
                                                    <td>@php echo "Rp ".number_format($order->total,0,',','.') @endphp</td>
                                                    <td>
                                                        <a href="{{route('back.order.delete', $order['id'])}}"><button type="button" class="btn btn-danger" style="color: #6d1c1e;"><i class="fa fa-trash"></i> </button></a>
                                                    </td>
                                                </tr>
                                            @empty
                                                <tr role="row" class="filter">
                                                    <td colspan="10">No Data Available</td>
                                                </tr>
                                            @endforelse
                                          </tbody>
                                      </table>
                                      @else
                                      <div class="row">
                                          <div class="col-ms-12 text-center">No Data Available, please add above button</div>
                                      </div>
                                      @endif
                                  </div>
                              </div>
                              <!-- END EXAMPLE TABLE PORTLET-->
                          </div>

        </div>
    <!-- END CONTENT BODY -->
</div>
@endsection
@section('page_plugin_js')
<script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="https://cdn.datatables.net/plug-ins/1.10.16/filtering/row-based/range_dates.js" type="text/javascript"></script>

    @endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
@endsection
