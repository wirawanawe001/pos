@extends('dashboard.layouts.layout')
@section('page_css')
<style media="screen">
  .dashboard-stat2{
    padding: 15px 15px 15px!important;
  }
</style>
@endsection
@section('content')
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="background: #EEEEEE;">
        <!-- BEGIN PAGE HEADER-->
        <!-- BEGIN PAGE BAR -->
        <div class="page-bar">
            <ul class="page-breadcrumb">
                <li>
                    <a href="index.html">Home</a>
                    <i class="fa fa-circle"></i>
                </li>
                <li>
                    <span>Dashboard</span>
                </li>
            </ul>
        </div>
        <!-- END PAGE BAR -->
        <!-- BEGIN PAGE TITLE-->
        <h1 class="page-title"> Dashboard
            <small>statistics, recent and reports</small>
        </h1>
        <!-- END PAGE TITLE-->
        <!-- END PAGE HEADER-->
        <!-- BEGIN ROW -->
        <div class="row">
            <div class="col-lg-6 col-md-12">
              <div class="row">
                <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-green-sharp">
                                    <small class="font-green-sharp">Rp</small>
                                    <span data-counter="counterup" data-value="{{$todaySales->first()['total']}}">0</span>
                                </h3>
                                <small>TODAY'S SALES</small>
                            </div>
                            <div class="icon">
                                <i class="fa fa-sort-asc" style="color:green;font-size:3em;"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-3 col-sm-6 col-xs-12">
                    <div class="dashboard-stat2 ">
                        <div class="display">
                            <div class="number">
                                <h3 class="font-blue-sharp">
                                  <small class="font-green-sharp">Rp</small>
                                    <span data-counter="counterup" data-value="{{$yesterdaySales->first()['total']}}"></span>
                                </h3>
                                <small>YESTERDAY'S SALES</small>
                            </div>
                            <div class="icon">
                                <i class="fa fa-sort-desc" style="color:red;font-size:3em;"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <!-- BEGIN CHART PORTLET-->
                  <div class="portlet light bordered">
                    <div class="portlet-body">
                      <div id="sales" class="chart" style="height: 350px;"> </div>
                    </div>
                  </div>
                  <!-- END CHART PORTLET-->
              </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-12">
              <div class="row">
                <div class="col-lg-12 col-xs-12 col-sm-12">
                    <!-- BEGIN PORTLET-->
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption">
                                <i class="icon-globe font-dark hide"></i>
                                <span class="caption-subject font-dark bold uppercase">Recent Orders</span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <!--BEGIN TABS-->
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_1_1">
                                    <div class="scroller" style="height: 339px;" data-always-visible="1" data-rail-visible="0">
                                        <ul class="feeds">
                                          @foreach($recentUpdateOrders as $recentUpdate)
                                          <li>
                                            <div class="col1">
                                                <div class="cont">
                                                    <div class="cont-col1">
                                                        <div class="label label-sm label-success">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </div>
                                                    </div>
                                                    <div class="cont-col2">
                                                        <div class="desc"> New order #<a href="{{route('back.orderDetail', $recentUpdate['id'])}}">{{$recentUpdate->order_code}}</a>. Please take care of it
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                          </li>
                                          @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!--END TABS-->
                        </div>
                    </div>
                    <!-- END PORTLET-->
                </div>
              </div>
            </div>
        </div>
        <!-- END ROW -->
        <!-- BEGIN ROW -->
        <div class="row">
          <div class="col-lg-6 col-xs-12 col-sm-12">
              <div class="portlet light ">
                  <div class="portlet-title">
                      <div class="caption">
                          <i class="icon-bubble font-dark hide"></i>
                          <span class="caption-subject font-hide bold uppercase">Recent Users</span>
                      </div>
                  </div>
                  <div class="portlet-body">
                      <div class="row">
                          @foreach($recentMembers as $recent)
                          <div class="col-md-4">
                              <!--begin: widget 1-1 -->
                              <div class="mt-widget-1">
                                  <div class="mt-img">
                                      <img src="{{ asset('') }}assets/pages/media/users/ava.png"> </div>
                                  <div class="mt-body">
                                      <h3 class="mt-username">{{$recent->name}}</h3>
                                      <p class="mt-user-title"> {{$recent->address}} </p>
                                  </div>
                              </div>
                              <!--end: widget 1-1 -->
                          </div>
                          @endforeach
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-6">
            <!-- BEGIN CHART PORTLET-->
            <div class="portlet light bordered">
              <div class="portlet-title">
                <div class="caption">
                  <i class="icon-bar-chart font-green-haze"></i>
                  <span class="caption-subject bold uppercase font-green-haze"> Top Product</span>
                </div>
              </div>
              <div class="portlet-body">
                <div id="top_product" class="chart" style="height: 400px;"> </div>
              </div>
            </div>
            <!-- END CHART PORTLET-->
          </div>
        </div>
        <!-- END ROW -->
    </div>
    <!-- END CONTENT BODY -->
</div>

@endsection
@section('page_plugin_js')
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
        <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
        <script src="{{ asset('assets/global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/horizontal-timeline/horizontal-timeline.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/jquery.vmap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.russia.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.world.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.europe.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.germany.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/maps/jquery.vmap.usa.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jqvmap/jqvmap/data/jquery.vmap.sampledata.js') }}" type="text/javascript"></script>
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
<script>
$(function(){
      var chart = AmCharts.makeChart("sales", {
        "type": "serial",
        "dataLoader": {
          "url": "{{route('api.order')}}"
        },
        "valueAxes": [{
          "gridColor": "#FFFFFF",
          "gridAlpha": 0.2,
          "dashLength": 0
        }],
        "gridAboveGraphs": true,
        "startDuration": 1,
        "graphs": [{
          "balloonText": "[[category]]: <b>[[value]]</b>",
          "bullet": "round",
          "bulletSize": 8,
          "lineThickness": 2,
          "lineColor": "#d1655d",
          "type": "smoothedLine",
          "valueField": "orders"
        }],
        "chartScrollbar": {
            "graph":"g1",
            "gridAlpha":0,
            "color":"#888888",
            "scrollbarHeight":25,
            "backgroundAlpha":0,
            "selectedBackgroundAlpha":0.1,
            "selectedBackgroundColor":"#888888",
            "graphFillAlpha":0,
            "autoGridCount":true,
            "selectedGraphFillAlpha":0,
            "graphLineAlpha":0.2,
            "graphLineColor":"#c2c2c2"
        },
        "chartCursor": {
          "categoryBalloonEnabled": false,
          "valueLineEnabled":true,
          "valueLineBalloonEnabled":true,
          "cursorAlpha": 0,
          "fullWidth":true
        },
        "categoryField": "date",
        "categoryAxis": {
          "parseDates": true,
          "minorGridAlpha": 0.1,
          "minorGridEnabled": true
        },
        "export": {
            "enabled": true
        }
      });

      var chart2 = AmCharts.makeChart("top_product", {
        "type": "pie",
        "dataLoader": {
          "url": "{{route('api.top')}}"
        },
        "valueField": "total",
        "titleField": "name",
      });
    });
</script>
@endsection
