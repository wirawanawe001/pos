<div class="page-sidebar-wrapper">
    <!-- BEGIN SIDEBAR -->
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
            <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
            <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
            <li class="heading">
                <h3 class="uppercase">E-Commerce</h3>
            </li>
            <li class="nav-item start {{$page_state=='Dashboard'?print 'active open':print ''}}">
                <a href="{{route('back')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Dashboard</span>
                </a>
            </li>
            <li class="nav-item start {{$page_state=='Inventory'?print 'active open':print ''}}">
                <a href="{{route('back.inventories')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Inventory</span>
                </a>
            </li>
            <li class="nav-item  {{$page_state=='Category'?print 'active open':print ''}}
            {{$page_state=='Product'?print 'active open':print ''}}
            {{$page_state=='Sku'?print 'active open':print ''}}
            {{$page_state=='Promotional'?print 'active open':print ''}}
            {{$page_state=='Featured'?print 'active open':print ''}}
            {{$page_state=='Featured Category'?print 'active open':print ''}}">
                <a href="javascript:;" class="nav-link nav-toggle">
                    <i class="icon-diamond"></i>
                    <span class="title">Menu</span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item {{$page_state=='Product'?print 'active open':print ''}}">
                        <a href="{{route('back.product')}}" class="nav-link ">
                            <span class="title">List</span>
                        </a>
                    </li>
                    <li class="nav-item {{$page_state=='Category'?print 'active open':print ''}}
                    {{$page_state=='Featured Category'?print 'active open':print ''}}">
                        <a class="nav-link nav-toggle">
                            <span class="title">Category</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">
                            <li class="nav-item {{$page_state=='Category'?print 'active open':print ''}}">
                                <a href="{{route('back.categories')}}" class="nav-link">
                                  <span class="title">List</span>
                                </a>
                            </li>
                            {{--<li class="nav-item {{$page_state=='Featured Category'?print 'active open':print ''}}">--}}
                                {{--<a href="{{route('back.category.featured')}}" class="nav-link">--}}
                                  {{--<span class="title">Featured</span>--}}
                                {{--</a>--}}
                            {{--</li>--}}
                        </ul>
                    </li>
                    <li class="nav-item  {{$page_state=='Pakage'?print 'active open':print ''}}">
                        <a href="{{route('back.pakages')}}" class="nav-link ">
                            <span class="title">Pakage</span>
                        </a>
                    </li>
                    {{--<li class="nav-item  {{$page_state=='Promotional'?print 'active open':print ''}}">--}}
                        {{--<a href="{{route('back.promotional')}}" class="nav-link ">--}}
                            {{--<span class="title">Promotional</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                    {{--<li class="nav-item  {{$page_state=='Featured'?print 'active open':print ''}}">--}}
                        {{--<a href="{{route('back.featured')}}" class="nav-link ">--}}
                            {{--<span class="title">Featured</span>--}}
                        {{--</a>--}}
                    {{--</li>--}}
                </ul>
            </li>
            <li class="nav-item start {{$page_state=='Table'?print 'active open':print ''}}">
                <a href="{{route('back.tables')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Table</span>
                </a>
            </li>
            <li class="nav-item start {{$page_state=='Order'?print 'active open':print ''}}">
                <a href="{{route('back.orders')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Orders</span>
                </a>
            </li>
            {{--<li class="nav-item start {{$page_state=='Payment'?print 'active open':print ''}}">--}}
                {{--<a href="{{route('back.payments')}}" class="nav-link">--}}
                    {{--<i class="icon-bar-chart"></i>--}}
                    {{--<span class="title">Payment Methods</span>--}}
                {{--</a>--}}
            {{--</li>--}}
            <li class="heading">
                <h3 class="uppercase">Website</h3>
            </li>
            <li class="nav-item start {{$page_state=='Slider'?print 'active open':print ''}}">
                <a href="{{route('back.home')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">Home Slide</span>
                </a>
            </li>
            <li class="nav-item start {{$page_state=='About'?print 'active open':print ''}}">
                <a href="{{route('back.about')}}" class="nav-link">
                    <i class="icon-bar-chart"></i>
                    <span class="title">About</span>
                </a>
            </li>
            <!-- BLOG and TnC -->

        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
