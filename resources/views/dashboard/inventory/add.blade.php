@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Inventory</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.inventories')}}">List</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Inventory </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.inventory.add.post')}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name" placeholder="" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Quantity:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control" maxlength="6" name="qty" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57||event.charCode ==8" required>
                                                <span class="input-group-addon" style="padding: 0px;">
                                                    <select  name="weight" style="border: 0px;background-color: transparent;" required>
                                                        <option value="">Select...</option>
                                                        <option value="gram">gram</option>
                                                        <option value="mililiter">mililiter</option>
                                                    </select>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-2">Expired Date</label>
                                        <div class="col-md-3">
                                            <div class="input-group input-medium date date-picker" data-date-format="dd-mm-yyyy" data-date-start-date="+0d">
                                                <input type="text" class="form-control" name="expired" readonly>
                                                <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                          <i class="fa fa-calendar"></i>
                                        </button>
                                      </span>
                                            </div>
                                            <!-- /input-group -->
                                            <span class="help-block"> Select date </span>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Total Price:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4">
                                          <div class="input-group">
                                              <span class="input-group-addon"><i>Rp</i></span>
                                              <input type="text" class="form-control" maxlength="9" name="total_price" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57||event.charCode ==8" required>
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.inventories')}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
            <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/clockface/js/clockface.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
@endsection
