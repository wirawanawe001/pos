@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Inventory</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.inventories')}}">List</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Inventory </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group pull-right">
                                            <a href="{{route('back.inventory.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(count($inventories))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="5%"> # </th>
                                    <th width="10%"> Name </th>
                                    <th width="10%"> Quantity </th>
                                    <th width="10%"> Unit Price </th>
                                    <th width="10%"> Expired Date </th>
                                    <th width="20%"> Action? </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($inventories as $indexKey => $inventory)
                                    <tr role="row" class="filter">
                                        <td>{{$indexKey + 1}}</td>
                                        <td>{{$inventory->name}}</td>
                                        <td>{{$inventory->qty}} {{$inventory->weight}}</td>
                                        <td>@php echo "Rp ".number_format($inventory->unit_price,0,',','.') @endphp</td>
                                        <td>@php echo date("d M Y",strtotime($inventory->expired)); @endphp</td>
                                        <td>
                                            <a href="{{route('back.inventory.delete', $inventory->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                                <i class="fa fa-trash"></i>
                                            </a>
                                            <a href="{{route('back.inventory.edit', $inventory->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                                <i class="fa fa-pencil"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="row">
                                <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
      </div>
        @endsection

        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
        @endsection
