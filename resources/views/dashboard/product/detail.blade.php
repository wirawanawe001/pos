@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Menu</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Detail</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Menu </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
              <div class="col-md-12">
                <div class="tabbable-line boxless tabbable-reversed">
                  <div class="tab-content">
                    <div class="tab-pane active" id="about">
                      <div class="portlet light bordered">
                        <div class="portlet-body form">
                          <!-- BEGIN FORM-->
                          <form class="form-horizontal" role="form">
                            <div class="form-body">
                              <h2 class="margin-bottom-20"> View About Information </h2>
                              <h3 class="form-section">Menu Info</h3>
                              <div class="row">
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Name:</label>
                                    <div class="col-md-9">
                                      <p class="form-control-static"> {{$product['name']}} </p>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Description:</label>
                                    <div class="col-md-9">
                                      <p class="form-control-static"> {{$product['description']}} </p>
                                    </div>
                                  </div>
                                </div>
                                <!--/span-->
                                <div class="col-md-6">
                                  <div class="form-group">
                                    <label class="control-label col-md-3"> Cost Price:</label>
                                    <div class="col-md-9">
                                      <p class="form-control-static"> @php echo "Rp ".number_format($product['cost_price'],0,',','.') @endphp </p>
                                    </div>
                                  </div>
                                </div>
                                  <div class="col-md-6">
                                      <div class="form-group">
                                          <label class="control-label col-md-3"> Menu Price:</label>
                                          <div class="col-md-9">
                                              <p class="form-control-static"> @php echo "Rp ".number_format($product['menu_price'],0,',','.') @endphp </p>
                                          </div>
                                      </div>
                                  </div>
                                <!--/span-->
                              </div>
                              <div class="form-actions">
                                <div class="row">
                                  <div class="col-md-12">
                                    <div class="row">
                                      <div class="col-md-offset-9 col-md-3">
                                        <a href="{{route('back.product.edit',$product['id'])}}" type="submit" class="btn yellow">
                                          <i class="fa fa-pencil"></i> Edit
                                        </a>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                              <!--/row-->
                              <h3 class="form-section">Images Product</h3>
                              <div class="row">
                                <div class="col-md-12">
                                  <!-- BEGIN EXAMPLE TABLE PORTLET-->
                                  <div class="portlet-body">
                                    @if($imageProducts->count() < 7)
                                    <div class="table-toolbar">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="btn-group pull-right">
                                            <a href="{{route('back.imageProduct.add',$product['id'])}}" id="sample_editable_1_new" class="btn green "> Add New
                                              <i class="fa fa-plus"></i>
                                            </a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    @endif
                                        <div class="mt-element-card mt-element-overlay">
                                          <div class="row">
                                            @foreach($imageProducts as $indexKey=>$imageProduct)
                                                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                                                    <div class="mt-card-item">
                                                        <div class="mt-card-avatar mt-overlay-1">
                                                            <img src="{{ asset('uploads') }}/{{$imageProduct->url}}" />
                                                            <div class="mt-overlay">
                                                                <ul class="mt-info">
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="{{route('back.imageProduct.edit',[$product['id'],$imageProduct->id])}}">
                                                                            <i class="fa fa-pencil"></i>
                                                                        </a>
                                                                    </li>
                                                                    @if($imageProducts->count() > 2)
                                                                    <li>
                                                                        <a class="btn default btn-outline" href="{{route('back.imageProduct.delete',[$product['id'],$imageProduct->id])}}">
                                                                            <i class="fa fa-trash"></i>
                                                                        </a>
                                                                    </li>
                                                                    @endif
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                            </div>
                                        </div>
                                    </div>
                                  <!-- END EXAMPLE TABLE PORTLET-->
                                </div>
                              </div>
                                <h3 class="form-section">Inventory Product</h3>
                                <div class="row">
                                    <div class="portlet light bordered">
                                        <div class="portlet-body">
                                            <div class="table-toolbar">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="btn-group pull-right">
                                                            <a href="{{route('back.inventoryProduct.add',$product['id'])}}" id="sample_editable_1_new" class="btn green"> Add New
                                                                <i class="fa fa-plus"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @if(count($product['inventoryProduct']))
                                                <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                                    <thead>
                                                    <tr role="row" class="heading">
                                                        <th width="5%"> # </th>
                                                        <th width="10%"> Name </th>
                                                        <th width="10%"> Quantity </th>
                                                        <th width="10%"> Unit Price </th>
                                                        <th width="20%"> Action? </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach($product->inventoryProduct as $indexKey => $invP)
                                                        <tr role="row" class="filter">
                                                            <td>{{$indexKey + 1}}</td>
                                                            <td>{{$invP->inventory['name']}}</td>
                                                            <td>{{$invP->qty}} {{$invP->inventory['weight']}}</td>
                                                            <td>{{$invP->inventory['unit_price']}}</td>
                                                            <td>
                                                                <a href="{{route('back.inventoryProduct.delete', [$product->id, $invP->id])}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                                                    <i class="fa fa-trash"></i>
                                                                </a>
                                                                <a href="{{route('back.inventoryProduct.edit', [$product->id, $invP->id])}}" id="sample_editable_1_new" class="btn sbold yellow"> Edit
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            @else
                                                <div class="row">
                                                    <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                          </form>
                          <!-- END FORM-->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
      </div>
        @endsection

        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
        @endsection
