@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Product</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Detail</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Edit</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Products </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.product.edit.post', $product->id)}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Name:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" name="name" placeholder="Enter Product name" value="{{$product['name']}}" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-8">
                                            <textarea class="form-control" name="description" required>{{$product['description']}}</textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Ingredients:
                                        </label>
                                        <div class="col-md-4">
                                            <a href="{{route('back.product.view',$product['id'])}}" class="btn btn-warning">Click here to redirect to product detail</a>
                                        </div>
                                    </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Categories:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-5">
                                            <div class="form-control height-auto">
                                                <div class="" style="" data-always-visible="1">
                                                    <ul class="list-unstyled" style="margin-left: 2px;">
                                                        @forelse($categories as $category)
                                                            <li>
                                                                <label>
                                                                    <input type="radio" name="category" value="{{$category['id']}}" @if($product->productCategory['category']['id']==$category['id']) checked @endif  required> {{$category['name']}}
                                                                </label>
                                                            </li>
                                                        @empty
                                                            <div class="margin-bottom-5">
                                                                <input type="text" name="product_categories" style="position:absolute;z-index:-1;" required>
                                                                <a href="{{route('back.category.add')}}" class="btn btn-sm btn-success filter-submit margin-bottom">Add Category</a>
                                                            </div>
                                                        @endforelse
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Price:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="col-md-4 input-group">
                                            <span class="input-group-addon">Rp</span>
                                            <input type="text" class="form-control" maxlength="6" value="{{$product['menu_price']}}" name="menu_price" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                        </div>
                                    </div>
                                    <div class="form-group image">
                                        <label class="col-md-2 control-label">Image(s):
                                        </label>
                                        <div class="col-md-4">
                                            <a href="{{route('back.product.view',$product['id'])}}" class="btn btn-warning">Click here to redirect to product detail</a>
                                        </div>
                                    </div>
                                    <!--<div class="form-group">
                                      <label class="col-md-2 control-label"></label>
                                      <div class="col-md-4">
                                        <button type="button" name="addImg" class="btn btn-default blue"><i class="fa fa-plus"></i>Add Another</button>
                                      </div>
                                    </div>-->
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.product.view',$product['id'])}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script>
                $(function(){
                    $("div.shoes").css("display","none");
                    $("div.clothes").css("display","none");
                    $( "input[name=choices]" ).on( "click", function(){
                        if($( "input[name=choices]:checked" ).val()=="clothes"){
                            $("div.shoes").remove();
                            $("div.choice").after('<div class="form-group clothes">'+
                                '<label class="col-md-2 control-label">'+
                                '</label>'+
                                '<!-- CLOTHES SIZE -->'+
                                '<div class="col-md-10">'+
                                '<div class="mt-checkbox-inline">'+
                                '<label for="checkboxS" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkboxS" value="s" name="size[]">S'+
                                '<span></span>'+
                                '</label>'+
                                '<label for="checkboxM" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkboxM" value="m" name="size[]">M'+
                                '<span></span>'+
                                '</label>'+
                                '<label for="checkboxL" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkboxL" value="l" name="size[]">L'+
                                '<span></span>'+
                                '</label>'+
                                '<label for="checkboxXL" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkboxXL" value="xl" name="size[]">XL'+
                                '<span></span>'+
                                '</label>'+
                                '<label for="checkboxAll" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkboxAll" value="all" name="size[]">All'+
                                '<span></span>'+
                                '</label>'+
                                '</div>'+
                                '</div>'+
                                '<!-- END CLOTHES SIZE -->'+
                                '</div>');
                        }else if($( "input[name=choices]:checked" ).val()=="shoes"){
                            $("div.clothes").remove();
                            $("div.choice").after('<div class="form-group shoes">'+
                                '<label class="col-md-2 control-label">'+
                                '</label>'+
                                '<!-- CLOTHES SIZE -->'+
                                '<div class="col-md-10">'+
                                '<div class="mt-checkbox-inline">'+
                                '@for($i=38; $i < 45; $i++)'+
                                '<label for="checkbox{{$i}}" class="mt-checkbox">'+
                                '<input type="checkbox" id="checkbox{{$i}}" value="{{$i}}" name="size[]">{{$i}}'+
                                '<span></span>'+
                                '</label>'+
                                '@endfor'+
                                '</div>'+
                                '</div>'+
                                '<!-- END CLOTHES SIZE -->'+
                                '</div>');
                        }else{
                            $("div.shoes").remove();
                            $("div.clothes").remove();
                        }
                    });

                    $( "button[name=addIng]" ).on( "click" , function(){
                        $("div.ing").after('<div class="form-group">'+
                            '<label class="col-md-2 control-label">'+
                            '</label>'+
                            '<div class="col-md-2">'+
                            '<select class="form-control" name="inventory">'+
                            '<option value="">Select...</option>'+
                            '@foreach($inventories as $inv)'+
                            '<option value="{{$inv['id']}}">{{$inv['name']}}</option>'+
                            '@endforeach'+
                            '</select>'+
                            '</div>'+
                            '<div class="col-md-4">' +
                            '<label class="col-md-2 control-label">Qty:</label>'+
                            '<div class="col-md-8">'+
                            '<div class="input-group">'+
                            '<input type="text" class="form-control" maxlength="6" name="qty" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>'+
                            '<span class="input-group-addon" style="padding: 0px;">'+
                            '<select  name="weight" style="border: 0px;background-color: transparent;font-size:12px;" required>'+
                            '<option value="">Select</option>'+
                            '<option value="gram">gram</option>'+
                            '<option value="mililiter">mililiter</option>'+
                            '</select>'+
                            '</span>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '<div class="col-md-4">' +
                            '<label class="col-md-2 control-label">Price: </label>'+
                            '<div class="col-md-8">'+
                            '<div class="input-group">'+
                            '<span class="input-group-addon">Rp</span>'+
                            '<input type="text" class="form-control" maxlength="6" name="cost_price" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>'+
                            '</div>'+
                            '</div>'+
                            '</div>'+
                            '</div>');
                    });

                    $( "button[name=addImg]" ).on( "click" , function(){
                        $("div.image").after('<div class="form-group">'+
                            '<label class="col-md-2 control-label">Image(s):'+
                            '<span class="required"> * </span>'+
                            '</label>'+
                            '<div class="col-md-4">'+
                            '<input type="file" class="form-control" name="image[]" placeholder="Enter Image">'+
                            '<span class="help-block"> A block of help text. </span>'+
                            '</div>'+
                            '</div>');
                    });

                    $(document).on( "click" , ".deleteImg", function(){
                        $(this).parents("div").eq(1).remove();
                    });
                });


            </script>
@endsection
@section('page_plugin_js')
@endsection
@section('page_js')
<script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.fileupload.js') }}" type="text/javascript"></script>

<script src="/js/vendor/jquery.ui.widget.js"></script>
<script src="/js/jquery.iframe-transport.js"></script>
<script src="/js/jquery.fileupload.js"></script>
@endsection
