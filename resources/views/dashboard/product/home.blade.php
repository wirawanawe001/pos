@extends('dashboard.layouts.layout')
@section('page_plugin_css')
    <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Product</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.product')}}">List</a>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Products </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN EXAMPLE TABLE PORTLET-->
                    <div class="portlet light bordered">
                        <div class="portlet-body">
                            <div class="table-toolbar">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="btn-group pull-right">
                                            <a href="{{route('back.product.add')}}" id="sample_editable_1_new" class="btn green"> Add New
                                                <i class="fa fa-plus"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @if(count($products))
                            <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="5%"> # </th>
                                    <th width="10%"> Image </th>
                                    <th width="10%"> Name </th>
                                    <th width="10%"> Category </th>
                                    <th width="10%"> Cost Price </th>
                                    <th width="10%"> Menu Price </th>
                                    <th width="20%"> Action? </th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($products as $indexKey => $product)
                                    <tr role="row" class="filter">
                                        <td>{{$indexKey + 1}}</td>
                                        <td><img class="imgupload" src="{{ asset('uploads') }}/{{$product->imageProduct->first()['url']}}" width="100%"></td>
                                        <td>{{$product['name']}}</td>
                                        <td>{{$product->productCategory['category']['name']}}</td>
                                        <td>@php echo "Rp ".number_format($product->cost_price,0,',','.') @endphp</td>
                                        <td>@php echo "Rp ".number_format($product->menu_price,0,',','.') @endphp</td>
                                        <td>
                                          <a href="{{route('back.product.view',$product->id)}}" id="sample_editable_1_new" class="btn sbold yellow"> View
                                              <i class="fa fa-eye"></i>
                                          </a>
                                            <a href="{{route('back.product.delete', $product->id)}}" id="sample_editable_1_new" class="btn sbold red"> Delete
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            @else
                            <div class="row">
                                <div class="col-ms-12 text-center">No Data Available, please add with above buttton</div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!-- END EXAMPLE TABLE PORTLET-->
                </div>

            </div>
            <!-- END CONTENT BODY -->
        </div>
      </div>
        @endsection

        @section('page_plugin_js')
            <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
            <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/table-datatables-managed.js') }}" type="text/javascript"></script>
        @endsection
