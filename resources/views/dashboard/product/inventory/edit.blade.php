@extends('dashboard.layouts.layout')
@section('page_plugin_css')
@endsection
@section('page_css')
@endsection
@section('content')
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER-->
            <!-- BEGIN PAGE BAR -->
            <div class="page-bar">
                <!-- BEGIN PAGE BREADCRUMB -->
                <ul class="page-breadcrumb">
                    <li>
                        <a href="{{route('back')}}">Home</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Inventory</span>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <a href="{{route('back.inventories')}}">List</a>
                        <i class="fa fa-circle"></i>
                    </li>
                    <li>
                        <span>Add</span>
                    </li>
                </ul>
                <!-- END PAGE BREADCRUMB -->
            </div>
            <!-- END PAGE BAR -->
            <!-- BEGIN PAGE TITLE-->
            <h1 class="page-title"> Inventory </h1>
            <!-- END PAGE TITLE-->
            <!-- END PAGE HEADER-->
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <form class="form-horizontal " method="post" action="{{route('back.inventoryProduct.edit.post', [$product_id ,$inventory_product_id])}}" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-body">
                                    <div class="form-group ing">
                                        <label class="col-md-2 control-label">Ingredients:
                                            <span class="required"> * </span>
                                        </label>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label"></label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="inventory">
                                                    @foreach($inventory as $indexKey => $inv)
                                                    <option value="{{$inv['id']}}" {{$inv->id==$inventory_product->inventory_id?"selected":""}}>{{$inv->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label class="col-md-2 control-label">Qty:</label>
                                                <div class="col-md-8">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" maxlength="6" name="qty" value="{{$inventory_product->qty}}" placeholder="" onkeypress="return event.charCode >= 48 && event.charCode <= 57" required>
                                                        <span class="input-group-addon" style="padding: 0px;">
                                                        <select  name="weight" style="border: 0px;background-color: transparent;font-size:12px;" required>
                                                            <option value="">Select</option>
                                                            <option value="gram" {{$inventory_product->inventory->weight=="gram"?"selected":""}}>gram</option>
                                                            <option value="mililiter" {{$inventory_product->inventory->weight==="mililiter"?"selected":""}}>mililiter</option>
                                                        </select>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-4">
                                            <button type="submit" class="btn green">Submit</button>
                                            <a href="{{route('back.product.view', $product_id)}}" class="btn default">Cancel</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!-- END FORM-->
                        </div>
                    </div>
                </div>
            </div>
            <!-- END CONTENT BODY -->
        </div>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
        @endsection
        @section('page_plugin_js')
        @endsection
        @section('page_js')
            <script src="{{ asset('assets/pages/scripts/form-samples.js') }}" type="text/javascript"></script>
            <script src="{{ asset('js/vendor/jquery.ui.widget.js') }}" type="text/javascript"></script>
            <script src="{{ asset('js/jquery.iframe-transport.js') }}" type="text/javascript"></script>
            <script src="{{ asset('js/jquery.fileupload.js') }}" type="text/javascript"></script>

            <script src="/js/vendor/jquery.ui.widget.js"></script>
            <script src="/js/jquery.iframe-transport.js"></script>
            <script src="/js/jquery.fileupload.js"></script>
@endsection
