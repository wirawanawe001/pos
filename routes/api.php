<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/order', 'Api\OrderController@list')->name('api.order');
Route::get('/top_product', 'Api\OrderController@top')->name('api.top');
Route::get('/timer', 'Api\PromotionalController@getTimer')->name('api.timer');

Route::prefix('/category')->group(function(){
  Route::get('/', 'Api\CategoryController@index')->name('api.category');
  Route::get('/featured', 'Api\CategoryController@featured')->name('api.category.featured');
});
Route::prefix('/product')->group(function(){
  Route::get('/', 'Api\ProductController@index')->name('api.product');
  Route::get('/category/{category_id}', 'Api\ProductController@byCategory')->name('api.product.category');
  Route::get('/sale', 'Api\ProductController@onSale')->name('api.product.featured');
  Route::get('/new', 'Api\ProductController@new')->name('api.product.featured');
  Route::get('/featured', 'Api\ProductController@featured')->name('api.product.featured');
});
Route::prefix('/brand')->group(function(){
  Route::get('/', 'Api\BrandController@index')->name('api.brand');
});
Route::get('/contact', 'Api\ContactController@index')->name('api.contact');
