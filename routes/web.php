<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'LandingController@table')->name('table');


Route::prefix('/menu')->group(function () {
    Route::get('/{order_id}', 'LandingController@menu')->name('menu');
    Route::get('/edit/{order_id}', 'LandingController@edit')->name('edit');
});

Route::prefix('/bill')->group(function(){
    Route::get('/{order_id}', 'LandingController@bill')->name('bill');
    Route::post('/{order_id}', 'OrderDetailController@add')->name('orderDetail.add');
    Route::post('/edit/{order_id}', 'OrderDetailController@update')->name('orderDetail.edit');
});

Route::prefix('/menu2')->group(function () {
    Route::get('/{order_id}', 'LandingController@menu2')->name('menu2');
});



//BEGIN FRONT END//
Route::get('/user/login', 'LandingController@login')->name('ulogin');
Route::get('/user/register', 'FrontRegister@register')->name('uregister');
Route::post('/user/register', 'FrontRegister@registerPost')->name('uregister.post');
Route::prefix('/account')->group(function () {
    Route::get('/', 'UserController@account')->name('account');
    Route::post('/edit', 'UserController@update')->name('account.edit');
});


Route::prefix('/order')->group(function () {
    Route::get('/', 'OrderController@index')->name('orders');
    Route::get('/detail/{id}', 'OrderDetailController@index')->name('orderDetail');
    Route::post('/add', 'OrderController@add')->name('order.add.post');
    Route::post('/update/{order_id}', 'OrderController@update')->name('order.add.update');
    Route::get('/{order_id}', 'OrderController@show')->name('order.view');
    Route::get('/status/{order_id}', 'OrderController@status')->name('order.status');
    Route::get('/delete/{order_id}', 'OrderController@delete')->name('order.delete');
    Route::get('/detail/delete/{orderDetail_id}', 'OrderDetailController@destroy')->name('orderDetail.delete');
});

Route::post('/print/{order}', 'OrderController@printBill')->name('order.add.print');


/*Route::get('/checkout', 'OrderController@index')->name('checkout');
Route::post('/checkout', 'OrderController@order')->name('checkout.post');*/


Route::post('/send', 'ContactController@send')->name('send');
//END FRONT END//

Auth::routes();
//BEGIN DASHBOARD//
Route::prefix('/back')->middleware(['auth', 'role:admin|owner'])->group(function () {
    Route::get('/', 'Dashboard\HomeController@index')->name('back');

    Route::prefix('/about')->group(function () {
        Route::get('/', 'Dashboard\AboutController@index')->name('back.about');
        Route::get('/edit', 'Dashboard\AboutController@edit')->name('back.about.edit');
        Route::post('/edit', 'Dashboard\AboutController@update')->name('back.about.edit.post');
        Route::prefix('/point')->group(function () {
            Route::get('/add', 'Dashboard\AboutPointController@store')->name('back.about_point.add');
            Route::post('/add', 'Dashboard\AboutPointController@store')->name('back.about_point.add.post');
            Route::get('/edit/{point_id}', 'Dashboard\AboutPointController@edit')->name('back.about_point.edit');
            Route::post('/edit/{point_id}', 'Dashboard\AboutPointController@update')->name('back.about_point.edit.post');
            Route::get('/delete/{point_id}', 'Dashboard\AboutController@destroy')->name('back.about_point.delete');
        });
    });

    Route::prefix('/contact')->group(function () {
        Route::get('/add', 'Dashboard\ContactController@store')->name('back.contact.add');
        Route::post('/add', 'Dashboard\ContactController@store')->name('back.contact.add.post');
        Route::get('/edit/{contact_id}', 'Dashboard\ContactController@edit')->name('back.contact.edit');
        Route::post('/edit/{contact_id}', 'Dashboard\ContactController@update')->name('back.contact.edit.post');
        Route::get('/delete/{contact_id}', 'Dashboard\ContactController@destroy')->name('back.contact.delete');
    });

    Route::prefix('/sosmed')->group(function () {
        Route::get('/add', 'Dashboard\SosmedController@store')->name('back.sosmed.add');
        Route::post('/add', 'Dashboard\SosmedController@store')->name('back.sosmed.add.post');
        Route::get('/edit/{sosmed_id}', 'Dashboard\SosmedController@edit')->name('back.sosmed.edit');
        Route::post('/edit/{sosmed_id}', 'Dashboard\SosmedController@update')->name('back.sosmed.edit.post');
        Route::get('/delete/{sosmed_id}', 'Dashboard\SosmedController@destroy')->name('back.sosmed.delete');
    });

    Route::prefix('/order')->group(function () {
        Route::get('/', 'Dashboard\OrderController@index')->name('back.orders');
        Route::get('/detail/{id}', 'Dashboard\OrderDetailController@index')->name('back.orderDetail');
        Route::get('/{order_id}', 'Dashboard\OrderController@show')->name('back.order.view');
        Route::get('/status/{order_id}', 'Dashboard\OrderController@status')->name('back.order.status');
        Route::get('/delete/{order_id}', 'Dashboard\OrderController@delete')->name('back.order.delete');
    });

    Route::prefix('/address')->group(function () {
        Route::get('/add', 'Dashboard\AddressController@store')->name('back.address.add');
        Route::post('/add', 'Dashboard\AddressController@store')->name('back.address.add.post');
        Route::get('/edit/{address_id}', 'Dashboard\AddressController@edit')->name('back.address.edit');
        Route::post('/edit/{address_id}', 'Dashboard\AddressController@update')->name('back.address.edit.post');
        Route::get('/delete/{address_id}', 'Dashboard\AddressController@destroy')->name('back.address.delete');
    });

    Route::prefix('/home')->group(function () {
        Route::get('/', 'Dashboard\SliderController@index')->name('back.home');
        Route::get('/add', 'Dashboard\SliderController@store')->name('back.home.add');
        Route::post('/add', 'Dashboard\SliderController@store')->name('back.home.add.post');
        Route::get('/edit/{home_id}', 'Dashboard\SliderController@edit')->name('back.home.edit');
        Route::post('/edit/{home_id}', 'Dashboard\SliderController@update')->name('back.home.edit.post');
        Route::get('/delete/{home_id}', 'Dashboard\SliderController@destroy')->name('back.home.delete');
    });

    //BEGIN PRODUCT//
    Route::prefix('/product')->group(function () {
        Route::get('/', 'Dashboard\ProductController@index')->name('back.product');
        Route::get('/add', 'Dashboard\ProductController@store')->name('back.product.add');
        Route::post('/add', 'Dashboard\ProductController@store')->name('back.product.add.post');
        Route::prefix('/{product_id}')->group(function ($product_id) {
            Route::get('/', 'Dashboard\ProductController@show')->name('back.product.view');
            Route::prefix('/image')->group(function ($product_id) {
                Route::get('/add', 'Dashboard\ImageProductController@add')->name('back.imageProduct.add');
                Route::post('/add', 'Dashboard\ImageProductController@add')->name('back.imageProduct.add.post');
                Route::get('/edit/{image_id}', 'Dashboard\ImageProductController@edit')->name('back.imageProduct.edit');
                Route::post('/edit/{image_id}', 'Dashboard\ImageProductController@update')->name('back.imageProduct.edit.post');
                Route::get('/delete/{image_id}', 'Dashboard\ImageProductController@destroy')->name('back.imageProduct.delete');
            });
            Route::prefix('/inventoryProduct')->group(function ($product_id) {
                Route::get('/add', 'Dashboard\InventoryProductController@add')->name('back.inventoryProduct.add');
                Route::post('/add', 'Dashboard\InventoryProductController@add')->name('back.inventoryProduct.add.post');
                Route::get('/edit/{inventory_product_id}', 'Dashboard\InventoryProductController@edit')->name('back.inventoryProduct.edit');
                Route::post('/edit/{inventory_product_id}', 'Dashboard\InventoryProductController@update')->name('back.inventoryProduct.edit.post');
                Route::get('/delete/{inventory_product_id}', 'Dashboard\InventoryProductController@destroy')->name('back.inventoryProduct.delete');
            });
            Route::get('/edit', 'Dashboard\ProductController@edit')->name('back.product.edit');
            Route::post('/edit', 'Dashboard\ProductController@update')->name('back.product.edit.post');
            Route::get('/delete/', 'Dashboard\ProductController@destroy')->name('back.product.delete');
        });
    });
    //END PRODUCT//
    //BEGIN PAKAGE//
    Route::prefix('/pakage')->group(function () {
        Route::get('/', 'Dashboard\PakageController@index')->name('back.pakages');
        Route::get('/add', 'Dashboard\PakageController@store')->name('back.pakage.add');
        Route::post('/add', 'Dashboard\PakageController@store')->name('back.pakage.add.post');
        Route::prefix('/{pakage_id}')->group(function ($pakage_id) {
            Route::get('/', 'Dashboard\PakageController@show')->name('back.pakage.view');
            Route::prefix('/image')->group(function ($pakage_id) {
                Route::get('/add', 'Dashboard\ImageProductController@add')->name('back.imageProduct.add');
                Route::post('/add', 'Dashboard\ImageProductController@add')->name('back.imageProduct.add.post');
                Route::get('/edit/{image_id}', 'Dashboard\ImageProductController@edit')->name('back.imageProduct.edit');
                Route::post('/edit/{image_id}', 'Dashboard\ImageProductController@update')->name('back.imageProduct.edit.post');
                Route::get('/delete/{image_id}', 'Dashboard\ImageProductController@destroy')->name('back.imageProduct.delete');
            });
            Route::get('/edit', 'Dashboard\PakageController@edit')->name('back.pakage.edit');
            Route::post('/edit', 'Dashboard\PakageController@update')->name('back.pakage.edit.post');
            Route::get('/delete/', 'Dashboard\PakageController@destroy')->name('back.pakage.delete');
        });
    });
    //END PAKAGE//

    Route::prefix('/promotional')->group(function () {
        Route::get('/', 'Dashboard\PromotionalController@index')->name('back.promotional');
        Route::get('/add', 'Dashboard\PromotionalController@store')->name('back.promotional.add');
        Route::post('/add', 'Dashboard\PromotionalController@store')->name('back.promotional.add.post');
        Route::get('/{promotional_id}', 'Dashboard\PromotionalController@show')->name('back.promotional.view');
        Route::get('/edit/{promotional_id}', 'Dashboard\PromotionalController@edit')->name('back.promotional.edit');
        Route::post('/edit/{promotional_id}', 'Dashboard\PromotionalController@update')->name('back.promotional.edit.post');
    });

    Route::prefix('/featured')->group(function () {
        Route::get('/', 'Dashboard\FeaturedController@index')->name('back.featured');
        Route::get('/add', 'Dashboard\FeaturedController@store')->name('back.featured.add');
        Route::post('/add', 'Dashboard\FeaturedController@store')->name('back.featured.add.post');
        Route::get('/delete/{featured_id}', 'Dashboard\FeaturedController@destroy')->name('back.featured.delete');
    });

    Route::prefix('/category')->group(function () {
        Route::get('/', 'Dashboard\CategoryController@index')->name('back.categories');
        Route::get('/add', 'Dashboard\CategoryController@store')->name('back.category.add');
        Route::post('/add', 'Dashboard\CategoryController@store')->name('back.category.add.post');
        Route::get('/edit/{category_id}', 'Dashboard\CategoryController@edit')->name('back.category.edit');
        Route::post('/edit/{category_id}', 'Dashboard\CategoryController@update')->name('back.category.edit.post');
        Route::get('/delete/{category_id}', 'Dashboard\CategoryController@destroy')->name('back.category.delete');
        Route::prefix('/featured')->group(function () {
            Route::get('/', 'Dashboard\FeaturedCategoryController@index')->name('back.category.featured');
            Route::get('/add', 'Dashboard\FeaturedCategoryController@store')->name('back.category.featured.add');
            Route::post('/add', 'Dashboard\FeaturedCategoryController@store')->name('back.category.featured.add.post');
            Route::get('/edit/{cat_featured_id}', 'Dashboard\FeaturedCategoryController@edit')->name('back.category.featured.edit');
            Route::post('/edit/{cat_featured_id}', 'Dashboard\FeaturedCategoryController@edit')->name('back.category.featured.edit.post');
            Route::get('/delete/{cat_featured_id}', 'Dashboard\FeaturedCategoryController@destroy')->name('back.category.featured.delete');
        });

        Route::get('/{category_id}', 'Dashboard\CategoryController@show')->name('back.category.view');
    });


    Route::prefix('/table')->group(function () {
        Route::get('/', 'Dashboard\TableController@index')->name('back.tables');
        Route::get('/add', 'Dashboard\TableController@store')->name('back.table.add');
        Route::post('/add', 'Dashboard\TableController@store')->name('back.table.add.post');
        Route::get('/{table_id}', 'Dashboard\TableController@show')->name('back.table.view');
        Route::get('/edit/{table_id}', 'Dashboard\TableController@edit')->name('back.table.edit');
        Route::post('/edit/{table_id}', 'Dashboard\TableController@update')->name('back.table.edit.post');
        Route::get('/delete/{table_id}', 'Dashboard\TableController@destroy')->name('back.table.delete');
    });

    Route::prefix('/payment')->group(function () {
        Route::get('/', 'Dashboard\PaymentMethodController@index')->name('back.payments');
        Route::get('/add', 'Dashboard\PaymentMethodController@store')->name('back.payment.add');
        Route::post('/add', 'Dashboard\PaymentMethodController@store')->name('back.payment.add.post');
        Route::get('/{payment_id}', 'Dashboard\PaymentMethodController@show')->name('back.payment.view');
        Route::get('/edit/{payment_id}', 'Dashboard\PaymentMethodController@edit')->name('back.payment.edit');
        Route::post('/edit/{payment_id}', 'Dashboard\PaymentMethodController@update')->name('back.payment.edit.post');
        Route::get('/delete/{payment_id}', 'Dashboard\PaymentMethodController@destroy')->name('back.payment.delete');
    });

    Route::prefix('/inventory')->group(function () {
        Route::get('/', 'Dashboard\InventoryController@index')->name('back.inventories');
        Route::get('/add', 'Dashboard\InventoryController@store')->name('back.inventory.add');
        Route::post('/add', 'Dashboard\InventoryController@store')->name('back.inventory.add.post');
        Route::get('/edit/{inventory_id}', 'Dashboard\InventoryController@edit')->name('back.inventory.edit');
        Route::post('/edit/{inventory_id}', 'Dashboard\InventoryController@update')->name('back.inventory.edit.post');
        Route::get('/delete/{inventory_id}', 'Dashboard\InventoryController@destroy')->name('back.inventory.delete');
    });

    Route::prefix('/discount')->group(function () {
        Route::get('/', 'Dashboard\DiscountController@index')->name('back.discount');
        Route::get('/add', 'Dashboard\DiscountController@store')->name('back.discount.add');
        Route::post('/add', 'Dashboard\DiscountController@store')->name('back.discount.add.post');
        Route::get('/{discount_id}', 'Dashboard\DiscountController@show')->name('back.discount.view');
        Route::get('/edit/{discount_id}', 'Dashboard\DiscountController@edit')->name('back.discount.edit');
        Route::post('/edit/{discount_id}', 'Dashboard\DiscountController@update')->name('back.discount.edit.post');
    });

});
//END DASHBOARD//
Route::get('example', function () {
    return view('example');
});

//BEGIN SOCIAL AUTH//
Route::prefix('/auth')->group(function () {
    Route::get('/{provider}', 'Auth\AuthController@redirect')->name('oauth.redirect');
    Route::get('/callback/{provider}', 'Auth\AuthController@callback')->name('oauth.callback');
});
//END SOCIAL AUTH//
Route::get('/home', 'HomeController@index')->name('home');
