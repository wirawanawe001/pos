<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = ['author', 'content', 'image', 'title'];

    public function comment(){
      return $this->hasMany('App\Models\Comment');
    }
}
