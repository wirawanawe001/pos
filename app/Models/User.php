<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Models\Role;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'photo', 'phone', 'address', 'password', 'provider', 'provider_id', 'status', 'activated_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activated_token', 'provider', 'provider_id'
    ];
    public function role_user(){
      return $this->hasMany('App\Models\RoleUser');
    }

    public function role(){
      return $this->hasMany('App\Models\Role');
    }

    public function cart(){
      return $this->hasMany('App\Models\ShoppingCart');
    }

    public function wishlist(){
      return $this->hasMany('App\Models\Wishlist');
    }
}
