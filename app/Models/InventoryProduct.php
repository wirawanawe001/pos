<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InventoryProduct extends Model
{
    protected $fillable = ['inventory_id', 'product_id','qty'];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function inventory(){
        return $this->belongsTo('App\Models\Inventory');
    }
}
