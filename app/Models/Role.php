<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    protected $fillable = ['name', 'display_name', 'description'];

    public function role_user(){
      return $this->hasMany('App\Models\RoleUser');
    }

    public function user(){
      return $this->hasMany('App\Models\User');
    }
}
