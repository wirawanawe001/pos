<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $fillable = ['blog_id', 'email', 'name', 'content', 'commnet_id'];

  public function parent(){
    return $this->belongsTo('App\Models\Comment', 'comment_id');
  }

  public function children(){
    return $this->hasMany('App\Models\Comment', 'comment_id', 'id');
  }

  public function blog(){
    return $this->belongsTo('App\Models\Blog');
  }
}
