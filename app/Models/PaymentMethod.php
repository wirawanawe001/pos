<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model
{
    protected $fillable = ['type', 'account_number', 'account_name'];

    public function order(){
    	return $this->hasOne('App\Models\Order', 'payment_method_id', 'id');
    }
}
