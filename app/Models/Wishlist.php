<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    protected $fillable = ['user_id', 'sku_id', 'note', 'qty'];

    public function user(){
      return $this->belongsTo('App\Models\User');
    }

    public function sku(){
      return $this->belongsTo('App\Models\Sku');
    }
}
