<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'image', 'display_name', 'description'];

    public function parent(){
      return $this->belongsTo('App\Models\Category', 'category_id');
    }

    public function children(){
      return $this->hasMany('App\Models\Category', 'category_id', 'id');
    }

    public function productCategory(){
      return $this->hasMany('App\Models\ProductCategory');
    }

    public function product(){
      return $this->belongsToMany('App\Models\Product', 'product_categories');
    }

    public function featured(){
      return $this->hasOne('App\Models\CategoryFeatured', 'category_id');
    }

    public function brand(){
      return $this->belongsToMany('App\Models\Brand', 'brand_categories');
    }
}
