<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name', 'menu_price', 'cost_price','description'];

    public function productCategory(){
      return $this->hasOne('App\Models\ProductCategory');
    }

    public function inventoryProduct(){
        return $this->hasMany('App\Models\InventoryProduct');
    }

    public function pakage(){
        return $this->belongsTo('App\Models\Pakage');
    }
    public function pakageProduct(){
        return $this->hasMany('App\Models\PakageProduct');
    }

    public function category(){
      return $this->belongsToMany('App\Models\Category', 'product_categories');
    }

    public function imageProduct(){
      return $this->hasMany('App\Models\ImageProduct');
    }

    public function inventory(){
        return $this->hasMany('App\Models\Inventory');
    }

    public function cart(){
      return $this->hasMany('App\Models\ShoppingCart');
    }

    public function featured(){
      return $this->hasMany('App\Models\FeaturedProduct');
    }

    public function promotional(){
      return $this->hasOne('App\Models\PromotionalProduct');
    }

    public function brand(){
      return $this->belongsTo('App\Models\Brand');
    }
}
