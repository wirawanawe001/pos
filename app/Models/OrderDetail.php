<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
    protected $fillable = ['order_id', 'product_id','pakage_id', 'total_price', 'price','stage', 'qty', 'note'];

    public function table(){
        return $this->belongsTo('App\Models\Table');
    }

    public function order(){
      return $this->belongsTo('App\Models\Order');
    }
    public function product(){
        return $this->belongsTo('App\Models\Product');
    }
}
