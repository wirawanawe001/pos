<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Table extends Model
{
    protected $fillable = ['number', 'position', 'status'];

    public function order(){
        return $this->hasMany('App\Models\Order');
    }
}
