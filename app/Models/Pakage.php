<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pakage extends Model
{
    protected $fillable = ['name', 'image', 'cost_price', 'pakage_price','qty','description'];

    public function pakageProduct(){
        return $this->hasMany('App\Models\PakageProduct');
    }

    public function product(){
        return $this->hasMany('App\Models\Product');
    }
}
