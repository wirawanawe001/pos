<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    protected $fillable = ['name', 'qty','weight', 'expired','unit_price','total_price'];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function inventoryProduct(){
        return $this->hasMany('App\Models\InventoryProduct');
    }
}
