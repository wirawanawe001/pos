<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PakageProduct extends Model
{
    protected $fillable = ['product_id','pakage_id', 'qty'];

    public function product(){
        return $this->belongsTo('App\Models\Product');
    }

    public function pakage(){
        return $this->belongsTo('App\Models\Pakage');
    }
}
