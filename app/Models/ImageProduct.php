<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ImageProduct extends Model
{
    protected $fillable = ['product_id', 'url', 'thumb'];

    public function product(){
      return $this->belongsTo('App\Models\Product');
    }
}
