<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ShoppingCart extends Model
{
    protected $fillable = ['user_id','name', 'product_id','table_id', 'note', 'qty'];

    public function user(){
      return $this->belongsTo('App\Models\User');
    }

    public function table(){
        return $this->belongsTo('App\Models\Table');
    }

    public function product(){
      return $this->belongsTo('App\Models\Product');
    }
}
