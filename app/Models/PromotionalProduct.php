<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PromotionalProduct extends Model
{
    protected $fillable = ['product_id', 'description', 'promo_timer'];

    public function product(){
      return $this->belongsTo('App\Models\Product');
    }
}
