<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = ['category_id', 'product_id', 'name', 'description'];

    public function category(){
      return $this->belongsToMany('App\Models\Category', 'brand_categories');
    }

    public function product(){
      return $this->hasMany('App\Models\Product');
    }
}
