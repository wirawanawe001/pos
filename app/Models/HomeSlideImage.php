<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class HomeSlideImage extends Model
{
   protected $fillable = ['background', 'title', 'subtitle'];
}
