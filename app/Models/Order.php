<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Order extends Model
{
    use SoftDeletes;

    protected $fillable = ['user_id', 'order_code', 'table_id', 'name',  'total', 'status', 'payment', 'change','stage', 'discount', 'address', 'type', 'payment_method_id' ];

    public function paymentConfirmation(){
        return $this->hasOne('App\Models\PaymentConfirmation');
    }

    public function paymentMethod(){
    	return $this->belongsTo('App\Models\PaymentMethod', 'payment_method_id', 'id');
    }

    public function orderDetail(){
      return $this->hasMany('App\Models\OrderDetail');
    }

    public function table(){
        return $this->belongsTo('App\Models\Table');
    }
}
