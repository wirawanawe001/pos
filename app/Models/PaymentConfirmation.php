<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentConfirmation extends Model
{
    protected $fillable = ['blog_id', 'email', 'name', 'content', 'commnet_id'];
}
