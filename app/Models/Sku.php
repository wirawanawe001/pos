<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sku extends Model
{
    protected $fillable = ['sku', 'size', 'status', 'product_id', 'stock'];

    public function product(){
      return $this->belongsTo('App\Models\Product');
    }

    public function wishlist(){
      return $this->hasMany('App\Models\Wishlist');
    }

    public function cart(){
      return $this->hasMany('App\Models\ShoppingCart');
    }

    public function orderDetail(){
      return $this->hasMany('App\Models\OrderDetail');
    }
}
