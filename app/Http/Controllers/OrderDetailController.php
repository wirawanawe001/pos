<?php

namespace App\Http\Controllers;

use App\Models\InventoryProduct;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Auth;
use App\Models\Category;
use App\Models\Address;
use App\Models\Product;
use App\Models\Sosmed;
use App\Models\Table;
use App\Models\HomeSlideImage;
use App\Models\Contact;

class OrderDetailController extends Controller
{
    public function index()
    {
        $orderDetail = OrderDetail::all();
        return view('orderDetail')
            ->with('orderDetail', $orderDetail);

    }

    public function add(Request $request)
    {
        if (Auth::check()) {
            $orderByProduct = OrderDetail::where('product_id', $request->product)->where('order_id', $request->order)->where('stage',$request->stage)->first();

            if (count($orderByProduct) == 0) {
                $orderByProduct = OrderDetail::create([
                    'order_id' => $request->order,
                    'product_id' => $request->product,
                    'pakage_id' => $request->pakage,
                    'price' => $request->price,
                    'total_price' => $request->price * $request->qtybutton,
                    'qty' => $request->qtybutton,
                    'stage' => $request->stage,
                ]);
                $invProducts = $orderByProduct->product->inventoryProduct;
                foreach($invProducts as $invProduct){
                    $invProduct->inventory->qty = $invProduct->inventory->qty - ($invProduct->qty * $orderByProduct->qty);
                    $invProduct->inventory->save();
                }
            } else {
                $orderByProduct->qty = $orderByProduct->qty + $request->qtybutton;
                $invProducts = $orderByProduct->product->inventoryProduct;
                foreach($invProducts as $invProduct){
                    $invProduct->inventory->qty = $invProduct->inventory->qty - ($invProduct->qty * $request->qtybutton);
                    $invProduct->inventory->save();
                }
                $orderByProduct->save();
            }
            return redirect()->route('menu', [$request->order]);
        } else {
            return redirect()->route('ulogin');
        }
    }

    public function edit($orderDetail)
    {
        $orderDetail = OrderDetail::find($orderDetail);
        $table = $orderDetail['order']['table'];
        $categories = $orderDetail['order']['table'];
        return view('edit')
            ->with('orderDetail', $orderDetail)
            ->with('categories', $categories)
            ->with('table', $table);
    }

    public function update(Request $request)
    {
        $orderByProduct = OrderDetail::where('product_id', $request->product)->where('order_id', $request->order)->where('stage',$request->stage)->first();
        if (count($orderByProduct) == 0) {
            $orderByProduct = OrderDetail::create([
                'order_id' => $request->order,
                'product_id' => $request->product,
                'price' => $request->price,
                'total_price' => $request->price * $request->qtybutton,
                'qty' => $request->qtybutton,
                'stage' => $request->stage,
            ]);
            $invProducts = $orderByProduct->product->inventoryProduct;
            foreach($invProducts as $invProduct){
                $invProduct->inventory->qty = $invProduct->inventory->qty - ($invProduct->qty * $orderByProduct->qty);
                $invProduct->inventory->save();
            }
        } else {
            $orderByProduct->qty = $orderByProduct->qty + $request->qtybutton;
            $invProducts = $orderByProduct->product->inventoryProduct;
            foreach($invProducts as $invProduct){
                $invProduct->inventory->qty = $invProduct->inventory->qty - ($invProduct->qty * $request->qtybutton);
                $invProduct->inventory->save();
            }
            $orderByProduct->save();
        }
        return redirect()->route('edit', [$orderByProduct->order_id]);
    }

    public function destroy($orderDetail)
    {
        $detail = OrderDetail::find($orderDetail);
        $invProducts = $detail->product->inventoryProduct;
        foreach($invProducts as $invProduct){
            $invProduct->inventory->qty = $invProduct->inventory->qty + ($invProduct->qty * $detail->qty);
            $invProduct->inventory->save();
        }
        OrderDetail::destroy($orderDetail);
        return redirect()->back();
    }
}
