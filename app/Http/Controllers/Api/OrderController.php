<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetail;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function list(){
        $orders = Order::select(DB::raw('COUNT(order_code) as orders'), DB::raw('DATE(updated_at) as date'))->groupBy(DB::raw('DATE(updated_at)'))
        ->where('status', 1)
        ->get();
        return $orders;
    }

    public function top(){
      $tops = OrderDetail::join('skus', 'order_details.sku_id', '=', 'skus.id')
      ->join('products', 'skus.product_id', '=', 'products.id')
      ->select('products.name as name', DB:: raw('COUNT(sku_id) as total'))->groupBy('sku_id')
      ->orderBy('total', 'desc')
      ->get();
      return $tops;
    }
}
