<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\CategoryFeatured;
use Illuminate\Support\Facades\DB;

class CategoryController extends Controller
{
    public function index(){
      	$category = Category::with(['children' => function($c){
          $c->with(['children' => function($c){
            $c->with(['children'=> function($c){
              $c->with(['children']);
            }]);
          }]);
        }])->where('category_id',NULL)->get();

  		return response()->json($category);
    }

    public function find($category_id){
      $data = new \stdClass();

      $category = Category::find($category_id)->with(['product']);
      $data->category = $category;

      return response()->json([
  		  'status' => 'success',
  		  'data' => $data,
  		  'count' => count($data)
  		]);
    }

    public function featured(){
      $featured = CategoryFeatured::with(['category'])->get();
      return response()->json($featured);
    }
}
