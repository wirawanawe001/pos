<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use Illuminate\Support\Facades\DB;

class ContactController extends Controller
{
    public function index(){
      $data = new \stdClass();
      $contact = Contact::all();
      $data->contact = $contact;

      return response()->json([
          'status' => 'success',
          'data' => $data,
          'count' => count($data)
      ]);
    }
}
