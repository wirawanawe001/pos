<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\FeaturedProduct;
use Illuminate\Support\Facades\DB;

class ProductController extends Controller
{
    public function index(){
      $data = new \stdClass();
      $product = Product::with(['sku','category','imageProduct'])->paginate(8);
      $data->product = $product;

      $product_images = array();
      $data->product_images = $product_images;

      $related_product = Product::all();
      $data->related_product = $related_product;

      return response()->json($product);
    }

    public function byCategory($category_id){
      $data = new \stdClass();
      $product = Product::with(['sku','imageProduct','brand','category'=> function($c) use($category_id){
        $c->where('categories.id',$category_id);
      }])
      ->paginate(8);
      $data->product = $product;

      $product_images = array();
      $data->product_images = $product_images;

      $related_product = Product::all();
      $data->related_product = $related_product;

      return response()->json([
          'status' => 'success',
          'data' => $data,
          'count' => count($product)
      ]);
    }

    public function featured(){
      $featured = FeaturedProduct::with(['product'])->limit(6)->get();
      return $featured;
    }

    public function onSale(){
      $sale = Product::with(['sku','imageProduct','brand','category'])->where('discount_price','!=',0)->limit(6)->get();
      return $sale;
    }

    public function new(){
      $new = Product::with(['sku','imageProduct','brand','category'])->orderBy('created_at','desc')->limit(3)->get();
      return $new;
    }
}
