<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PromotionalProduct;

class PromotionalController extends Controller
{
    public function getTimer(){
      $promotional = PromotionalProduct::first();
      return strtotime($promotional->promo_timer);
    }
}
