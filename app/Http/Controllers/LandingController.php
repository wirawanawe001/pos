<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Pakage;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Product;
use App\Models\Sosmed;
use App\Models\Table;
use App\Models\HomeSlideImage;
use App\Models\Contact;
use App\Models\ShoppingCart;
use FrontRegister;
use View;


class LandingController extends Controller
{
    protected $home;
    protected $phone;
    protected $email;
    protected $address;
    protected $facebook;
    protected $instagram;
    protected $whatsapp;
    protected $youtube;
    protected $twitter;
    protected $line;

    public function __construct()
    {
        $email = Contact::where('name', 'email')->get();
        $carts = ShoppingCart::where('user_id', Auth::id())->get();
        $this->phone = Contact::where('name', 'phone')->first();
        $this->email = Contact::where('name', 'email')->first();
        $this->instagram = Sosmed::where('name', 'instagram')->first();
        $this->twitter = Sosmed::where('name', 'twitter')->first();
        $this->facebook = Sosmed::where('name', 'facebook')->first();
        $this->line = Sosmed::where('name', 'line')->first();
        $this->whatsapp = Sosmed::where('name', 'whatsapp')->first();
        $this->youtube = Sosmed::where('name', 'youtube')->first();
        View::share('phone', $this->phone);
        View::share('instagram', $this->instagram);
        View::share('twitter', $this->twitter);
        View::share('facebook', $this->facebook);
        View::share('line', $this->line);
        View::share('whatsapp', $this->whatsapp);
        View::share('youtube', $this->youtube);
        View::share('email', $this->email);
        View::share('emails', $email);
    }


    public function table()
    {
        if (Auth::check()) {
            $homeSlideImage = HomeSlideImage::all();
            $tables = Table::all();
            return view('table')
                ->with('slider', $homeSlideImage)
                ->with('tables', $tables);
        } else {
            return redirect()->route('ulogin');
        }
    }

    public function menu($order_id, $stage = NULL)
    {
        $orderDetail = OrderDetail::where('order_id', $order_id)->get();
        $subtotal = 0;
        foreach ($orderDetail as $detail) {
            $subtotal = $subtotal + ($detail->price * $detail->qty);
        }
//        $pakages = Pakage::all()->paginate(3);
        $homeSlideImage = HomeSlideImage::all();
        $categories = Category::with(['productCategory' => function($productCategory){
            $productCategory->with(['product' => function($product){
                $product->orderBy('name','desc');
            }]);
        }])->get();
        $order = Order::find($order_id);
        $addresses = Address::all();
	    $products = Product::orderBy('name')->get();
        $table = Table::find($order['table_id']);
		$view = view('menu');
		if($stage!=NULL){
            $view->with('stage',$stage);
        }

        return $view
            ->with('products', $products)
//            ->with('pakages', $pakages)
            ->with('slider', $homeSlideImage)
            ->with('addresses', $addresses)
            ->with('categories', $categories)
            ->with('table', $table)
            ->with('order', $order)
            ->with('orderDetail', $orderDetail)
            ->with('subtotal', $subtotal);
    }

    public function menu2($order_id)
    {
        $orderDetail = OrderDetail::where('order_id', $order_id)->get();
        $subtotal = 0;
        foreach ($orderDetail as $detail) {
            $subtotal = $subtotal + ($detail->price * $detail->qty);
        }
        $homeSlideImage = HomeSlideImage::all();
        $categories = Category::all();
        $order = Order::find($order_id);
        $addresses = Address::all();
        $products = Product::orderByDesc('name')->get();
        $table = Table::find($order['table_id']);
        return view('menu2')
            ->with('products', $products)
            ->with('slider', $homeSlideImage)
            ->with('addresses', $addresses)
            ->with('categories', $categories)
            ->with('table', $table)
            ->with('order', $order)
            ->with('orderDetail', $orderDetail)
            ->with('subtotal', $subtotal);
    }

    public function bill($order_id, $table_id = NULL)
    {
        $orderDetail = OrderDetail::where('order_id', $order_id)->get();
        $subtotal = 0;
        foreach ($orderDetail as $detail) {
            $subtotal = $subtotal + ($detail->price * $detail->qty);
        }
        $homeSlideImage = HomeSlideImage::all();
        $categories = Category::all();
        $order = Order::find($order_id);
        $order->stage = $order['stage']+1;
        $order->save();
        $addresses = Address::all();
        $products = Product::orderBy('name')->get();
        $view = view('bill');
        if ($order->table_id != NULL) {
            $table = Table::find($order->table_id);
            $view->with('table', $table);
        }
        return $view->with('products', $products)
            ->with('slider', $homeSlideImage)
            ->with('addresses', $addresses)
            ->with('categories', $categories)
            ->with('order', $order)
            ->with('orderDetail', $orderDetail)
            ->with('subtotal', $subtotal);


    }

    public function edit($order_id, $stage = NULL)
    {
        $orderDetail = OrderDetail::where('order_id', $order_id)->get();
        $subtotal = 0;
        foreach ($orderDetail as $detail) {
            $subtotal = $subtotal + ($detail->price * $detail->qty);
        }
        $homeSlideImage = HomeSlideImage::all();
        $categories = Category::all();
        $order = Order::find($order_id);
        $addresses = Address::all();
        $products = Product::orderBy('name')->get();
        $view = view('edit');
        if ($order->table_id != NULL) {
            $table = Table::find($order->table_id);
            $view->with('table', $table);
        }
        return $view->with('products', $products)
            ->with('slider', $homeSlideImage)
            ->with('addresses', $addresses)
            ->with('categories', $categories)
            ->with('table', $table)
            ->with('order', $order)
            ->with('orderDetail', $orderDetail)
            ->with('subtotal', $subtotal);
    }

    public function checkout()
    {
        return view('checkout');
    }

    public function contact()
    {
        return view('contact');
    }

    public function login()
    {
        $homeSlideImage = HomeSlideImage::all();
        return view('login')
            ->with('slider', $homeSlideImage);
    }

    public function register()
    {
        if (Auth::check()) {
            if ($request->isMethod('post')) {

                return view('register')
                    ->with('user', Auth::user())
                    ->with('slider', $homeSlideImage);
            } else {
                return redirect()->route('table');
            }
        } else {
            return redirect()->route('ulogin');
        }
    }
}