<?php

namespace App\Http\Controllers;

use App\Models\OrderDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Table;
use Illuminate\Support\Facades\Auth;
use View;
use Webpatser\Uuid\Uuid;

class OrderController extends Controller
{
    public function __construct()
    {
        View::share('page_state', 'Order');
    }


    public function add(Request $request)
    {
        $uuid = Uuid::generate()->string;
        if ($request->isMethod('post')) {
            $order = Order::create([
                'user_id' => Auth::id(),
                'order_code' => date("Ymd") . strtoupper(substr($request->name, 0, 2)) . strtoupper(substr($request->name, -1, 2)) . substr($uuid, 0, 4),
                'type' => $request->type,
                'address' => $request->address,
                'table_id' => $request->table,
                'name' => $request->name,
                'status' => 5,
            ]);
            if (!empty($request->table)) {
                $table = Table::find($request->table);
                $table->status = "Filled";
                $table->save();
                $order->stage = 1;
                $order->save();
            }
                return redirect()->route('menu', [$order['id']]);

        } else {
            return view('menu');
        }
    }

    public function show($order_id)
    {
        $order = Order::find($order_id);
        return view('dashboard.order.detail')->with('order', $order);
    }

    public function update(Request $request, $order_id)
    {
        $order = Order::find($order_id);
        $subtotal = 0;
        foreach($order['orderDetail'] as $detail){
            $subtotal = $subtotal + ($detail->price * $detail->qty);
        }
        $order->status = 3;
        $order->total = $request->total;
        $order->payment = $request->payment;
        $order->change = $request->payment - $request->total;
        $order->discount = $request->discount;
        $order->save();

        if($order->table != NULL) {
        return redirect()->route('bill',$order_id);
    }else{
            return redirect()->route('menu',$order_id);
        }
    }

    public function printBill($order)
    {
        $order = Order::find($order);
        $order->status = 1;
        if($order->table_id != NULL){
            $order->table->status = 'Empty';
            $order->table->save();
        }
        $order->table_id = NULL;
        $order->save();
        return redirect()->route('table');
    }

    public function status($order_id)
    {
        $order = Order::find($order_id);
        if ($order->status == 5) {
            $order->status = 4;
        } elseif ($order->status == 4) {
            $order->status = 3;
        } elseif ($order->status == 3) {
            $order->status = 2;
        } elseif ($order->status == 2) {
            $order->status = 1;
        }
        $order->save();
        return redirect()->route('orders');
    }


    public function delete($order_id)
    {
        $order = Order::find($order_id);
        if ($order->table_id != NULL) {
            $table = Table::find($order->table_id);
            $table->status = "Empty";
            $table->save();
        }
        $orderDetail = $order->orderDetail;
        foreach($orderDetail as $detail){
            $invProducts = $detail->product->inventoryProduct;
            foreach($invProducts as $invProduct){
                $invProduct->inventory->qty = $invProduct->inventory->qty + ($invProduct->qty * $detail->qty);
                $invProduct->inventory->save();
            }
        }
        $order->orderDetail()->delete();
        $order->delete();


        return redirect()->route('table');

    }
}
