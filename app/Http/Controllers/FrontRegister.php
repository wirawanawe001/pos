<?php

namespace App\Http\Controllers;

use App\Models\HomeSlideImage;
use Auth;
use App\Models\User;
use App\Models\Role;
use App\Http\Controllers\LandingController;
use Illuminate\Http\Request;
use App\Http\Requests\CreateNewUser;

class FrontRegister extends Controller
{

    public function register()
    {   $front = new LandingController();
        $homeSlideImage = HomeSlideImage::all();

        return view('register')
            ->with('front',$front)
            ->with('slider',$homeSlideImage);
    }

    public function registerPost(Request $request)
    {
        $input = $request->input();
        $userCreate= User::create([
            'name' => $input['first']." ".$input['last'],
            'address' => $input['address'].', '.$input['city'],
            'phone' => $input['phone'],
            'email' => $input['email'],
            'password' => bcrypt($input['password']),
            'provider' => 'local',
            'status' => true,
            'activated_token' => sha1($input['email']),
        ]);
        if ($request->hasFile('photo')) {
          if ($request->file('photo')->isValid()) {
              return $request->photo->extension();exit;
              //$path = $request->photo->storeAs('images/profile',);
          }
          $user->photo = $request->file('photo');
        }
        $member = Role::whereName('member')->first();
        $userCreate->attachRole($member);
        //$request->session()->flash('alert-success', 'Check Your Email For Activation !');
        auth()->login($userCreate, true);
        if (Auth::user()->hasRole('owner')) {
            return redirect()->to('/dashboard');
        } elseif (Auth::user()->hasRole('admin')) {
            return redirect()->to('/dashboard');
        } elseif (Auth::user()->hasRole('member')) {
            return redirect()->to('/');
        }
        return redirect()->to('/');
    }
}
