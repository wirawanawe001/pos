<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use Auth;
use App\Models\Category;
use App\Models\Address;
use App\Models\Product;
use App\Models\Sosmed;
use App\Models\Table;
use App\Models\HomeSlideImage;
use App\Models\Contact;
use App\Models\ShoppingCart;

class CartController extends Controller
{
    public function __construct(){
      View::share('phone',  Contact::where('name', 'phone')->first());
      View::share('phones', Contact::where('name', 'phone')->get());
      View::share('address', Contact::where('name', 'address')->first());
      View::share('instagram', Contact::where('name', 'instagram')->first());
      View::share('twitter', Contact::where('name', 'twitter')->first());
      View::share('facebook', Contact::where('name', 'facebook')->first());
      View::share('youtube', Contact::where('name', 'youtube')->first());
      View::share('line', Contact::where('name', 'line')->first());
      View::share('whatsapp', Contact::where('name', 'whatsapp')->first());
      View::share('email', Contact::where('name', 'email')->first());
      View::share('emails', Contact::where('name', 'email')->get());
      View::share('categories', Category::all());
    }

    public function index(){
        $carts = ShoppingCart::all();
        $total += $cart->product->cost_price*$cart->qty;
        return view('cart')
            ->with('carts',$carts)
            ->with('total',$total);
    }

    public function add(Request $request){
        if(Auth::check()){
          $cartByProduct = ShoppingCart::where('product_id',$request->product)->where('table_id',$request->table)->first();
          if(count($cartByProduct)==0){
            ShoppingCart::create([
              'user_id' => Auth::id(),
              'product_id' => $request->product,
              'table_id' => $request->table,
              'qty' => $request->qtybutton,
            ]);
          }else{
              $cartByProduct->qty = $cartByProduct->qty + $request->qtybutton;
              $cartByProduct->save();
          }
          return redirect()->route('table',$request->table);
        }else{
          return redirect()->route('ulogin');
        }
    }
    public function edit_name(Request $request, $table_id){
        $lastCart = ShoppingCart::orderBy('created_at','desc')->pluck('created_at')->first();
        $updateCart = ShoppingCart::where('table_id',$table_id)->update(['name'=>$request->order_name]);

        $carts = ShoppingCart::where('table_id',$table_id)->orderBy('created_at','desc')->get();
        $table = Table::find($table_id);
        $products = Product::all();
        $homeSlideImage = HomeSlideImage::all();
        $addresses = Address::all();
        return view('cart')
            ->with('products',$products)
            ->with('slider',$homeSlideImage)
            ->with('addresses',$addresses)
            ->with('table',$table)
            ->with('carts',$carts);
    }


    public function delete($cart_id){
      ShoppingCart::destroy($cart_id);
      return redirect()->back();
    }
}
