<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Order;
use App\Models\Contact;
use App\Models\Category;
use App\Http\Requests\UpdateUser;
use Auth;
use View;

class UserController extends Controller
{
  public function __construct(){
    View::share('phone',  Contact::where('name', 'phone')->first());
    View::share('phones', Contact::where('name', 'phone')->get());
    View::share('address', Contact::where('name', 'address')->first());
    View::share('instagram', Contact::where('name', 'instagram')->first());
    View::share('twitter', Contact::where('name', 'twitter')->first());
    View::share('facebook', Contact::where('name', 'facebook')->first());
    View::share('youtube', Contact::where('name', 'youtube')->first());
    View::share('line', Contact::where('name', 'line')->first());
    View::share('whatsapp', Contact::where('name', 'whatsapp')->first());
    View::share('email', Contact::where('name', 'email')->first());
    View::share('emails', Contact::where('name', 'email')->get());
    View::share('categories', Category::all());
  }
  public function account(Request $request)
  {
      if (Auth::check()) {
          return view('account')->with('orders', Order::where('user_id', Auth::id())->get());
      } else {
          return redirect()->route('ulogin');
      }
  }

    public function update(UpdateUser $request){
        $user = User::find(Auth::id());
        $user->name = $request->name;
        $user->phone = $request->phone;
        $user->address = $request->address;
        $user->save();
        return redirect()->back();
    }
}
