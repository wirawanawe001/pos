<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Storage;
use App\Models\Blog;


class BlogController extends Controller
{
    public function __construct()
    {
        View::share('page_state', 'Blog');
    }

    public function index()
    {
        $blogs = Blog::orderBy('created_at','desc')->paginate(4);
        return view('dashboard.blog.home')->with('blogs', $blogs);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            //IMAGE UPLOAD FUNCTION TO public/upload/about
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $path = $request->file('image')->store('blog', 'public_uploads');
                }
            }
            Blog::create([
                'author' => $request->author,
                'title' => $request->title,
                'content' => $request->content,
                'image' => $path,
            ]);
            return redirect()->route('back.blogs');
        } else {
            return view('dashboard.blog.add');
        }
    }

    public function edit($blog_id)
    {
        $blog = Blog::find($blog_id);
        return view('dashboard.blog.edit')->with('blog', $blog);
    }

    public function update(Request $request, $blog_id)
    {
        $blog = Blog::find($blog_id);
        //IMAGE UPLOAD FUNCTION TO public/upload/home
        if ($blog->icon !=null || "") {
            Storage::disk('public_uploads')->delete('/'.$blog['image']);
        }
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $blog->image  = $request->file('image')->store('blog', 'public_uploads');
            }
        }
        $dom = new \DomDocument();

        $dom->loadHtml($request->content, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);

        $images = $dom->getElementsByTagName('img');

        foreach($images as $k => $img){

            $data = $img->getAttribute('src');

            list($type, $data) = explode(';', $data);

            list(, $data)      = explode(',', $data);

            $data = base64_decode($data);

            $image_name= "/uploads/blog" . time().$k.'.png';

            $path = public_path() . $image_name;

            file_put_contents($path, $data);

            $img->removeAttribute('src');

            $img->setAttribute('src', $image_name);

        }
        $detail = $dom->saveHTML();
        $blog->title = $request->title;
        $blog->content = $detail;
        $blog->save();

        return redirect()->route('back.blogs');
    }

    public function destroy($blog_id)
    {
        $blog = Blog::find($blog_id);
        Storage::disk('public_uploads')->delete('/'.$blog['image']);
        $blog->comment()->delete();
        Blog::destroy($blog_id);
        return redirect()->route('back.blogs');
    }
}
