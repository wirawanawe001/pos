<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\HomeSlideImage;
use Illuminate\Support\Facades\Storage;

class SliderController extends Controller
{
    public function __construct(){
      View::share('page_state','Slider');
    }

    public function index(){
      	$homeSlideImage = HomeSlideImage::all();
      	return view('dashboard.slider.home')->with('slider',$homeSlideImage);
    }

    public function store(Request $request){
    	if ($request->isMethod('post')) {
    		//IMAGE UPLOAD FUNCTION TO public/upload/home
    		if ($request->hasFile('background')) {
  			    if ($request->file('background')->isValid()) {
  				    $path = $request->file('background')->store('home','public_uploads');
  				}
  			}
	        HomeSlideImage::create([
	            'background' => $path,
	            'title' => $request->title,
	            'subtitle' => $request->subtitle,
	        ]);
        	return redirect()->route('back.home');
	    }else{
	        return view('dashboard.slider.add');
	    }
  	}

	public function edit($home_id){
    	$homeSlideImage = HomeSlideImage::find($home_id);
    	return view('dashboard.slider.edit')->with('slider', $homeSlideImage);
	}

	public function update(Request $request, $home_id){
    	$homeSlideImage = HomeSlideImage::find($home_id);
    	//IMAGE UPLOAD FUNCTION TO public/upload/home
    	if ($homeSlideImage->background !=NULL || "") {
    		Storage::disk('public_uploads')->delete('/'.$homeSlideImage->background);
    	}
    	if ($request->hasFile('background')) {
		    if ($request->file('background')->isValid()) {
			    $homeSlideImage->background = $request->file('background')->store('home', 'public_uploads');
  			}
  		}
    	$homeSlideImage->title = $request->input('title');
     	$homeSlideImage->subtitle = $request->input('subtitle');
     	$homeSlideImage->save();

    	return redirect()->route('back.home');
	}

	public function destroy($home_id){
		$homeSlideImage = HomeSlideImage::find($home_id);
		Storage::disk('public_uploads')->delete('/'.$homeSlideImage->background);
		HomeSlideImage::destroy($home_id);
        return redirect()->route('back.home');
	}
}
