<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Table;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Order;
use View;

class OrderController extends Controller
{
    public function __construct()
    {
        View::share('page_state', 'Order');
    }

    public function index()
    {
        $orders = Order::all();
        return view('dashboard.order.home')->with('orders', $orders);
    }

    public function show($order_id)
    {
        $order = Order::find($order_id);
        return view('dashboard.order.detail')->with('order', $order);
    }

    public function delete($order_id)
    {
        $order = Order::find($order_id);
        if ($order->table_id != NULL) {
            $table = Table::find($order->table_id);
            $table->status = "Empty";
            $table->save();
        }
        $orderDetail = $order->orderDetail;
        foreach($orderDetail as $detail){
            $invProducts = $detail->product->inventoryProduct;
            foreach($invProducts as $invProduct){
                $invProduct->inventory->qty = $invProduct->inventory->qty + ($invProduct->qty * $detail->qty);
                $invProduct->inventory->save();
            }
        }
        $order->orderDetail()->delete();
        $order->delete();


        return redirect()->route('back.orders');

    }

}
