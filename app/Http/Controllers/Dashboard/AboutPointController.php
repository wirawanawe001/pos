<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\AboutPoint;
use Illuminate\Support\Facades\Storage;

class AboutPointController extends Controller
{
	public function __construct(){
		View::share('page_state','About');
	}

	public function index(){
	      $contacts = Contact::all();
	      $about = About::find(1);
	      $aboutPoints = AboutPoint::all();
	      return view('dashboard.about.home')
	      ->with('contacts',$contacts)
	      ->with('about',$about)
	      ->with('aboutPoints',$aboutPoints);
	}

	public function store(Request $request){
    	if ($request->isMethod('post')) {
    		//IMAGE UPLOAD FUNCTION TO public/upload/about
    		if ($request->hasFile('icon')) {
				    if ($request->file('icon')->isValid()) {
					    $path = $request->file('icon')->store('about','public_uploads');
					}
				}
		        AboutPoint::create([
		            'icon' => $path,
		            'title' => $request->title,
		            'description' => $request->description,
		        ]);
	        	return redirect()->route('back.about');
		    }else{
		        return view('dashboard.about_point.add');
		    }
  	}

	public function edit($point_id){
		$aboutPoint = AboutPoint::find($point_id);
    	return view('dashboard.about_point.edit')->with('aboutPoint', $aboutPoint);
	}

	public function update(Request $request, $point_id){
		$aboutPoint = AboutPoint::find($point_id);
    	//IMAGE UPLOAD FUNCTION TO public/upload/home
    	if ($aboutPoint->icon !=NULL || "") {
    		Storage::disk('public_uploads')->delete('/'.$aboutPoint['icon']);
    	}
    	if ($request->hasFile('icon')) {
			    if ($request->file('icon')->isValid()) {
				    $aboutPoint->icon  = $request->file('icon')->store('about', 'public_uploads');
				}
			}
    	$aboutPoint->title = $request->input('title');
     	$aboutPoint->description = $request->input('description');
     	$aboutPoint->save();

    	return redirect()->route('back.home');
	}

	public function destroy($point_id){
		$aboutPoint = AboutPoint::find($point_id);
		Storage::disk('public_uploads')->delete('/'.$aboutPoint['icon']);
		AboutPoint::destroy($point_id);
    return redirect()->route('back.about');
	}
}
