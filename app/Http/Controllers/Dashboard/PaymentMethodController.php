<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\PaymentMethod;

class PaymentMethodController extends Controller
{
    public function __construct(){
        View::share('page_state','Payment');
    }

    public function index(){
        $payment = PaymentMethod::all();
        return view('dashboard.payment.home')->with('payment',$payment);
    }

    public function store(Request $request){
        if ($request->isMethod('post')) {
            PaymentMethod::create([
                'type'=> $request->type,
                'account_name' => $request->name,
                'account_number' => $request->number,
            ]);
            return redirect()->route('back.payments');

        }else{
            return view('dashboard.payment.add');
        }
    }

    public function show($payment_id){
        $payment = PaymentMethod::find($payment_id);
        return view('dashboard.payment.detail')->with('payment',$payment);

    }

    public function edit($payment_id){
        $payment = PaymentMethod::find($payment_id);
        return view('dashboard.payment.edit')->with('payment',$payment);
    }

    public function update(Request $request, $payment_id){
        $payment = PaymentMethod::find($payment_id);
        $payment->type = $request->type;
        $payment->account_name = $request->name;
        $payment->account_number = $request->number;
        $payment->save();
        return redirect()->route('back.payments');
    }

    public function destroy($payment_id){
        PaymentMethod::destroy($payment_id);
        return redirect()->route('back.payments');
    }
}
