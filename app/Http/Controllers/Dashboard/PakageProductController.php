<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Pakage;
use App\Models\Product;
use App\Models\PakageProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;

class PakageProductController extends Controller
{
    public function __construct(){
        View::share('page_state','');
    }
    public function store($request, $pakage_id){
        foreach($request->product as $index => $prod ){
            $pakageProduct = PakageProduct::create([
                'pakage_id' => $pakage_id,
                'product_id' => $prod,
                'qty' => $request->qty[$index],

            ]);
            $price = $pakageProduct->product['menu_price'] * $pakageProduct['qty'];
            $pakageProduct->pakage['cost_price'] = $pakageProduct->pakage['cost_price'] + $price;
            $pakageProduct->pakage->save();
        }

    }
    public function add(Request $request, $pakage_id){
        $product = Product::all();
        if ($request->isMethod('post')) {
            PakageProduct::create([
                'pakage_id' => $pakage_id,
                'product_id' => $request->product,

            ]);
            return redirect()->route('back.pakage.view',$pakage_id);
        }else {
            return view('dashboard.pakage.add')
                ->with('pakage_id',$pakage_id)
                ->with('product',$product);
        }

    }

    public function edit($pakage_id, $pakage_product_id){
        $product = Product::all();
        $pakage = Pakage::find($pakage_id);
        return view('dashboard.product.inventory.edit')
            ->with('pakage_product_id',$pakage_product_id)
            ->with('pakage_id',$pakage_id)
            ->with('product',$product)
            ->with('pakage',$pakage);
    }


    public function destroy($pakage_product_id){
        PakageProduct::destroy($pakage_product_id);
        return back();
    }
}

