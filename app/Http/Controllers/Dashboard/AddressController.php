<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Address;
use View;


class AddressController extends Controller
{
  public function __construct(){
    View::share('page_state','Address');
  }


  public function store(Request $request){
    if ($request->isMethod('post')) {
        Address::create([
            'name' => $request->name,
            'value' => $request->value,
            'location' => $request->location,

        ]);
        return redirect()->route('back.about');
    }else{
        $addresses = Address::all();
        return view('dashboard.address.add')->with('addresses',$addresses);
    }
  }

  public function show($address_id){
      $address = Address::find($address_id);
      return view('dashboard.address.detail')->with('address',$address);
  }

  public function edit($address_id){
      $address = Address::find($address_id);
      return view('dashboard.address.edit')->with('address', $address);
  }

  public function update(Request $request, $address_id){
          $address = Address::find($address_id);
          $address->name = $request->input('name');
          $address->value = $request->input('value');
          $address->location = $request->input('location');

      $address->save();

          return redirect()->route('back.about');
  }

  public function destroy($address_id){
          Address::destroy($address_id);
          return redirect()->route('back.about');
  }
}
