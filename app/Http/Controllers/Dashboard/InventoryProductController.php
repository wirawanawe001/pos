<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Inventory;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InventoryProduct;
use View;
class InventoryProductController extends Controller
{
    public function __construct(){
        View::share('page_state','');
    }

    public function store($request, $product_id){
        foreach($request->inventory as $index => $inv) {
            $inventory = InventoryProduct::create([
                'product_id' => $product_id,
                'inventory_id' => $inv,
                'qty' => $request->qty[$index],

            ]);
            $price = $inventory->inventory['unit_price'] * $inventory['qty'];
            $inventory->product['cost_price'] = $inventory->product['cost_price'] + $price;
            $inventory->product->save();
        }
    }

    public function add(Request $request, $product_id){
        $inventory = Inventory::all();
        if ($request->isMethod('post')) {
            $inventory = InventoryProduct::create([
            'product_id' => $product_id,
            'inventory_id' => $request->inventory,
            'qty' => $request->qty,

        ]);
            $price = $inventory->inventory['unit_price'] * $inventory['qty'];
            $inventory->product['cost_price'] = $inventory->product['cost_price'] + $price;
            $inventory->product->save();
            return redirect()->route('back.product.view',$product_id);
        }else {
            return view('dashboard.product.inventory.add')
                ->with('product_id',$product_id)
                ->with('inventory',$inventory);
        }

    }

    public function edit($product_id, $inventory_product_id){
        $inventory = Inventory::all();
        $product = Product::find($product_id);
        /* salah */$inventory_product = InventoryProduct::find($inventory_product_id);
        return view('dashboard.product.inventory.edit')
            ->with('inventory_product', $inventory_product)
            ->with('inventory_product_id',$inventory_product_id)
            ->with('product_id',$product_id)
            ->with('product',$product)
            ->with('inventory',$inventory);
    }


    public function update(Request $request, $product_id, $inventory_product_id){
            $inventoryProduct = InventoryProduct::find($inventory_product_id);

            $inventoryProduct->qty = $request->qty;
            $inventoryProduct->save();

            $product = Product::find($product_id);
            $cost_price = 0;
            foreach($product->inventoryProduct as $inventoryProduct){
                $cost_price= $cost_price + ($inventoryProduct->inventory['unit_price'] * $inventoryProduct->qty);
            }
            $product->cost_price = $cost_price;
            $product->save();


            return redirect()->route('back.product.view',$inventoryProduct['product_id']);
    }

    public function destroy($product_id, $inventory_product_id){
        $inventoryProduct = InventoryProduct::find($inventory_product_id);
        $unit_price = $inventoryProduct->inventory['unit_price'] * $inventoryProduct['qty'];
        $inventoryProduct->product['cost_price'] = $inventoryProduct->product['cost_price'] - $unit_price;
        $inventoryProduct->product->save();
//
        InventoryProduct::destroy($inventory_product_id);
        return back();
    }
}
