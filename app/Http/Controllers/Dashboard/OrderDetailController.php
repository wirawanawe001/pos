<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OrderDetail;
use App\Models\Order;
use View;


class OrderDetailController extends Controller
{
    public function __construct(){
        View::share('page_state','Order');
    }
    public function index($id){
        $orderDetail = OrderDetail::where('order_id','=',$id)->get();
        $order = Order::find($id);

        return view('dashboard.order.detail')
            ->with('orderDetail',$orderDetail)
            ->with('order',$order);
    }
}
