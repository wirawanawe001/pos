<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Sosmed;
use App\Models\Address;
use App\Models\About;
use App\Models\AboutPoint;
use View;
use Storage;

class AboutController extends Controller
{
    public function __construct(){
        View::share('page_state','About');
    }

    public function index(){
        $contacts = Contact::all();
        $sosmeds = Sosmed::all();
        $addresses = Address::all();
        $about = About::find(1);
        $aboutPoints = AboutPoint::all();
        return view('dashboard.about.home')
            ->with('contacts',$contacts)
            ->with('sosmeds',$sosmeds)
            ->with('addresses',$addresses)
            ->with('about',$about);
    }

    public function edit(){
        $about = About::find(1);
        return view('dashboard.about.edit')->with('about', $about);
    }

    public function update(Request $request){
        $about = About::find(1);
        if ($about['image'] !=NULL || "") {
            Storage::disk('public_uploads')->delete('/'.$about->icon);
        }
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $about['image'] = $request->file('image')->store('about', 'public_uploads');
            }
        }
        $about->information = $request->input('information');
        $about->save();

        return redirect()->route('back.about');
    }
}
