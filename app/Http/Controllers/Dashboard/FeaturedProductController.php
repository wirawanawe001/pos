<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\FeaturedProduct;
use App\Models\Product;

class FeaturedProductController extends Controller
{
    public function __construct(){
        View::share('page_state','Featured');
    }

    public function index(){
        $products = Product::all();
        $featured = FeaturedProduct::all();
        return view('dashboard.featured.home')
            ->with('featured', $featured)
            ->with('products',$products);
    }
}
