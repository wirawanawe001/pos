<?php

namespace App\Http\Controllers\Dashboard;

use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use View;
use App\Models\Pakage;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\ProductCategory;
use App\Models\ImageProduct;

class PakageController extends Controller
{
    public function __construct(){
        View::share('page_state','Pakage');
    }

    public function index(){
        $pakages = Pakage::all();
        return view('dashboard.pakage.home')->with('pakages',$pakages)
            ;
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            if ($request->hasFile('image')) {
                if ($request->file('image')->isValid()) {
                    $path = $request->file('image')->store('home','public_uploads');
                }
            }
            $pakage = Pakage::create([
                'name' => $request->name,
                'pakage_price' => $request->pakage_price,
                'description' => $request->description,
                'qty' => 1,
                'cost_price' => 0,
                'image' => $path,
            ]);
            $pakage_product = new PakageProductController();
            $pakage_product->store($request, $pakage->id);
            return redirect()->route('back.pakages');
        }else{
            $products = Product::all();
            return view('dashboard.pakage.add')
                ->with('products', $products);
        }
    }

    public function show($pakage_id){
        $pakage = Pakage::find($pakage_id);
        $product = Product::all();
        return view('dashboard.pakage.detail')->with('pakage',$pakage)
            ->with('product',$product);
    }

    public function edit($pakage_id){
        $pakage = Pakage::find($pakage_id);
        $product = Product::all();
        return view('dashboard.pakage.edit')
            ->with('pakage',$pakage)
            ->with('product', $product);
    }

    public function update(Request $request, $pakage_id){
        $pakage = Pakage::find($pakage_id);
        if ($pakage->image !=NULL || "") {
            Storage::disk('public_uploads')->delete('/'.$pakage['image']);
        }
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $pakage->image = $request->file('image')->store('home', 'public_uploads');
            }
        }
        $pakage->name = $request->name;
        $pakageProduct = $pakage->pakageProduct;
        $cost_price = 0;
        foreach($pakageProduct as $pkp){
            $cost_price = $cost_price + ($pkp->product['menu_price'] * $pkp['qty']);
        }
        $pakage->cost_price = $cost_price;
        $pakage->pakage_price = $request->pakage_price;
        $pakage->description = $request->description;
        $pakage->pakageProduct['qty'] = $request->qty;
        $pakage->save();
        return redirect()->route('back.pakage.view',$pakage['id']);
    }

    public function destroy($pakage_id){
        $pakage = Pakage::find($pakage_id);
        $pakage->pakageProduct()->delete();
        Pakage::destroy($pakage_id);
        return back();
    }
}
