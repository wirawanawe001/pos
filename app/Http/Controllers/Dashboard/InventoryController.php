<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Inventory;

class InventoryController extends Controller
{
    public function __construct(){
        View::share('page_state','Inventory');
    }

    public function index(){
        $inventories = Inventory::all();
        return view('dashboard.inventory.home')
            ->with('inventories',$inventories);
    }

    public function store(Request $request){
        if ($request->isMethod('post')) {
            $unit_price = $request->total_price / $request->qty;
            Inventory::create([
                'name'=> $request->name,
                'qty' => $request->qty,
                'weight' => $request->weight,
                'expired' => date("Y-m-d H:i:s",strtotime($request->expired)),
                'unit_price' => $unit_price,
                'total_price' => $request->total_price,
            ]);
            return redirect()->route('back.inventories');

        }else{
            return view('dashboard.inventory.add');
        }
    }

    public function edit($inventory_id){
        $inventory = Inventory::find($inventory_id);
        return view('dashboard.inventory.edit')->with('inventory',$inventory);
    }

    public function update(Request $request, $inventory_id){
        $unit_price = $request->total_price / $request->qty;
        $inventory = Inventory::find($inventory_id);
        $inventory->name = $request->name;
        $inventory->qty = $request->qty;
        $inventory->weight = $request->weight;
        $inventory->expired = date("Y-m-d H:i:s",strtotime($request->expired));
        $inventory->unit_price = $unit_price;
        $inventory->total_price = $request->total_price;
        $inventory->save();
        return redirect()->route('back.inventories');
    }

    public function destroy($inventory_id)
    {
        Inventory::destroy($inventory_id);
        return redirect()->route('back.inventories');
    }
}
