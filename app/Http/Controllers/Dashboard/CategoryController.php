<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Category;
use Illuminate\Support\Facades\Storage;


class CategoryController extends Controller
{
  public function __construct(){
    View::share('page_state','Category');
  }

  public function index(){
    $category = Category::all();
    return view('dashboard.category.home')->with('categories',$category);
  }

  public function store(Request $request){
    if ($request->isMethod('post')) {
        if ($request->hasFile('image')) {
            if ($request->file('image')->isValid()) {
                $path = $request->file('image')->store('home','public_uploads');
            }
        }
        if($request->category_id != ""){
          $category = Category::find($request->category_id)->get();
          if($category->level == NULL){
            Category::create([
                'name' => $request->name,
                'image' => $path,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'level' => 1,
            ]);
          }elseif($category->level == 1){
            Category::create([
                'name' => $request->name,
                'image' => $path,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'level' => 2,
            ]);
          }elseif($category->level == 2){
            Category::create([
                'name' => $request->name,
                'image' => $path,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'level' => 3,
            ]);
          }elseif($category->level == 3){
            Category::create([
                'name' => $request->name,
                'image' => $path,
                'description' => $request->description,
                'category_id' => $request->category_id,
                'level' => 4,
            ]);
          }
        }else{
          Category::create([
              'name' => $request->name,
              'image' => $path,
              'description' => $request->description,
          ]);
        }
        return redirect()->route('back.categories');

    }else{
      $categories = Category::all();
      return view('dashboard.category.add')->with('categories',$categories);
    }
  }

  public function show($category_id){
    $category = Category::find($category_id);
    $categories = Category::all();
    return view('dashboard.category.detail')->with('category',$category)
    ->with('categories',$categories);
  }

  public function edit($category_id){
    $category = Category::find($category_id);

    return view('dashboard.category.edit')->with('category',$category);
  }

  public function update(Request $request, $category_id){
      $category = Category::find($category_id);
      if ($category->image !=NULL || "") {
          Storage::disk('public_uploads')->delete('/'.$category->image);
      }
      if ($request->hasFile('image')) {
          if ($request->file('image')->isValid()) {
              $category->image = $request->file('image')->store('home', 'public_uploads');
          }
      }
      if($request->category_id != ""){
        $category->category_id = $request->category_id;
      }

      $category->name = $request->name;
      $category->description = $request->description;
      $category->save();
      return redirect()->route('back.categories');
  }

  public function destroy($category_id){
      $category = Category::find($category_id);
      Storage::disk('public_uploads')->delete('/'.$category->image);
      $category->productCategory()->delete();
      Category::destroy($category_id);
      return redirect()->route('back.categories');
  }
}
