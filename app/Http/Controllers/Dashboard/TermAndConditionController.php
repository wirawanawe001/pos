<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\TermAndCondition;

class TermAndConditionController extends Controller
{
    public function __construct(){
        View::share('page_state','Term');
    }

    public function index(){
        $term = TermAndCondition::all();
        return view('dashboard.term.home')->with('term',$term);
    }

    public function store(Request $request){
        if ($request->isMethod('post')) {
            TermAndCondition::create([
                'description'=> $request->desc,

            ]);
            return redirect()->route('back.term');

        }else{
            $term = TermAndCondition::all();
            return view('dashboard.term.add')->with('term',$term);
        }
    }

    public function show($term_id){
        $term = TermAndCondition::find($term_id);
        return view('dashboard.term.detail')->with('term',$term);

    }

    public function edit($term_id){
        $term = TermAndCondition::find($term_id);
        return view('dashboard.term.edit')->with('term',$term);
    }

    public function update(Request $request, $term_id){
        $payment = TermAndCondition::find($term_id);
        $payment->description = $request->desc;
        $payment->save();
        return redirect()->route('back.term');
    }

    public function destroy($term_id){
        TermAndCondition::destroy($term_id);
        return redirect()->route('back.term');
    }
}
