<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\FeaturedProduct;
use App\Models\Product;

class FeaturedController extends Controller
{
  public function __construct()
  {
      View::share('page_state', 'Featured');
  }

  public function index()
  {
      $features = FeaturedProduct::all();
      return view('dashboard.featured.home')->with('features', $features);
  }

  public function store(Request $request)
  {
      if ($request->isMethod('post')) {
          FeaturedProduct::create([
              'product_id' => $request->product_id,
              'description' => $request->description,
          ]);
          return redirect()->route('back.featured');
      } else {
          $featured = FeaturedProduct::pluck('product_id')->toArray();
          $products = Product::whereNotIn('id',$featured)->get();
          return view('dashboard.featured.add')->with('products',$products);
      }
  }

  public function destroy($featured_id)
  {
      FeaturedProduct::destroy($featured_id);
      return redirect()->route('back.featured');
  }
}
