<?php

namespace App\Http\Controllers\Dashboard;

use App\InventoryProduct;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Product;
use App\Models\Category;
use App\Models\Inventory;
use App\Models\Sku;
use App\Models\ProductCategory;
use App\Models\ImageProduct;
use App\Http\Controllers\Dashboard\ImageProductController;
use App\Http\Controllers\Dashboard\SkuController;
use App\Http\Controllers\Dashboard\ProductCategoryController;
use App\Http\Controllers\Dashboard\InventoryProductController;


class ProductController extends Controller
{
  public function __construct(){
    View::share('page_state','Product');
  }

  public function index(){
    $products = Product::all();
    return view('dashboard.product.home')->with('products',$products)
    ;
  }

  public function store(Request $request)
  {
      if ($request->isMethod('post')) {
        $product = Product::create([
            'name' => $request->name,
            'menu_price' => $request->menu_price,
            'description' => $request->description,
            'qty' => 1,
        ]);
        $inventoryProduct = new InventoryProductController();
        $inventoryProduct->store($request, $product->id);
        $productCategory = new ProductCategoryController();
        $productCategory->store($request, $product->id);
        $images = new ImageProductController();
        $images->store($request, $product->id);
        return redirect()->route('back.product');
    }else{
        $category = Category::all();
        $inventories = Inventory::all();
        return view('dashboard.product.add')
            ->with('category',$category)
            ->with('inventories', $inventories);
    }
  }

  public function show($product_id){
    $product = Product::find($product_id);
    $inventory = Inventory::all();
    $imageProducts = new ImageProductController();
    $imageProducts = $imageProducts->index($product_id);
    $imageProducts = ImageProduct::where('product_id',$product_id)->get();
    return view('dashboard.product.detail')->with('product',$product)
        ->with('imageProducts',$imageProducts)
        ->with('inventory',$inventory);
  }

  public function edit($product_id){
      $product = Product::find($product_id);
      $categories = Category::all();
      $inventories = Inventory::all();
      return view('dashboard.product.edit')
          ->with('categories',$categories)
          ->with('product',$product)
          ->with('inventories', $inventories);
  }

  public function update(Request $request, $product_id){
      $product = Product::find($product_id);
      $inventoryProduct = $product->inventoryProduct;
      $cost_price = 0;
      foreach($inventoryProduct as $inv){
          $cost_price= $cost_price + ($inv->inventory['unit_price'] * $inv['qty']);
      }
      $product->name = $request->name;
      $product->menu_price = $request->menu_price;
      $product->cost_price = $cost_price;
      $product->description = $request->description;
      $product->qty = $request->qty;
      $product->save();
      return redirect()->route('back.product.view',$product['id']);
  }

  public function destroy($product_id){
      $product = Product::find($product_id);
      $product->imageProduct()->delete();
      $product->productCategory()->delete();
      $product->inventoryProduct()->delete();
      $product->promotional()->delete();
      Product::destroy($product_id);
      return redirect()->route('back.product',$product['id']);
  }
}
