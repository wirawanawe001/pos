<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Contact;
use View;


class ContactController extends Controller
{
  public function __construct(){
    View::share('page_state','Contact');
  }


  public function store(Request $request){
    if ($request->isMethod('post')) {
        Contact::create([
            'name' => $request->type,
            'value' => $request->value,
            'icon' => $request->type,
        ]);
        return redirect()->route('back.about');
    }else{
        $contact = Contact::all();
        return view('dashboard.contact.add')->with('contact',$contact);
    }
  }

  public function show($contact_id){
      $contact = Contact::find($contact_id);
      return view('dashboard.contact.detail')->with('contact',$contact);
  }

  public function edit($contact_id){
      $contact = Contact::find($contact_id);
      return view('dashboard.contact.edit')->with('contact', $contact);
  }

  public function update(Request $request, $contact_id){
          $contact = Contact::find($contact_id);
          $contact->name = $request->input('type');
          $contact->value = $request->input('value');
          $contact->icon = $request->input('type');
      $contact->save();

          return redirect()->route('back.about');
  }

  public function destroy($contact_id){
          Contact::destroy($contact_id);
          return redirect()->route('back.about');
  }
}
