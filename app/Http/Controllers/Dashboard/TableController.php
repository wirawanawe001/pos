<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\Table;

class TableController extends Controller
{
    public function __construct(){
        View::share('page_state','Table');
    }

    public function index(){
        $tables = Table::all();
        return view('dashboard.table.home')->with('tables',$tables);
    }

    public function store(Request $request){
        if ($request->isMethod('post')) {
            Table::create([
                'number'=> $request->number,
                'position' => $request->position,
                'status' => $request->status,
            ]);
            return redirect()->route('back.tables');

        }else{
            return view('dashboard.table.add');
        }
    }

    public function show($table_id){
        $table = Table::find($table_id);
        return view('dashboard.table.detail')->with('table',$table);

    }

    public function edit($table_id){
        $table = Table::find($table_id);
        return view('dashboard.table.edit')->with('table',$table);
    }

    public function update(Request $request, $table_id){
        $table = Table::find($table_id);
        $table->number = $request->number;
        $table->position = $request->position;
        $table->status = $request->status;
        $table->save();
        return redirect()->route('back.tables');
    }

    public function destroy($table_id){
        Table::destroy($table_id);
        return redirect()->route('back.tables');
    }
}
