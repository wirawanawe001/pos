<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\PromotionalProduct;
use App\Models\Product;

class PromotionalController extends Controller
{
    public function __construct()
    {
        View::share('page_state', 'Promotional');
    }

    public function index()
    {
        $promotional = PromotionalProduct::first();
        return view('dashboard.promotional.home')->with('promotional', $promotional);
    }

    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            PromotionalProduct::create([
                'product_id' => $request->product_id,
                'promo_timer' => date("Y-m-d H:i:s",strtotime($request->promo_timer)),
                'description' => $request->description,
            ]);
            return redirect()->route('back.promotional');
        } else {
            $products = Product::all();
            return view('dashboard.promotional.add')->with('products',$products);
        }
    }

    public function edit($promotional_id)
    {
        $promotional = PromotionalProduct::find($promotional_id);
        $products = Product::all();
        return view('dashboard.promotional.edit')->with('promotional', $promotional)
        ->with('products',$products);
    }

    public function update(Request $request, $promotional_id)
    {
        $promotional = PromotionalProduct::find($promotional_id);
        $promotional->product_id = $request->product_id;
        $promotional->promo_timer = date("Y-m-d H:i:s",strtotime($request->promo_timer));
        $promotional->description = $request->description;
        $promotional->save();

        return redirect()->route('back.promotional');
    }
}
