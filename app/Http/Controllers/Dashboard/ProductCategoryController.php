<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;

class ProductCategoryController extends Controller
{
  public function store($request, $product_id){
        ProductCategory::create([
          'product_id' => $product_id,
          'category_id' => $request->category,
        ]);
  }

  public function update(Request $request, $sku_id){

  }

  public function destroy($sku_id){

  }
}
