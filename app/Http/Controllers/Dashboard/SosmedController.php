<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sosmed;
use View;


class SosmedController extends Controller
{
  public function __construct(){
    View::share('page_state','Sosmed');
  }


  public function store(Request $request){
    if ($request->isMethod('post')) {
        Sosmed::create([
            'name' => $request->type,
            'value' => $request->value,
            'icon' => $request->type,

        ]);
        return redirect()->route('back.about');
    }else{
        $sosmeds = Sosmed::all();
        return view('dashboard.sosmed.add')->with('sosmeds',$sosmeds);
    }
  }

  public function show($sosmed_id){
      $sosmed = Sosmed::find($sosmed_id);
      return view('dashboard.sosmed.detail')->with('sosmed',$sosmed);
  }

  public function edit($sosmed_id){
      $sosmed = Sosmed::find($sosmed_id);
      return view('dashboard.sosmed.edit')->with('sosmed', $sosmed);
  }

  public function update(Request $request, $sosmed_id){
          $sosmed = Sosmed::find($sosmed_id);
          $sosmed->name = $request->input('type');
          $sosmed->value = $request->input('value');
          $sosmed->icon = $request->input('type');

      $sosmed->save();

          return redirect()->route('back.about');
  }

  public function destroy($sosmed_id){
          Sosmed::destroy($sosmed_id);
          return redirect()->route('back.about');
  }
}
