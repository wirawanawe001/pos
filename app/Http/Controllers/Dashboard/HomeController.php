<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\DB;
use App\Models\OrderDetail;
use App\Models\Order;
use App\Models\User;

class HomeController extends Controller
{
    public function __construct(){
      View::share('page_state','Dashboard');
    }

    public function index(){
      $detail = OrderDetail::orderBy('created_at','desc')->paginate(4);
      $order = Order::orderBy('updated_at','desc')->get();
      $todayDate = date('Y-m-d');
      $yesterdayDate = date('Y m d', strtotime('-1 day', strtotime(date('r'))));
      $yesterdaySales = Order::select(DB::raw('SUM(total) as total'), DB::raw('DATE(created_at) as date'))
      ->groupBy(DB::raw('DATE(created_at)'))
      ->where(DB::raw('DATE(created_at)'), $yesterdayDate)
      ->get();
      $todaySales = Order::select(DB::raw('SUM(total) as total'), DB::raw('DATE(created_at) as date'))
      ->groupBy(DB::raw('DATE(created_at)'))
      ->where(DB::raw('DATE(created_at)'), $todayDate)
      ->get();
      $members = User::withRole('member')->orderBy('created_at', 'desc')->get();
      $recentUpdateOrders = Order::orderBy('updated_at', 'desc')->limit(3)->get();
      return view('dashboard.home')
      ->with('recentUpdateOrders', $recentUpdateOrders)
      ->with('recentMembers', $members)
      ->with('todaySales', $todaySales)
      ->with('yesterdaySales', $yesterdaySales);
    }
}
