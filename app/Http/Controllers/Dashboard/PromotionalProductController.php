<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use App\Models\PromotionalProduct;
use App\Models\Product;
class PromotionalProductController extends Controller
{
    public function __construct(){
        View::share('page_state','Promotional');
    }

    public function index(){

        $products = Product::all();
        $promotional = PromotionalProduct::all();
        return view('dashboard.promotional.home')
            ->with('promotional', $promotional)
            ->with('products',$products);
    }
}
