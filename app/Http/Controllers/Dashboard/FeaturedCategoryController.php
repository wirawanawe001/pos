<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use View;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use App\Models\CategoryFeatured;
use App\Models\Category;

class FeaturedCategoryController extends Controller
{
  public function __construct()
  {
      View::share('page_state', 'Featured Category');
  }

  public function index()
  {
      $features = CategoryFeatured::with(['category'])->get();
      return view('dashboard.category.featured.home')->with('features', $features);
  }

  public function store(Request $request)
  {
      if ($request->isMethod('post')) {
          CategoryFeatured::create([
              'category_id' => $request->category_id,
          ]);
          return redirect()->route('back.category.featured');
      } else {
          $catArr = array();
          $feaArr = array();
          $features = CategoryFeatured::orderBy('category_id','asc')->pluck('category_id')->all();
          $categories = Category::whereNotIn('id',$features)->orderBy('id','asc')->get();
          return view('dashboard.category.featured.add')->with('categories',$categories);
      }
  }

  public function edit(Request $request, $cat_featured_id){
    if ($request->isMethod('post')) {
      $featured = CategoryFeatured::find($cat_featured_id);
      $featured->category_id = $request->category_id;
      $featured->save();
      return redirect()->route('back.category.featured');
    }else{
      $featured = CategoryFeatured::find($cat_featured_id);
      $features = CategoryFeatured::orderBy('category_id','asc')->pluck('category_id')->all();
      $categories = Category::whereNotIn('id',$features)->get();
      return view('dashboard.category.featured.edit')->with('categories',$categories)
      ->with('featured',$featured);
    }

  }

  public function destroy($cat_featured_id)
  {
      FeaturedProduct::destroy($featured_id);
      return redirect()->route('back.featured');
  }
}
