<?php

namespace App\Http\Controllers\Dashboard;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Image;
use View;
use Illuminate\Support\Facades\Storage;
use App\Models\ImageProduct;
use App\Models\Product;


class ImageProductController extends Controller
{
  public function __construct(){
    View::share('page_state','Product');
  }

  public function index($product_id){
    $imageProduct = ImageProduct::where('product_id',$product_id)->get();
    return $imageProduct;
  }

  public function store($request, $product_id)
  {
    $product = Product::find($product_id);
    foreach($request->image as $image){
        $path = $image->store('product','public_uploads');
        /*Image::make($image->getRealPath())->resize(null, 200, function ($constraint) {
            $constraint->aspectRatio();
        })->save('uploads/products/');*/
        ImageProduct::create([
          'product_id' => $product_id,
          'url' => $path,
        ]);
    }
  }

  public function add(Request $request, $product_id){
    $product = Product::find($product_id);
    if ($request->isMethod('post')) {
        $path = $request->file('image')->store('product','public_uploads');
        ImageProduct::create([
          'product_id' => $product_id,
          'url' => $path,
        ]);
        return redirect()->route('back.product.view',$product_id);
    }else{
        return view('dashboard.imageProduct.add')->with('product',$product);
    }
  }

  public function show($image_id){

  }

  public function edit($product_id, $image_id){
    $product = Product::find($product_id);
    $imageProduct = ImageProduct::find($image_id);
    return view('dashboard.imageProduct.edit')->with('product',$product)
    ->with('imageProduct',$imageProduct);
  }

  public function update(Request $request, $product_id, $image_id){
    $product = Product::find($product_id);
    $imageProduct = ImageProduct::find($image_id);
    if ($imageProduct->image !=NULL || "") {
      Storage::disk('public_uploads')->delete('/'.$imageProduct->image);
    }
    if ($request->hasFile('image')) {
        if ($request->file('image')->isValid()) {
          $imageProduct->url = $request->file('image')->store('product','public_uploads');
      }
    }
    $imageProduct->product_id = $product['id'];
    $imageProduct->save();
    return redirect()->route('back.product.view',$product['id']);
  }

  public function destroy($product_id, $image_id){
      $imageProduct = ImageProduct::find($image_id);
      Storage::disk('public_uploads')->delete('/'.$imageProduct->image);
      ImageProduct::destroy($image_id);
      return redirect()->route('back.product.view',$product_id);
  }
}
