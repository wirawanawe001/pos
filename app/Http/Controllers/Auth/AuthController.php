<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Role;
use Auth;
use Socialite;
use Google_Client;
use Google_Service_People;

class AuthController extends Controller
{
    protected $redirectTo = '/';
    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return Response
     */
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    /**
     * Obtain the user information from provider.  Check if the user already exists in our
     * database by looking up their provider_id in the database.
     * If the user exists, log them in. Otherwise, create a new user then log them in. After that
     * redirect them to the authenticated users homepage.
     *
     * @return Response
     */
    public function callback($provider)
    {
        $user = Socialite::driver($provider)->user();

        $authUser = $this->createOrGetUser($user, $provider);
        auth()->login($authUser, true);
        if (Auth::user()->hasRole('owner')) {
            return redirect()->to('/dashboard');
        } elseif (Auth::user()->hasRole('admin')) {
            return redirect()->to('/dashboard');
        } elseif (Auth::user()->hasRole('member')) {
            return redirect()->to('/');
        }
        return redirect()->to('/');
    }

    /**
     * If a user has registered before using social auth, return the user
     * else, create a new user object.
     * @param  $user Socialite user object
     * @param $provider Social auth provider
     * @return  User
     */

    public function createOrGetUser($user, $provider)
    {
        $account = User::whereProvider($provider)->whereProviderId($user->getId())->first();
        if ($account) {
            return $account;
        } else {
            $userCreate= User::create([
                'name' => $user->getName(),
                'address' => "",
                'photo' => $user->getAvatar(),
                'phone' => "",
                'email' => $user->getEmail(),
                'password' => md5(rand(1, 10000)),
                'provider' => $provider,
                'provider_id' => $user->getId(),
                'status' => true,
                'activated_token' => sha1($user->getEmail()),
            ]);
            $member = Role::whereName('member')->first();
            $userCreate->attachRole($member);
            return $userCreate;
        }
    }
}
