<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          'name' => 'required|string|max:255',
          'address' => 'required|string|max:255',
          'phone' => 'required|numeric|min:7',
        ];
    }

    public function messages()
    {
        return [
          'required' => 'The :attribute field is required.',
        ];
    }
}
