<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateNewUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
        /**$comment = Comment::find($this->route('comment'));
        return $comment && $this->user()->can('update', $comment);*/
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'address' => 'required|string|max:255',
            'city' => 'required|string|min:3',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|numeric|min:11',
            'password' => 'required|string|min:6|confirmed',
        ];
    }

    public function messages()
    {
        return [
          'required' => 'The :attribute field is required.',
        ];
    }
}
