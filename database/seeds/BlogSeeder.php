<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Blog;

class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for($i=0;$i<8;$i++){
          Blog::create([
            'author' => $faker->name(),
            'title' => $faker->sentence(3),
            'content' => $faker->paragraph(6),
            'image' => $faker->imageUrl(1920,1280,'cats'),
          ]);
        }
    }
}
