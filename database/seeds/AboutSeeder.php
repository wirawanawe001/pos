<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\About;

class AboutSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        About::create([
        	'image' => $faker->imageUrl(570,400,'cats'),
            'information' => $faker->sentence(10),
        ]);
    }
}
