<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\User;
use App\Models\Role;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      $user = User::create([
          'name' => 'Riski',
          'address' => $faker->streetAddress,
          'email' => 'soliduszeld@gmail.com',
          'password' => bcrypt('assasin'),
          'status' => TRUE,
      ]);
      $owner = Role::whereName('owner')->first();
      $user->attachRole($owner);

      $user = User::create([
          'name' => 'Riski Zulf',
          'address' => $faker->streetAddress,
          'email' => 'riski.zulfiansyah@gmail.com',
          'password' => bcrypt('assasin'),
          'status' => TRUE,
      ]);
      $admin = Role::whereName('admin')->first();
      $user->attachRole($admin);

      $user = User::create([
          'name' => 'Admin Test',
          'address' => $faker->streetAddress,
          'email' => 'admin@test.com',
          'password' => bcrypt('admintest'),
          'status' => TRUE,
      ]);
      $admin = Role::whereName('admin')->first();
      $user->attachRole($admin);

      for ($i = 0; $i < 3; $i++) {
        $user = User::create([
            'name' => $faker->name,
            'address' => $faker->streetAddress,
            'email' => $faker->safeEmail,
            'password' => bcrypt('password'),
            'status' => TRUE,
        ]);
        $member = Role::whereName('member')->first();
        $user->attachRole($member);
      }
    }
}
