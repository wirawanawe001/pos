<?php

use Illuminate\Database\Seeder;
use App\Models\Product;
use App\Models\Sku;
use Faker\Factory as Faker;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 5; $i++) {
            $product = Product::create([
                'name' => $faker->unique()->word,
                'price' => $faker->biasedNumberBetween(1000,10000),
                'brand_id' => $faker->biasedNumberBetween(1,5),
                'discount_price' => $faker->biasedNumberBetween(0,10000),
                'description' => $faker->sentence(10),
                'status' => $faker->boolean(),
                'list' => $faker->boolean(),
                'rating' => $faker->biasedNumberBetween(1,5),
            ]);
            $name = NULL;
            $pName = explode(" ",$product->name);
            foreach($pName as $pn){
              $strLength = strlen($pn);
              if($strLength==1){
                $name .= $pn[0];
              }elseif($strLength<=2){
                $name .= substr($pn,0,2);
              }elseif($strLength>2){
                $name .= substr($pn,0,2).substr($pn,-1,1);
              }
            }
            Sku::create([
              'sku' => strtoupper('iri-'.$name),
              'stock' => $faker->biasedNumberBetween(1,10),
              'status' => $faker->boolean(),
              'product_id' => $product->id,
            ]);
        }
    }
}
