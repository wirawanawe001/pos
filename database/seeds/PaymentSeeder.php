<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\PaymentMethod;

class PaymentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      for ($i = 0; $i < 5; $i++) {
          PaymentMethod::create([
              'type' => $faker->unique()->word,
              'account_number' => $faker->creditCardNumber,
              'account_name' => $faker->name,
          ]);
      }
    }
}
