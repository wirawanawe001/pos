<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Contact;

class ContactSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      for ($i = 0; $i < 5; $i++) {
          if(rand(0,1) == 1){
            $value = $faker->phoneNumber;
          }else{
            $value = $faker->safeEmail;
          }
          Contact::create([
              'name' => $faker->word,
              'value' => $value,
          ]);
      }
    }
}
