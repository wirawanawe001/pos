<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\BrandCategory;

class BrandCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      for ($i = 0; $i < 3; $i++) {
          BrandCategory::create([
              'brand_id' => $faker->biasedNumberBetween(1,5),
              'category_id' => $faker->biasedNumberBetween(1,5),
          ]);
      }
    }
}
