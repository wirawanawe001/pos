<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\CategoryFeatured;

class CategoryFeaturedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      for ($i = 0; $i < 4; $i++) {
          CategoryFeatured::create([
              'category_id' => $faker->unique()->biasedNumberBetween(1,5),
          ]);
      }
    }
}
