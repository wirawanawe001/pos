<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Brand;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
      for ($i = 0; $i < 6; $i++) {
          Brand::create([
              'name' => $faker->unique()->word,
              'description' => $faker->sentence(8),
          ]);
      }
    }
}
