<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createUser = new Permission();
        $createPost->name         = 'create-user';
        $createPost->display_name = 'Create Users'; // optional
        // Allow a user to...
        $createPost->description  = 'create new users'; // optional
        $createPost->save();

        $editUser = new Permission();
        $editUser->name         = 'edit-user';
        $editUser->display_name = 'Edit Users'; // optional
        // Allow a user to...
        $editUser->description  = 'edit existing users'; // optional
        $editUser->save();

        $deleteUser = new Permission();
        $deleteUser->name         = 'delete-user';
        $deleteUser->display_name = 'Delete Users'; // optional
        // Allow a user to...
        $deleteUser->description  = 'delete existing users'; // optional
        $deleteUser->save();

        $makeOrder = new Permission();
        $makeOrder->name         = 'make-order';
        $makeOrder->display_name = 'Make Orders'; // optional
        // Allow a user to...
        $makeOrder->description  = 'order from existing shop'; // optional
        $makeOrder->save();
    }
}
