<?php

use Illuminate\Database\Seeder;
use App\Models\ImageProduct;
use Faker\Factory as Faker;

class ProductImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 3; $i++) {
          $url1 = $faker->image('public\uploads\product', 640, 400, 'cats');
          $url2 = $faker->image('public\uploads\product', 300, 200, 'cats');
            ImageProduct::create([
                'product_id' => $faker->biasedNumberBetween(1,5),
                'url' => $url1,
                'thumb' => $url2,
            ]);
        }
    }
}
