<?php

use Illuminate\Database\Seeder;
use App\Models\ProductCategory;
use Faker\Factory as Faker;

class ProductCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();
        for ($i = 0; $i < 5; $i++) {
            ProductCategory::create([
                'product_id' => $faker->unique()->biasedNumberBetween(1,5),
                'category_id' => $faker->biasedNumberBetween(1,5),
            ]);
        }
    }
}
